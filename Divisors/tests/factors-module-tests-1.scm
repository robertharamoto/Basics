;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for factors-module.scm                    ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated August 26, 2022                              ###
;;;###                                                       ###
;;;###  updated February 26, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((factors-module)
              :renamer (symbol-prefix-proc 'factors-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (factors-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-number-of-divisors-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-number-of-divisors-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list -1 0) (list 0 0) (list 1 1) (list 2 2)
           (list 3 2) (list 4 3) (list 5 2) (list 6 4)
           (list 7 2) (list 8 4) (list 9 3) (list 10 4)
           (list 11 2) (list 12 6) (list 13 2) (list 14 4)
           (list 15 4) (list 16 5) (list 17 2) (list 18 6)
           (list 19 2) (list 20 6) (list 21 4) (list 22 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (factors-module:number-of-divisors test-num)))
                (let ((string-1 "number"))
                  (begin
                    (factors-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-unique-divisors-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-unique-divisors-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 (list 1)) (list 2 (list 1 2))
           (list 3 (list 1 3)) (list 4 (list 1 2 4))
           (list 5 (list 1 5)) (list 6 (list 1 2 3 6))
           (list 7 (list 1 7)) (list 8 (list 1 2 4 8))
           (list 9 (list 1 3 9)) (list 10 (list 1 2 5 10))
           (list 11 (list 1 11)) (list 12 (list 1 2 3 4 6 12))
           (list 13 (list 1 13)) (list 14 (list 1 2 7 14))
           (list 15 (list 1 3 5 15)) (list 16 (list 1 2 4 8 16))
           (list 17 (list 1 17)) (list 18 (list 1 2 3 6 9 18))
           (list 19 (list 1 19)) (list 20 (list 1 2 4 5 10 20))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (factors-module:unique-divisors test-num)))
                (let ((err-1
                       (format
                        #f "~a : (~a) error : num=~a"
                        sub-name test-label-index test-num)))
                  (begin
                    (factors-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-of-divisors-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-sum-of-divisors-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 1)
           (list 2 3) (list 3 4) (list 4 7) (list 5 6)
           (list 6 12) (list 7 8) (list 8 15) (list 9 13)
           (list 10 18) (list 11 12) (list 12 28) (list 13 14)
           (list 14 24) (list 15 24) (list 16 31)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (factors-module:sum-of-divisors nn)))
                (let ((string-1 "nn"))
                  (begin
                    (factors-simple-test-check
                     shouldbe result
                     string-1 nn
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-remove-factors-of-k-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-remove-factors-of-k-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 2 1) (list 4 2 1) (list 8 2 1)
           (list 16 2 1) (list 32 2 1) (list 64 2 1)
           (list 3 2 3) (list 9 2 9) (list 27 2 27)
           (list 6 2 3) (list 12 2 3) (list 24 2 3)
           (list 18 2 9) (list 36 2 9) (list 54 2 27)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ss (list-ref this-list 0))
                  (kk (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (factors-module:remove-factors-of-k ss kk)))
                (let ((string-1
                       (format #f "ss=~a, kk" ss)))
                  (begin
                    (factors-simple-test-check
                     shouldbe result
                     string-1 kk
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-max-prime-divisor-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-max-prime-divisor-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 #f) (list 2 2) (list 3 3) (list 4 2)
           (list 5 5) (list 6 3) (list 7 7) (list 8 2)
           (list 9 3) (list 10 5) (list 11 11) (list 12 3)
           (list 13 13) (list 14 7) (list 15 5) (list 16 2)
           (list 17 17) (list 18 3) (list 19 19) (list 20 5)
           (list 21 7) (list 22 11) (list 23 23) (list 24 3)
           (list 25 5) (list 26 13) (list 27 3) (list 28 7)
           (list 29 29) (list 30 5) (list 31 31) (list 32 2)
           (list 33 11) (list 34 17) (list 35 7) (list 36 3)
           (list 37 37) (list 38 19) (list 39 13) (list 40 5)
           (list 41 41) (list 42 7) (list 43 43) (list 44 11)
           (list 45 5) (list 46 23) (list 47 47) (list 48 3)
           (list 49 7) (list 50 5) (list 51 17) (list 52 13)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ss (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (factors-module:find-max-prime-divisor ss)))
                (let ((string-1 "ss"))
                  (begin
                    (factors-simple-test-check
                     shouldbe result
                     string-1 ss
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-min-prime-divisor-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-min-prime-divisor-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 #f) (list 2 2) (list 3 3) (list 4 2)
           (list 5 5) (list 6 2) (list 7 7) (list 8 2)
           (list 9 3) (list 10 2) (list 11 11) (list 12 2)
           (list 13 13) (list 14 2) (list 15 3) (list 16 2)
           (list 17 17) (list 18 2) (list 19 19) (list 20 2)
           (list 21 3) (list 22 2) (list 23 23) (list 24 2)
           (list 25 5) (list 26 2) (list 27 3) (list 28 2)
           (list 29 29) (list 30 2) (list 31 31) (list 32 2)
           (list 41 41) (list 42 2) (list 43 43) (list 44 2)
           (list 45 3) (list 46 2) (list 47 47) (list 48 2)
           (list 49 7) (list 50 2) (list 51 3) (list 52 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ss (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (factors-module:find-min-prime-divisor ss)))
                (let ((string-1 "ss"))
                  (begin
                    (factors-simple-test-check
                     shouldbe result
                     string-1 ss
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-min-prime-divisor-max-minutes-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-min-prime-divisor-max-minutes-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 #f) (list 2 2) (list 3 3) (list 4 2)
           (list 5 5) (list 6 2) (list 7 7) (list 8 2)
           (list 9 3) (list 10 2) (list 11 11) (list 12 2)
           (list 13 13) (list 14 2) (list 15 3) (list 16 2)
           (list 17 17) (list 18 2) (list 19 19) (list 20 2)
           (list 21 3) (list 22 2) (list 23 23) (list 24 2)
           (list 25 5) (list 26 2) (list 27 3) (list 28 2)
           (list 29 29) (list 30 2) (list 31 31) (list 32 2)
           (list 41 41) (list 42 2) (list 43 43) (list 44 2)
           (list 45 3) (list 46 2) (list 47 47) (list 48 2)
           (list 49 7) (list 50 2) (list 51 3) (list 52 2)
           ))
         (max-minutes 100)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ss (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (factors-module:find-min-prime-divisor-max-minutes-list
                      ss max-minutes)))
                (let ((string-1 "ss"))
                  (begin
                    (factors-simple-test-check
                     shouldbe result
                     string-1 ss
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-number-to-prime-factors-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-number-to-prime-factors-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 "1") (list 2 "2") (list 3 "3")
           (list 4 "2^2") (list 5 "5") (list 6 "2 * 3")
           (list 7 "7") (list 8 "2^3") (list 9 "3^2")
           (list 10 "2 * 5") (list 11 "11") (list 12 "2^2 * 3")
           (list 13 "13") (list 14 "2 * 7") (list 15 "3 * 5")
           (list 16 "2^4") (list 17 "17") (list 18 "2 * 3^2")
           (list 19 "19") (list 20 "2^2 * 5") (list 21 "3 * 7")
           (list 22 "2 * 11") (list 23 "23") (list 24 "2^3 * 3")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((anum (list-ref this-list 0))
                  (shouldbe-string (list-ref this-list 1)))
              (let ((result-string
                     (factors-module:number-to-prime-factors-to-string
                      anum)))
                (let ((string-1 "anum"))
                  (begin
                    (factors-simple-test-check
                     shouldbe-string result-string
                     string-1 anum
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-exp-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-integer-exp-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 0 1) (list 2 0 1) (list 2 1 2)
           (list 2 2 4) (list 2 3 8) (list 2 4 16)
           (list 2 5 32) (list 2 6 64) (list 2 7 128)
           (list 2 8 256) (list 2 9 512) (list 2 10 1024)
           (list 3 0 1) (list 3 1 3) (list 3 2 9)
           (list 3 3 27) (list 3 4 81) (list 3 5 243)
           (list 4 0 1) (list 4 1 4) (list 4 2 16)
           (list 4 3 64)
           (list 5 0 1) (list 5 1 5) (list 5 2 25)
           (list 5 3 125) (list 5 4 625) (list 5 5 3125)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((base (list-ref this-list 0))
                  (ee (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (factors-module:integer-exp base ee)))
                (let ((string-1
                       (format #f "base=~a, exp" base)))
                  (begin
                    (factors-simple-test-check
                     shouldbe result
                     string-1 ee
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-of-divisors-v1-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-sum-of-divisors-v1-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 3) (list 3 4) (list 4 7)
           (list 5 6) (list 6 12) (list 7 8) (list 8 15)
           (list 9 13) (list 10 18) (list 11 12)
           (list 12 28) (list 13 14) (list 14 24)
           (list 15 24) (list 16 31)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((prime-list
                     (factors-module:prime-factors-list nn)))
                (let ((pr-htable
                       (factors-module:make-prime-factors-hash-table
                        prime-list)))
                  (let ((result
                         (factors-module:sum-of-divisors-v1
                          pr-htable)))
                    (let ((string-1 "nn"))
                      (begin
                        (factors-simple-test-check
                         shouldbe result
                         string-1 nn
                         sub-name test-label-index
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
          ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-proper-sum-of-divisors-v1-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-proper-sum-of-divisors-v1-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 1) (list 3 1)
           (list 4 3) (list 5 1)
           (list 6 6) (list 7 1)
           (list 8 7) (list 9 4)
           (list 10 8) (list 11 1)
           (list 12 16) (list 13 1)
           (list 14 10) (list 15 9)
           (list 16 15) (list 28 28)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((pr-list
                     (factors-module:prime-factors-list num)))
                (let ((pr-htable
                       (factors-module:make-prime-factors-hash-table
                        pr-list)))
                  (let ((result
                         (factors-module:proper-sum-of-divisors-v1
                          num pr-htable)))
                    (let ((string-1 "num"))
                      (begin
                        (factors-simple-test-check
                         shouldbe result
                         string-1 num
                         sub-name test-label-index
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-radical-v1-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-radical-v1-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 1) (list 2 2)
           (list 3 3) (list 4 2)
           (list 5 5) (list 6 6)
           (list 7 7) (list 8 2)
           (list 9 3) (list 10 10)
           (list 11 11) (list 12 6)
           (list 13 13) (list 14 14)
           (list 15 15) (list 16 2)
           (list 17 17) (list 18 6)
           (list 19 19) (list 20 10)
           (list 21 21) (list 22 22)
           (list 23 23) (list 24 6)
           (list 25 5) (list 26 26)
           (list 27 3) (list 28 14)
           (list 29 29) (list 30 30)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((prime-list
                     (factors-module:prime-factors-list nn)))
                (let ((pr-htable
                       (factors-module:make-prime-factors-hash-table
                        prime-list)))
                  (let ((result
                         (factors-module:radical-v1 pr-htable)))
                    (let ((string-1 "nn"))
                      (begin
                        (factors-simple-test-check
                         shouldbe result
                         string-1 nn
                         sub-name test-label-index
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-factors-hash-table-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-make-prime-factors-hash-table-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2 3) (list (list 2 1) (list 3 1)))
           (list (list 2 2 3) (list (list 2 2) (list 3 1)))
           (list (list 2 2 2 3) (list (list 2 3) (list 3 1)))
           (list (list 2 2 2 2 3) (list (list 2 4) (list 3 1)))
           (list (list 2 2 2 2 3 3) (list (list 2 4) (list 3 2)))
           (list (list 2 2 2 3 3 3) (list (list 2 3) (list 3 3)))
           (list (list 2 2 2 3 3 3 3) (list (list 2 3) (list 3 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((pr-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((pr-htable
                     (factors-module:make-prime-factors-hash-table
                      pr-list)))
                (begin
                  (for-each
                   (lambda (aa-list)
                     (begin
                       (let ((shouldbe-prime (list-ref aa-list 0))
                             (shouldbe-count (list-ref aa-list 1)))
                         (let ((result-count
                                (hash-ref pr-htable shouldbe-prime -1)))
                           (let ((string-1 "shouldbe-prime"))
                             (begin
                               (factors-simple-test-check
                                shouldbe-count result-count
                                string-1 shouldbe-prime
                                sub-name test-label-index
                                result-hash-table)
                               ))
                           ))
                       )) shouldbe-list-list)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-factors-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-prime-factors-list-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 2))
           (list 3 (list 3))
           (list 4 (list 2 2))
           (list 5 (list 5))
           (list 6 (list 2 3))
           (list 7 (list 7))
           (list 8 (list 2 2 2))
           (list 9 (list 3 3))
           (list 10 (list 2 5))
           (list 11 (list 11))
           (list 12 (list 2 2 3))
           (list 13 (list 13))
           (list 14 (list 2 7))
           (list 15 (list 3 5))
           (list 16 (list 2 2 2 2))
           (list 17 (list 17))
           (list 18 (list 2 3 3))
           (list 19 (list 19))
           (list 20 (list 2 2 5))
           (list 21 (list 3 7))
           (list 22 (list 2 11))
           (list 23 (list 23))
           (list 24 (list 2 2 2 3))
           (list 25 (list 5 5))
           (list 26 (list 2 13))
           (list 27 (list 3 3 3))
           (list 28 (list 2 2 7))
           (list 29 (list 29))
           (list 30 (list 2 3 5))
           (list 31 (list 31))
           (list 32 (list 2 2 2 2 2))
           (list 33 (list 3 11))
           (list 34 (list 2 17))
           (list 35 (list 5 7))
           (list 36 (list 2 2 3 3))
           (list 37 (list 37))
           (list 38 (list 2 19))
           (list 39 (list 3 13))
           (list 40 (list 2 2 2 5))
           (list 41 (list 41))
           (list 42 (list 2 3 7))
           (list 43 (list 43))
           (list 44 (list 2 2 11))
           (list 45 (list 3 3 5))
           (list 46 (list 2 23))
           (list 47 (list 47))
           (list 48 (list 2 2 2 2 3))
           (list 49 (list 7 7))
           (list 50 (list 2 5 5))
           (list 51 (list 3 17))
           (list 52 (list 2 2 13))
           (list 53 (list 53))
           (list 54 (list 2 3 3 3))
           (list 55 (list 5 11))
           (list 56 (list 2 2 2 7))
           (list 57 (list 3 19))
           (list 58 (list 2 29))
           (list 59 (list 59))
           (list 60 (list 2 2 3 5))
           (list 61 (list 61))
           (list 62 (list 2 31))
           (list 63 (list 3 3 7))
           (list 64 (list 2 2 2 2 2 2))
           (list 65 (list 5 13))
           (list 66 (list 2 3 11))
           (list 67 (list 67))
           (list 68 (list 2 2 17))
           (list 69 (list 3 23))
           (list 70 (list 2 5 7))
           (list 71 (list 71))
           (list 72 (list 2 2 2 3 3))
           (list 73 (list 73))
           (list 74 (list 2 37))
           (list 75 (list 3 5 5))
           (list 76 (list 2 2 19))
           (list 77 (list 7 11))
           (list 78 (list 2 3 13))
           (list 79 (list 79))
           (list 80 (list 2 2 2 2 5))
           (list 81 (list 3 3 3 3))
           (list 82 (list 2 41))
           (list 83 (list 83))
           (list 84 (list 2 2 3 7))
           (list 85 (list 5 17))
           (list 86 (list 2 43))
           (list 87 (list 3 29))
           (list 88 (list 2 2 2 11))
           (list 89 (list 89))
           (list 90 (list 2 3 3 5))
           (list 91 (list 7 13))
           (list 92 (list 2 2 23))
           (list 93 (list 3 31))
           (list 94 (list 2 47))
           (list 95 (list 5 19))
           (list 96 (list 2 2 2 2 2 3))
           (list 97 (list 97))
           (list 98 (list 2 7 7))
           (list 99 (list 3 3 11))
           (list 100 (list 2 2 5 5))
           (list 125 (list 5 5 5))
           (list 504 (list 2 2 2 3 3 7))
           (list 21342 (list 2 3 3557))
           (list 71406 (list 2 3 3 3967))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (factors-module:prime-factors-list test-num)))
                (let ((error-message-1
                       (format
                        #f "~a : (~a) error : num=~a"
                        sub-name test-label-index test-num)))
                  (begin
                    (factors-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     error-message-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-group-prime-exp-pairs-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-group-prime-exp-pairs-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2) (list (list 2 1)))
           (list (list 3) (list (list 3 1)))
           (list (list 2 2) (list (list 2 2)))
           (list (list 5) (list (list 5 1)))
           (list (list 2 3) (list (list 2 1) (list 3 1)))
           (list (list 7) (list (list 7 1)))
           (list (list 2 2 2) (list (list 2 3)))
           (list (list 3 3) (list (list 3 2)))
           (list (list 2 5) (list (list 2 1) (list 5 1)))
           (list (list 2 2 3) (list (list 2 2) (list 3 1)))
           (list (list 2 2 2 2) (list (list 2 4)))
           (list (list 2 3 3) (list (list 2 1) (list 3 2)))
           (list (list 2 2 2 3 3) (list (list 2 3) (list 3 2)))
           (list (list 3 3 3 3 5 5 5 7 7 7 7 7)
                 (list (list 3 4) (list 5 3) (list 7 5)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((prime-list (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (factors-module:group-prime-exp-pairs
                      prime-list)))
                (let ((err-msg-1
                       (format
                        #f "~a : error (~a) : prime list = ~a, "
                        sub-name test-label-index prime-list))
                      (err-msg-2
                       (format
                        #f "shouldbe = ~a, result = ~a"
                        shouldbe-list-list result-list-list)))
                  (begin
                    (factors-assert-lists-equal
                     shouldbe-list-list
                     result-list-list
                     sub-name
                     (string-append err-msg-1 err-msg-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-exp-pairs-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-exp-pairs-list-to-string-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 1 "1") (list 2 "2") (list 3 "3")
           (list 4 "2^2") (list 5 "5") (list 6 "2 * 3")
           (list 7 "7") (list 8 "2^3") (list 9 "3^2")
           (list 10 "2 * 5") (list 11 "11") (list 12 "2^2 * 3")
           (list 13 "13") (list 14 "2 * 7") (list 15 "3 * 5")
           (list 16 "2^4") (list 17 "17") (list 18 "2 * 3^2")
           (list 19 "19") (list 20 "2^2 * 5") (list 21 "3 * 7")
           (list 22 "2 * 11") (list 23 "23") (list 24 "2^3 * 3")
           (list 25 "5^2") (list 26 "2 * 13") (list 27 "3^3")
           (list 28 "2^2 * 7") (list 29 "29") (list 30 "2 * 3 * 5")
           (list 31 "31") (list 32 "2^5") (list 33 "3 * 11")
           (list 64 "2^6") (list 128 "2^7") (list 256 "2^8")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-string (list-ref alist 1)))
              (let ((prime-exp-list
                     (factors-module:group-prime-exp-pairs
                      (factors-module:prime-factors-list test-num))))
                (let ((result-string
                       (factors-module:exp-pairs-list-to-string
                        prime-exp-list)))
                  (let ((string-1 "test-num"))
                    (begin
                      (factors-simple-test-check
                       shouldbe-string result-string
                       string-1 test-num
                       sub-name test-label-index
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (factors-assert-lists-equal
         shouldbe-list result-list
         sub-name
         error-message
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((slen (min shouldbe-length result-length))
            (err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              error-message shouldbe-list result-list))
            (err-2
             (format
              #f "shouldbe length=~a, result length=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((found-flag (member s-elem result-list))
                      (err-3
                       (format
                        #f "~a : missing element(~a)=~a"
                        err-1 ii s-elem)))
                  (begin
                    (unittest2:assert?
                     (not (equal? found-flag #f))
                     sub-name
                     err-3
                     result-hash-table)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
