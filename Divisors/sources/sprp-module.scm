;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  strongly probable prime functions                    ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 4, 2022                                  ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start sprp prime modules
(define-module (sprp-module)
  #:export (mr-exp-mod
            miller-rabin-test
            mr-rand-prime?
            div-two-and-odd
            probable-prime?
            sprp-prime?
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-11 for let-values
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;#############################################################
;;;#############################################################
;;; miller-rabin version of exp-mod
(define (mr-exp-mod base exp mm)
  (begin
    (cond
     ((= exp 0)
      (begin
        1
        ))
     ((even? exp)
      (begin
        (let ((xx
               (mr-exp-mod base (/ exp 2) mm)))
          (let ((xx-2
                 (remainder (* xx xx) mm)))
            (begin
              (if (zero? xx-2)
                  (begin
                    0)
                  (begin
                    xx-2
                    ))
              )))
        ))
     (else
      (begin
        (remainder
         (* base (mr-exp-mod base (1- exp) mm))
         mm)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (miller-rabin-test nn aa)
  (begin
    (let ((result
           (mr-exp-mod aa (1- nn) nn)))
      (let ((rr-2
             (remainder (* result result) nn)))
        (begin
          (cond
           ((= rr-2 0)
            (begin
              #f
              ))
           ((= rr-2 1)
            (begin
              #t
              ))
           (else
            (begin
              (= result 1)
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (mr-rand-prime? nn times)
  (define (local-mr-test nn)
    (define (try-it aa)
      (begin
        (miller-rabin-test nn aa)
        ))
    (begin
      (try-it (1+ (random (1- nn))))
      ))
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((= nn 2)
      (begin
        #t
        ))
     ((even? nn)
      (begin
        #f
        ))
     ((<= times 0)
      (begin
        #t
        ))
     ((local-mr-test nn)
      (begin
        (mr-rand-prime? nn (1- times))
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (div-two-and-odd even-num)
  (begin
    (let ((done-flag #f)
          (dd 0)
          (ss 0)
          (local-num even-num))
      (begin
        (while (equal? done-flag #f)
          (begin
            (srfi-11:let-values
             (((div-num remainder)
               (euclidean/ local-num 2)))
             (begin
               (if (or (odd? div-num)
                       (equal? div-num 1))
                   (begin
                     (set! dd div-num)
                     (set! done-flag #t)
                     ))

               (set! ss (1+ ss))
               (set! local-num div-num)
               ))
            ))

        (list ss dd)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (probable-prime? aa dd nn ss)
  (begin
    (let ((ad-tmp (mr-exp-mod aa dd nn)))
      (begin
        (if (equal? ad-tmp 1)
            (begin
              #t)
            (begin
              (let ((continue-flag #t)
                    (sprp-flag #f)
                    (nn-m-1 (- nn 1)))
                (begin
                  (do ((ii 0 (1+ ii)))
                      ((or (>= ii ss)
                           (equal? continue-flag #f)))
                    (begin
                      (if (equal? ad-tmp nn-m-1)
                          (begin
                            (set! continue-flag #f)
                            (set! sprp-flag #t))
                          (begin
                            (let ((next-ad-tmp
                                   (* ad-tmp ad-tmp)))
                              (begin
                                (set!
                                 ad-tmp (modulo next-ad-tmp nn))
                                ))
                            ))
                      ))
                  sprp-flag
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sprp-prime? nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((or (= nn 2)
          (= nn 3))
      (begin
        #t
        ))
     ((or (even? nn)
          (zero? (modulo nn 3)))
      (begin
        #f
        ))
     ((< nn 1373653)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss))
              )))
        ))
     ((< nn 25326001)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss))
              )))
        ))
     ((< nn 118670087467)
      (begin
        (if (= nn 3215031751)
            (begin
              #f)
            (begin
              (let ((aa-1 2)
                    (aa-2 3)
                    (aa-3 5)
                    (aa-4 7)
                    (sd-list
                     (div-two-and-odd (- nn 1))))
                (let ((ss (list-ref sd-list 0))
                      (dd (list-ref sd-list 1)))
                  (begin
                    (and (probable-prime? aa-1 dd nn ss)
                         (probable-prime? aa-2 dd nn ss)
                         (probable-prime? aa-3 dd nn ss)
                         (probable-prime? aa-4 dd nn ss))
                    )))
              ))
        ))
     ((< nn 2152302898747)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (aa-4 7)
              (aa-5 11)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss)
                   (probable-prime? aa-4 dd nn ss)
                   (probable-prime? aa-5 dd nn ss))
              )))
        ))
     ((< nn 3474749660383)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (aa-4 7)
              (aa-5 11)
              (aa-6 13)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss)
                   (probable-prime? aa-4 dd nn ss)
                   (probable-prime? aa-5 dd nn ss)
                   (probable-prime? aa-6 dd nn ss))
              )))
        ))
     ((< nn 341550071728321)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (aa-4 7)
              (aa-5 11)
              (aa-6 13)
              (aa-7 17)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss)
                   (probable-prime? aa-4 dd nn ss)
                   (probable-prime? aa-5 dd nn ss)
                   (probable-prime? aa-6 dd nn ss)
                   (probable-prime? aa-7 dd nn ss))
              )))
        ))
     (else
      (begin
        (let ((ntimes 100))
          (begin
            ;;; probable prime
            (mr-rand-prime? nn ntimes)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
