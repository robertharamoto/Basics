#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  find the list of divisors interactively              ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated August 26, 2022                              ###
;;;###                                                       ###
;;;###  updated January 16, 2019                             ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "tests"))

(set! %load-compiled-path
      (append %load-compiled-path (list "." "objects")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### ice-9 ftw for file tree walk functions
(use-modules ((ice-9 ftw)
              :renamer (symbol-prefix-proc 'ice-9-ftw:)))

;;; ice-9 rdelim for read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

(use-modules ((factors-module)
              :renamer (symbol-prefix-proc 'factors-module:)))

(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;#############################################################
;;;#############################################################
;;;### load all test-*.scm files
(define (load-all-test-files
         dir-string file-string sub-string lcommand)
  (begin
    (let ((counter 0))
      (begin
        (ice-9-ftw:nftw
         "."
         (lambda (filename statinfo flag base level)
           (begin
             (if (equal? flag 'regular)
                 (begin
                   (if (and
                        (not
                         (equal?
                          (ice-9-regex:string-match
                           dir-string filename) #f))
                        (not
                         (equal?
                          (ice-9-regex:string-match
                           file-string filename) #f))
                        (not
                         (equal?
                          (ice-9-regex:string-match
                           sub-string filename) #f))
                        (not
                         (ice-9-regex:string-match
                          "./run-tests.scm" filename)))
                       (begin
                         (display
                          (format #f "load file ~a~%" filename))
                         (lcommand filename)
                         (set! counter (+ counter 1))
                         ))
                   ))
             #t
             )))
        (newline)
        counter
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (compute-output anum)
  (begin
    (let ((ndigits (digits-module:number-of-digits anum)))
      (begin
        (display
         (ice-9-format:format
          #f "number of digits = ~:d~%" ndigits))
        (force-output)
        (let ((is-prime-flag
               (sprp-module:sprp-prime? anum)))
          (begin
            (if (equal? is-prime-flag #f)
                (begin
                  (let ((ep-list
                         (factors-module:group-prime-exp-pairs
                          (factors-module:prime-factors-list anum))))
                    (let ((pstring
                           (factors-module:exp-pairs-list-to-string ep-list)))
                      (begin
                        (display
                         (ice-9-format:format
                          #f "n = ~:d = ~a~%"
                          anum pstring))
                        (force-output)
                        ))
                    ))
                (begin
                  (display
                   (ice-9-format:format
                    #f "n = ~:d = ~:d prime!~%"
                    anum anum))
                  (force-output)
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-number-line-macro
  (syntax-rules ()
    ((process-number-line-macro
      in-line num-line)
     (begin
       (let ((number (string->number num-line)))
         (begin
           (if (equal? number #f)
               (begin
                 (display
                  (format
                   #f "invalid number ~a~%"
                   num-line))
                 (force-output))
               (begin
                 (if (equal? (string-index in-line #\() #f)
                     (begin
                       (timer-module:time-code-macro
                        (begin
                          (compute-output number)
                          )))
                     (begin
                       (let ((enum (eval-string in-line)))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "number ~:d~%"
                             enum))
                           (force-output)
                           (timer-module:time-code-macro
                            (begin
                              (compute-output enum)
                              ))
                           ))
                       ))
                 ))
           (newline)
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((count 0)
          (continue-loop-flag #t))
      (begin
        (while
            (equal? continue-loop-flag #t)
          (begin
            (display
             (format #f "Enter integer (q to quit) : "))
            (force-output)
            (let ((in-line
                   (string-delete
                    #\, (ice-9-rdelim:read-line))))
              (let ((num-line
                     (string-filter char-numeric? in-line))
                    (quit-line
                     (string-filter char-alphabetic? in-line)))
                (begin
                  (if (or (string-ci=? quit-line "q")
                          (string-ci=? quit-line "quit"))
                      (begin
                        (set! continue-loop-flag #f))
                      (begin
                        (process-number-line-macro
                         in-line num-line)
                        ))
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-07-26")
          (details-flag #t))
      (let ((title-string
             (format #f "Calculate prime factors (version ~a)" version-string))
            (counter
             (load-all-test-files
              "tests" "tests" ".scm$" load))
            (counter-2
             (load-all-test-files
              "objects" "tests" ".go$" load-compiled)
             ))
        (begin
          (timer-module:time-code-macro
           (begin
             (unittest2:run-all-tests
              title-string details-flag)
             (newline)
             ))

          (display
           (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-loop)

          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
