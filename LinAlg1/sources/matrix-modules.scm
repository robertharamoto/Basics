;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  matrix-module - basic matrix operations              ###
;;;###  matrix implemented with guile's arrays               ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start matrix modules
(define-module (matrix-modules)
  #:export (matrix-display
            make-zero-array
            make-list-list-array
            matrix-multiply
            matrix-addition
            matrix-subtraction

            matrix-scalar-multiply
            matrix-scalar-divide
            matrix-transpose
            matrix-augment-with-identity

            matrix-switch-two-rows
            matrix-inverse
            matrix-trace

            make-reduced-array
            matrix-determinant

            vector-dot-product
            vector-scalar-product
            vector-gram-schmidt-proj
            vector-normalize

            mgs-normalized-vector-list-columns
            mgs-calc-q-columns
            mgs-calc-r
            mgs-matrix-rank
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### ice-9-format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;#############################################################
;;;#############################################################
;;;### begin main functions

;;;#############################################################
;;;#############################################################
(define (matrix-display matrix)
  (begin
    (if (array? matrix)
        (begin
          (let ((dim-list (array-dimensions matrix)))
            (let ((row-dim (list-ref dim-list 0))
                  (col-dim (list-ref dim-list 1)))
              (begin
                (display (format #f "    ["))
                (do ((ii 0 (1+ ii)))
                    ((>= ii row-dim))
                  (begin
                    (if (equal? ii 0)
                        (begin
                          (display (format #f " [ ")))
                        (begin
                          (display (format #f "      [ "))
                          ))
                    (do ((jj 0 (1+ jj)))
                        ((>= jj col-dim))
                      (begin
                        (let ((m-elem (array-ref matrix ii jj)))
                          (begin
                            (if (zero? jj)
                                (begin
                                  (if (inexact? m-elem)
                                      (begin
                                        (display
                                         (ice-9-format:format
                                          #f "~12,8f" m-elem)))
                                      (begin
                                        (display
                                         (format #f "~a" m-elem))
                                        )))
                                (begin
                                  (if (inexact? m-elem)
                                      (begin
                                        (display
                                         (ice-9-format:format
                                          #f ", ~12,8f" m-elem)))
                                      (begin
                                        (display (format #f ", ~a" m-elem))
                                        ))
                                  ))
                            ))
                        ))
                    (display (format #f " ]~%"))
                    ))
                (display (format #f "    ]~%"))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-zero-array row-dim col-dim)
  (begin
    (if (and (> row-dim 0)
             (> col-dim 0))
        (begin
          (make-array 0 row-dim col-dim))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-list-list-array alist-list)
  (begin
    (if (and (list? alist-list)
             (list? (list-ref alist-list 0)))
        (begin
          (let ((new-arr
                 (list->array (list 0 0) alist-list)))
            (begin
              new-arr
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-multiply mat-1 mat-2)
  (begin
    (if (and (array? mat-1)
             (array? mat-2))
        (begin
          (let ((dim-1 (array-dimensions mat-1))
                (dim-2 (array-dimensions mat-2)))
            (let ((row-1-dim (list-ref dim-1 0))
                  (col-1-dim (list-ref dim-1 1))
                  (row-2-dim (list-ref dim-2 0))
                  (col-2-dim (list-ref dim-2 1)))
              (begin
                (if (equal? col-1-dim row-2-dim)
                    (begin
                      (let ((new-arr
                             (make-zero-array
                              row-1-dim col-2-dim)))
                        (begin
                          (do ((ii 0 (1+ ii)))
                              ((>= ii row-1-dim))
                            (begin
                              (do ((jj 0 (1+ jj)))
                                  ((>= jj col-2-dim))
                                (begin
                                  (let ((sum 0))
                                    (begin
                                      (do ((kk 0 (1+ kk)))
                                          ((>= kk col-1-dim))
                                        (begin
                                          (let ((elem-1
                                                 (array-ref mat-1 ii kk))
                                                (elem-2
                                                 (array-ref mat-2 kk jj)))
                                            (begin
                                              (set!
                                               sum
                                               (+ sum (* elem-1 elem-2)))
                                              ))
                                          ))
                                      (array-set! new-arr sum ii jj)
                                      ))
                                  ))
                              ))
                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax apply-func-to-two-matrices-macro
  (syntax-rules ()
    ((apply-func-to-two-matrices-macro
      max-row max-col mat-1 mat-2 func
      new-arr)
     (begin
       (do ((ii 0 (1+ ii)))
           ((>= ii max-row))
         (begin
           (do ((jj 0 (1+ jj)))
               ((>= jj max-col))
             (begin
               (let ((elem-1 (array-ref mat-1 ii jj))
                     (elem-2 (array-ref mat-2 ii jj)))
                 (begin
                   (array-set!
                    new-arr
                    (func elem-1 elem-2)
                    ii jj)
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-addition mat-1 mat-2)
  (begin
    (if (and (array? mat-1)
             (array? mat-2))
        (begin
          (let ((dim-1 (array-dimensions mat-1))
                (dim-2 (array-dimensions mat-2)))
            (let ((row-1-dim (list-ref dim-1 0))
                  (col-1-dim (list-ref dim-1 1))
                  (row-2-dim (list-ref dim-2 0))
                  (col-2-dim (list-ref dim-2 1)))
              (begin
                (if (and (equal? row-1-dim row-2-dim)
                         (equal? col-1-dim col-2-dim))
                    (begin
                      (let ((new-arr
                             (make-zero-array
                              row-1-dim col-1-dim)))
                        (begin
                          (apply-func-to-two-matrices-macro
                           row-1-dim col-1-dim mat-1 mat-2 +
                           new-arr)

                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-subtraction mat-1 mat-2)
  (begin
    (if (and (array? mat-1)
             (array? mat-2))
        (begin
          (let ((dim-1 (array-dimensions mat-1))
                (dim-2 (array-dimensions mat-2)))
            (let ((row-1-dim (list-ref dim-1 0))
                  (col-1-dim (list-ref dim-1 1))
                  (row-2-dim (list-ref dim-2 0))
                  (col-2-dim (list-ref dim-2 1)))
              (begin
                (if (and (equal? row-1-dim row-2-dim)
                         (equal? col-1-dim col-2-dim))
                    (begin
                      (let ((new-arr
                             (make-zero-array
                              row-1-dim col-1-dim)))
                        (begin
                          (apply-func-to-two-matrices-macro
                           row-1-dim col-1-dim mat-1 mat-2 -
                           new-arr)

                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax apply-func-to-single-matrix-macro
  (syntax-rules ()
    ((apply-func-to-single-matrix-macro
      max-row max-col mat-1 func
      new-arr)
     (begin
       (do ((ii 0 (1+ ii)))
           ((>= ii max-row))
         (begin
           (do ((jj 0 (1+ jj)))
               ((>= jj max-col))
             (begin
               (let ((elem-1 (array-ref mat-1 ii jj)))
                 (begin
                   (array-set!
                    new-arr
                    (func elem-1)
                    ii jj)
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-scalar-multiply mat-1 scalar)
  (begin
    (if (and (array? mat-1)
             (number? scalar))
        (begin
          (let ((dim-1-list (array-dimensions mat-1)))
            (let ((row-1-dim (list-ref dim-1-list 0))
                  (col-1-dim (list-ref dim-1-list 1)))
              (begin
                (if (and (> row-1-dim 0)
                         (> col-1-dim 0))
                    (begin
                      (let ((new-arr
                             (make-zero-array row-1-dim col-1-dim)))
                        (begin
                          (apply-func-to-single-matrix-macro
                           row-1-dim col-1-dim mat-1
                           (lambda (elem) (* scalar elem))
                           new-arr)

                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-scalar-divide mat-1 scalar)
  (begin
    (if (and (array? mat-1)
             (number? scalar)
             (not (equal? scalar 0)))
        (begin
          (let ((dim-1-list (array-dimensions mat-1)))
            (let ((row-1-dim (list-ref dim-1-list 0))
                  (col-1-dim (list-ref dim-1-list 1)))
              (begin
                (if (and (> row-1-dim 0)
                         (> col-1-dim 0))
                    (begin
                      (let ((new-arr
                             (make-zero-array
                              row-1-dim col-1-dim)))
                        (begin
                          (apply-func-to-single-matrix-macro
                           row-1-dim col-1-dim mat-1
                           (lambda (elem)
                             (begin
                               (/ elem scalar)
                               ))
                           new-arr)

                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-transpose mat-1)
  (begin
    (if (array? mat-1)
        (begin
          (let ((dim-1-list (array-dimensions mat-1)))
            (let ((row-1-dim (list-ref dim-1-list 0))
                  (col-1-dim (list-ref dim-1-list 1)))
              (begin
                (if (and (> row-1-dim 0)
                         (> col-1-dim 0))
                    (begin
                      (let ((new-arr
                             (make-zero-array col-1-dim row-1-dim)))
                        (begin
                          (do ((ii 0 (1+ ii)))
                              ((>= ii row-1-dim))
                            (begin
                              (do ((jj 0 (1+ jj)))
                                  ((>= jj col-1-dim))
                                (begin
                                  (let ((elem-1
                                         (array-ref mat-1 ii jj)))
                                    (begin
                                      (array-set!
                                       new-arr
                                       elem-1 jj ii)
                                      ))
                                  ))
                              ))

                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-augment-with-identity mat-1)
  (begin
    (if (array? mat-1)
        (begin
          (let ((dim-1-list (array-dimensions mat-1)))
            (let ((row-1-dim (list-ref dim-1-list 0))
                  (col-1-dim (list-ref dim-1-list 1)))
              (begin
                (if (and (> row-1-dim 0)
                         (> col-1-dim 0))
                    (begin
                      (let ((new-arr
                             (make-zero-array
                              row-1-dim (* 2 col-1-dim))))
                        (begin
                          (do ((ii 0 (1+ ii)))
                              ((>= ii row-1-dim))
                            (begin
                              (do ((jj 0 (1+ jj)))
                                  ((>= jj col-1-dim))
                                (begin
                                  (let ((elem-1
                                         (array-ref mat-1 ii jj)))
                                    (begin
                                      (array-set!
                                       new-arr elem-1 ii jj)
                                      ))
                                  ))
                              ))
                          (do ((ii 0 (1+ ii)))
                              ((>= ii row-1-dim))
                            (begin
                              (let ((col-num (+ row-1-dim ii)))
                                (begin
                                  (array-set! new-arr 1 ii col-num)
                                  ))
                              ))

                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-switch-two-rows mat-1 row-1 row-2)
  (begin
    (if (array? mat-1)
        (begin
          (let ((dim-1-list (array-dimensions mat-1)))
            (let ((row-1-dim (list-ref dim-1-list 0))
                  (col-1-dim (list-ref dim-1-list 1)))
              (begin
                (if (and (> row-1-dim 0)
                         (> col-1-dim 0))
                    (begin
                      (let ((new-arr
                             (make-zero-array
                              row-1-dim col-1-dim)))
                        (begin
                          (do ((ii 0 (1+ ii)))
                              ((>= ii row-1-dim))
                            (begin
                              (do ((jj 0 (1+ jj)))
                                  ((>= jj col-1-dim))
                                (begin
                                  (let ((elem-1
                                         (array-ref mat-1 ii jj)))
                                    (begin
                                      (array-set!
                                       new-arr elem-1 ii jj)
                                      ))
                                  ))
                              ))
                          (do ((jj 0 (1+ jj)))
                              ((>= jj col-1-dim))
                            (begin
                              (let ((elem-1
                                     (array-ref mat-1 row-1 jj))
                                    (elem-2
                                     (array-ref mat-1 row-2 jj)))
                                (begin
                                  (array-set!
                                   new-arr elem-2 row-1 jj)
                                  (array-set!
                                   new-arr elem-1 row-2 jj)
                                  ))
                              ))

                          new-arr
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax divide-out-pivot-element-macro
  (syntax-rules ()
    ((divide-out-pivot-element-macro
      aug-arr aug-col-dim
      pivot-elem current-pivot-row)
     (begin
       (do ((jj 0 (1+ jj)))
           ((>= jj aug-col-dim))
         (begin
           (let ((curr-elem
                  (array-ref
                   aug-arr current-pivot-row jj)))
             (begin
               (array-set!
                aug-arr
                (/ curr-elem pivot-elem)
                current-pivot-row jj)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax subtract-current-row-from-row-ii-macro
  (syntax-rules ()
    ((subtract-current-row-from-row-ii-macro
      aug-arr aug-col-dim
      pivot-elem current-pivot-row ii)
     (begin
       (let ((this-factor
              (/ (array-ref
                  aug-arr ii current-pivot-row)
                 pivot-elem)))
         (begin
           (do ((jj 0 (1+ jj)))
               ((>= jj aug-col-dim))
             (begin
               (let ((this-elem
                      (array-ref aug-arr ii jj))
                     (curr-elem
                      (array-ref
                       aug-arr current-pivot-row jj)))
                 (let ((next-elem
                        (- this-elem (* curr-elem this-factor))))
                   (begin
                     (array-set! aug-arr next-elem ii jj)
                     )))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax exchange-two-rows-macro
  (syntax-rules ()
    ((exchange-two-rows-macro
      aug-arr aug-col-dim row-1 row-2)
     (begin
       (do ((jj 0 (1+ jj)))
           ((>= jj aug-col-dim))
         (begin
           (let ((elem-1 (array-ref aug-arr row-1 jj))
                 (elem-2 (array-ref aug-arr row-2 jj)))
             (begin
               (array-set! aug-arr elem-2 row-1 jj)
               (array-set! aug-arr elem-1 row-2 jj)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax extract-inverse-from-augmented-array-macro
  (syntax-rules ()
    ((extract-inverse-from-augmented-array-macro
      aug-arr aug-row aug-col inv-array)
     (begin
       (let ((inv-arr (make-zero-array aug-row aug-col)))
         (begin
           (do ((ii 0 (1+ ii)))
               ((>= ii aug-row))
             (begin
               (do ((jj 0 (1+ jj)))
                   ((>= jj aug-col))
                 (begin
                   (let ((aug-index (+ aug-col jj)))
                     (let ((this-elem
                            (array-ref aug-arr ii aug-index)))
                       (begin
                         (array-set! inv-arr this-elem ii jj)
                         )))
                   ))
               ))
           (set! inv-array inv-arr)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax matrix-inverse-ii-curr-macro
  (syntax-rules ()
    ((matrix-inverse-ii-curr-macro
      aug-arr row-dim col-dim aug-col-dim
      ii-curr pivot-tolerance
      stop-loop-flag)
     (begin
       (let ((pivot-elem
              (array-ref aug-arr ii-curr ii-curr)))
         (begin
           (if (> (abs pivot-elem) pivot-tolerance)
               (begin
                 ;;; we have a good pivot
                 (do ((ii-2 0 (1+ ii-2)))
                     ((>= ii-2 row-dim))
                   (begin
                     (if (equal? ii-2 ii-curr)
                         (begin
                           (divide-out-pivot-element-macro
                            aug-arr aug-col-dim
                            pivot-elem ii-curr)

                           (set! pivot-elem 1))
                         (begin
                           (subtract-current-row-from-row-ii-macro
                            aug-arr aug-col-dim
                            pivot-elem ii-curr ii-2)
                           ))
                     )))
               (begin
                 ;;; we have a zero pivot point
                 ;;; so find the next non-zero point and interchange rows
                 (let ((next-pivot-row (1+ ii-curr))
                       (found-nonzero-pivot-flag #f))
                   (begin
                     ;;; first find next non-zero pivot point
                     (do ((ii-3 next-pivot-row (1+ ii-3)))
                         ((or (>= ii-3 row-dim)
                              (equal? found-nonzero-pivot-flag #t)))
                       (begin
                         (let ((this-elem
                                (array-ref aug-arr ii-3 ii-curr)))
                           (begin
                             (if (> (abs this-elem) pivot-tolerance)
                                 (begin
                                   (set! found-nonzero-pivot-flag #t)
                                   (set! next-pivot-row ii-3)
                                   ))
                             ))
                         ))

                     (if (equal? found-nonzero-pivot-flag #t)
                         (begin
                           (exchange-two-rows-macro
                            aug-arr aug-col-dim ii-curr next-pivot-row)

                           (let ((pivot-elem
                                  (array-ref aug-arr ii-curr ii-curr)))
                             (begin
                               (do ((ii-3 0 (1+ ii-3)))
                                   ((>= ii-3 row-dim))
                                 (begin
                                   (if (equal? ii-3 ii-curr)
                                       (begin
                                         (divide-out-pivot-element-macro
                                          aug-arr aug-col-dim
                                          pivot-elem ii-curr)

                                         (set! pivot-elem 1))
                                       (begin
                                         (subtract-current-row-from-row-ii-macro
                                          aug-arr aug-col-dim
                                          pivot-elem ii-curr ii-3)
                                         ))
                                   ))
                               )))
                         (begin
                           (set! stop-loop-flag #t)
                           (set! aug-arr #f)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-inverse mat-1 pivot-tolerance)
  (begin
    (if (array? mat-1)
        (begin
          (let ((dim-1-list (array-dimensions mat-1)))
            (let ((row-dim (list-ref dim-1-list 0))
                  (col-dim (list-ref dim-1-list 1)))
              (begin
                (if (and (> row-dim 0)
                         (> col-dim 0)
                         (equal? row-dim col-dim))
                    (begin
                      (let ((aug-arr
                             (matrix-augment-with-identity mat-1))
                            (aug-col-dim (* 2 col-dim))
                            (stop-loop-flag #f))
                        (begin
                          (do ((ii-curr 0 (1+ ii-curr)))
                              ((or (>= ii-curr row-dim)
                                   (equal? stop-loop-flag #t)))
                            (begin
                              (matrix-inverse-ii-curr-macro
                               aug-arr row-dim col-dim aug-col-dim
                               ii-curr pivot-tolerance
                               stop-loop-flag)
                              ))
                          (if (equal? stop-loop-flag #f)
                              (begin
                                (let ((result-inverse-array #f))
                                  (begin
                                    (extract-inverse-from-augmented-array-macro
                                     aug-arr row-dim col-dim
                                     result-inverse-array)

                                    result-inverse-array
                                    )))
                              (begin
                                #f
                                ))
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-trace mat-1)
  (begin
    (if (array? mat-1)
        (begin
          (let ((aa-dim-list (array-dimensions mat-1)))
            (let ((aa-rows (list-ref aa-dim-list 0))
                  (aa-cols (list-ref aa-dim-list 1)))
              (begin
                (if (equal? aa-rows aa-cols)
                    (begin
                      (let ((trace 0))
                        (begin
                          (do ((ii 0 (1+ ii)))
                              ((>= ii aa-rows))
                            (begin
                              (let ((aa-elem (array-ref mat-1 ii ii)))
                                (begin
                                  (set! trace (+ trace aa-elem))
                                  ))
                              ))
                          trace
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-reduced-array
         mat-1 max-rows max-cols elim-row elim-col)
  (begin
    (let ((next-array
           (make-zero-array
            (1- max-rows) (1- max-cols))))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-rows))
          (begin
            (do ((jj 0 (1+ jj)))
                ((>= jj max-cols))
              (begin
                (cond
                 ((< ii elim-row)
                  (begin
                    (cond
                     ((< jj elim-col)
                      (begin
                        (let ((a-elem (array-ref mat-1 ii jj)))
                          (begin
                            (array-set! next-array a-elem ii jj)
                            ))
                        ))
                     ((> jj elim-col)
                      (begin
                        (let ((a-elem (array-ref mat-1 ii jj)))
                          (begin
                            (array-set! next-array a-elem ii (1- jj))
                            ))
                        )))
                    ))
                 ((> ii elim-row)
                  (begin
                    (cond
                     ((< jj elim-col)
                      (begin
                        (let ((a-elem (array-ref mat-1 ii jj)))
                          (begin
                            (array-set! next-array a-elem (1- ii) jj)
                            ))
                        ))
                     ((> jj elim-col)
                      (begin
                        (let ((a-elem (array-ref mat-1 ii jj)))
                          (begin
                            (array-set! next-array a-elem (1- ii) (1- jj))
                            ))
                        )))
                    )))
                ))
            ))
        next-array
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-determinant mat-1)
  (begin
    (if (array? mat-1)
        (begin
          (let ((aa-dim (array-dimensions mat-1)))
            (let ((aa-rows (list-ref aa-dim 0))
                  (aa-cols (list-ref aa-dim 1)))
              (begin
                (if (equal? aa-rows aa-cols)
                    (begin
                      (cond
                       ((equal? aa-rows 2)
                        (begin
                          (let ((det
                                 (- (* (array-ref mat-1 0 0)
                                       (array-ref mat-1 1 1))
                                    (* (array-ref mat-1 0 1)
                                       (array-ref mat-1 1 0))
                                    )))
                            (begin
                              det
                              ))
                          ))
                       ((equal? aa-rows 3)
                        (begin
                          (let ((a (array-ref mat-1 0 0))
                                (b (array-ref mat-1 0 1))
                                (c (array-ref mat-1 0 2))
                                (d (array-ref mat-1 1 0))
                                (e (array-ref mat-1 1 1))
                                (f (array-ref mat-1 1 2))
                                (g (array-ref mat-1 2 0))
                                (h (array-ref mat-1 2 1))
                                (i (array-ref mat-1 2 2)))
                            (let ((det
                                   (- (+ (* a e i) (* b f g) (* c d h))
                                      (+ (* c e g) (* b d i) (* a f h)))))
                              (begin
                                det
                                )))
                          ))
                       (else
                        (begin
                          (let ((det 0)
                                (sign 1)
                                (ok-flag #t))
                            (begin
                              (do ((jj 0 (1+ jj)))
                                  ((or (>= jj aa-cols)
                                       (equal? ok-flag #f)))
                                (begin
                                  (let ((next-array
                                         (make-reduced-array
                                          mat-1 aa-rows aa-cols 0 jj))
                                        (aa-elem (array-ref mat-1 0 jj)))
                                    (let ((sub-md
                                           (matrix-determinant next-array)))
                                      (begin
                                        (if (number? sub-md)
                                            (begin
                                              (set!
                                               det
                                               (+ det
                                                  (* sign
                                                     aa-elem sub-md)))
                                              (set! sign (* -1 sign)))
                                            (begin
                                              (set! ok-flag #f)
                                              (set! det #f)
                                              ))
                                        )))
                                  ))
                              det
                              ))
                          ))
                       ))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (vector-dot-product uu vv)
  (begin
    (if (and (array? uu)
             (array? vv)
             (equal? (array-dimensions uu)
                     (array-dimensions vv)))
        (begin
          (let ((size (car (array-dimensions uu)))
                (sum 0))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii size))
                (begin
                  (let ((u-elem (array-ref uu ii))
                        (v-elem (array-ref vv ii)))
                    (begin
                      (set! sum (+ sum (* u-elem v-elem)))
                      ))
                  ))
              sum
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (vector-scalar-product uu scalar)
  (begin
    (if (array? uu)
        (begin
          (let ((size (car (array-dimensions uu))))
            (let ((next-array (make-array 0 size)))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((>= ii size))
                  (begin
                    (let ((u-elem (array-ref uu ii)))
                      (begin
                        (array-set!
                         next-array
                         (* u-elem scalar) ii)
                        ))
                    ))
                next-array
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (vector-gram-schmidt-proj uu vv)
  (begin
    (let ((uu-sq (vector-dot-product uu uu)))
      (begin
        (if (and (not (equal? uu-sq #f))
                 (> uu-sq 0))
            (begin
              (let ((uu-vv (vector-dot-product uu vv)))
                (begin
                  (if (not (equal? uu-vv #f))
                      (begin
                        (let ((scalar (/ uu-vv uu-sq)))
                          (begin
                            (vector-scalar-product uu scalar)
                            )))
                      (begin
                        #f
                        ))
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a vector with norm=1
(define (vector-normalize uu)
  (begin
    (let ((uu-norm-sq (vector-dot-product uu uu)))
      (begin
        (if (and (not (equal? uu-norm-sq #f))
                 (> uu-norm-sq 0))
            (begin
              (let ((uu-sqrt (sqrt uu-norm-sq)))
                (let ((scalar (/ 1 uu-sqrt)))
                  (begin
                    (vector-scalar-product uu scalar)
                    ))
                ))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax subtract-two-vectors-macro
  (syntax-rules ()
    ((subtract-two-vectors-macro
      uu vv nrows)
     (begin
       (do ((ii 0 (1+ ii)))
           ((>= ii nrows))
         (begin
           (let ((uu-elem (array-ref uu ii))
                 (vv-elem (array-ref vv ii)))
             (begin
               (array-set!
                uu (- uu-elem vv-elem) ii)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of vectors (list v1, v2, ..., vn)
(define (mgs-normalized-vector-list-columns
         aa-array ftolerance)
  (begin
    (let ((aa-dim-list (array-dimensions aa-array))
          (vv-list (list)))
      (let ((nrows (list-ref aa-dim-list 0))
            (ncols (list-ref aa-dim-list 1))
            (continue-loop-flag #t))
        (let ((max-vectors (min nrows ncols)))
          (begin
            (do ((jj 0 (1+ jj)))
                ((or (>= jj max-vectors)
                     (equal? continue-loop-flag #f)))
              (begin
                (let ((vvec (make-array 0 nrows)))
                  (begin
                    (do ((ii 0 (1+ ii)))
                        ((>= ii nrows))
                      (begin
                        (let ((aa-elem
                               (array-ref aa-array ii jj)))
                          (begin
                            (array-set! vvec aa-elem ii)
                            ))
                        ))

                    (if (> jj 0)
                        (begin
                          (do ((ii 0 (1+ ii)))
                              ((or (>= ii jj)
                                   (equal? continue-loop-flag #f)))
                            (begin
                              (let ((vv-item (list-ref vv-list ii)))
                                (let ((proj-vv
                                       (vector-gram-schmidt-proj
                                        vv-item vvec)))
                                  (begin
                                    (if (not (equal? proj-vv #f))
                                        (begin
                                          (subtract-two-vectors-macro
                                           vvec proj-vv nrows))
                                        (begin
                                          (set! continue-loop-flag #f)
                                          (set! vv-list #f)
                                          ))
                                    )))
                              ))
                          ))

                    (if (list? vv-list)
                        (begin
                          (let ((v2
                                 (vector-dot-product vvec vvec))
                                (normed-vvec #f))
                            (begin
                              (if (> (abs v2) ftolerance)
                                  (begin
                                    (let ((nvec
                                           (vector-normalize vvec)))
                                      (begin
                                        (set! normed-vvec nvec)
                                        )))
                                  (begin
                                    (set!
                                     normed-vvec (make-array 0 nrows))
                                    ))

                              (set!
                               vv-list
                               (append vv-list (list normed-vvec)))
                              ))
                          ))
                    ))
                ))

            (if (equal? continue-loop-flag #t)
                (begin
                  vv-list)
                (begin
                  #f
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns the Q-array, column vectors
(define (mgs-calc-q-columns
         aa-array ftolerance)
  (begin
    (let ((aa-dim (array-dimensions aa-array)))
      (let ((nrows (car aa-dim)))
        (let ((vv-list
               (mgs-normalized-vector-list-columns
                aa-array ftolerance)))
          (begin
            (if (list? vv-list)
                (begin
                  (let ((vlen (length vv-list)))
                    (let ((qq-array
                           (make-array 0 nrows vlen)))
                      (begin
                        (do ((jj 0 (1+ jj)))
                            ((>= jj vlen))
                          (begin
                            (let ((jth-col (list-ref vv-list jj)))
                              (let ((ii-max
                                     (list-ref
                                      (array-dimensions jth-col) 0)))
                                (begin
                                  (do ((ii 0 (1+ ii)))
                                      ((>= ii ii-max))
                                    (begin
                                      (let ((vv-elem
                                             (array-ref jth-col ii)))
                                        (begin
                                          (array-set!
                                           qq-array vv-elem ii jj)
                                          ))
                                      ))
                                  )))
                            ))
                        qq-array
                        ))
                    ))
                (begin
                  (gc)
                  #f
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns the R-array
(define (mgs-calc-r aa-array qq-array)
  (begin
    (if (and (array? aa-array)
             (array? qq-array))
        (begin
          (let ((aa-dim-list (array-dimensions aa-array))
                (qq-dim-list (array-dimensions qq-array)))
            (let ((aa-rows (list-ref aa-dim-list 0))
                  (qq-rows (list-ref qq-dim-list 0)))
              (begin
                (if (equal? qq-rows aa-rows)
                    (begin
                      (let ((qq-transpose
                             (matrix-transpose qq-array)))
                        (begin
                          (if (array? qq-transpose)
                              (begin
                                (matrix-multiply
                                 qq-transpose aa-array))
                              (begin
                                #f
                                ))
                          )))
                    (begin
                      #f
                      ))
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns the rank of the matrix from the R matrix
(define (mgs-matrix-rank
         rr-array ftolerance)
  (begin
    (if (array? rr-array)
        (begin
          (let ((rrank 0)
                (rr-dim-list (array-dimensions rr-array)))
            (let ((rr-rows (list-ref rr-dim-list 0))
                  (rr-cols (list-ref rr-dim-list 1)))
              (let ((rmax (min rr-rows rr-cols)))
                (begin
                  (do ((ii 0 (1+ ii)))
                      ((>= ii rmax))
                    (begin
                      (let ((r-elem
                             (array-ref rr-array ii ii)))
                        (begin
                          (if (> (abs r-elem) ftolerance)
                              (begin
                                (set! rrank (1+ rrank))
                                ))
                          ))
                      ))
                  rrank
                  )))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
