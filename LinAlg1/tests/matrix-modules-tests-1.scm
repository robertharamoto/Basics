;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for matrix-modules.scm                    ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((matrix-modules)
              :renamer (symbol-prefix-proc 'matrix-modules:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (assert-2d-arrays-identical
         a-list-list result-array ftolerance
         sub-name test-label-index result-hash-table)
  (begin
    (let ((row-dim (length a-list-list))
          (col-dim (length (list-ref a-list-list 0)))
          (res-dim (array-dimensions result-array)))
      (let ((res-row (list-ref res-dim 0))
            (res-col (list-ref res-dim 1))
            (err-1
             (format
              #f "~a : error (~a) : list-list=~a, "
              sub-name test-label-index a-list-list))
            (err-2
             (format
              #f "result=~a, " result-array)))
        (let ((err-3
               (format
                #f "dimensions not equal : shouldbe row/col=~a/~a, "
                row-dim col-dim))
              (err-4
               (format
                #f "result row/col=~a/~a"
                res-row res-col)))
          (begin
            (unittest2:assert?
             (and (equal? row-dim res-row)
                  (equal? col-dim res-col))
             sub-name
             (string-append
              err-1 err-2 err-3 err-4)
             result-hash-table)

            (if (and (equal? row-dim res-row)
                     (equal? col-dim res-col))
                (begin
                  (do ((ii 0 (1+ ii)))
                      ((>= ii row-dim))
                    (begin
                      (let ((srow (list-ref a-list-list ii)))
                        (begin
                          (do ((jj 0 (1+ jj)))
                              ((>= jj col-dim))
                            (begin
                              (let ((s-elem (list-ref srow jj))
                                    (r-elem (array-ref result-array ii jj)))
                                (let ((err-5
                                       (format
                                        #f "shouldbe=~a, result=~a, "
                                        a-list-list result-array))
                                      (err-6
                                       (format
                                        #f "discrepancy at (~a, ~a), "
                                        ii jj))
                                      (err-7
                                       (format
                                        #f "shouldbe=~a, result=~a"
                                        s-elem r-elem)))
                                  (begin
                                    (unittest2:assert?
                                     (< (abs (- s-elem r-elem))
                                        ftolerance)
                                     sub-name
                                     (string-append
                                      err-1 err-5 err-6 err-7)
                                     result-hash-table)
                                    )))
                              ))
                          ))
                      ))
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-1d-arrays-identical
         shouldbe-list result-array ftolerance
         sub-name test-label-index result-hash-table)
  (begin
    (let ((row-dim (length shouldbe-list))
          (res-dim (array-dimensions result-array)))
      (let ((res-row (list-ref res-dim 0))
            (err-1
             (format
              #f "~a : error (~a) : shouldbe-list=~a, "
              sub-name test-label-index shouldbe-list))
            (err-2
             (format
              #f "result=~a, " result-array)))
        (let ((err-3
               (format
                #f "dimensions not equal : shouldbe=~a, result=~a"
                row-dim res-row)))
          (begin
            (unittest2:assert?
             (equal? row-dim res-row)
             sub-name
             (string-append
              err-1 err-2 err-3)
             result-hash-table)

            (if (equal? row-dim res-row)
                (begin
                  (do ((ii 0 (1+ ii)))
                      ((>= ii row-dim))
                    (begin
                      (let ((s-elem (list-ref shouldbe-list ii))
                            (r-elem (array-ref result-array ii)))
                        (let ((err-4
                               (format
                                #f "shouldbe=~a, result=~a, "
                                shouldbe-list result-array))
                              (err-5
                               (format
                                #f "discrepancy at row (~a) : "
                                ii))
                              (err-6
                               (format
                                #f "shouldbe=~a, result=~a"
                                s-elem r-elem)))
                          (begin
                            (unittest2:assert?
                             (<= (abs (- s-elem r-elem)) ftolerance)
                             sub-name
                             (string-append
                              err-1 err-4 err-5 err-6)
                             result-hash-table)
                            )))
                      ))
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (matrix-float-tolerance-check
         shouldbe result
         tolerance
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (<= (abs (- shouldbe result))
               tolerance)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-list-list-array-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-make-list-list-array-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 2) (list 3 4)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list-list (list-ref this-list 0)))
              (let ((result-array
                     (matrix-modules:make-list-list-array
                      a-list-list)))
                (begin
                  (assert-2d-arrays-identical
                   a-list-list result-array ftolerance
                   sub-name test-label-index result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-multiply-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-multiply-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0) (list 0 1))
                 (list (list 1 0) (list 0 1))
                 (list (list 1 0) (list 0 1)))
           (list (list (list 2 0) (list 0 1))
                 (list (list 1 0) (list 0 1))
                 (list (list 2 0) (list 0 1)))
           (list (list (list 1 0) (list 0 2))
                 (list (list 1 0) (list 0 1))
                 (list (list 1 0) (list 0 2)))
           (list (list (list 1 0) (list 0 1))
                 (list (list 1 2) (list 3 4))
                 (list (list 1 2) (list 3 4)))
           (list (list (list 1 1) (list 2 2))
                 (list (list 1 2) (list 3 4))
                 (list (list 4 6) (list 8 12)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (a2-list-list (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((m1 (matrix-modules:make-list-list-array
                         a1-list-list))
                    (m2 (matrix-modules:make-list-list-array
                         a2-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-multiply m1 m2)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-multiply-2 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-multiply-2"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 2 0 0)
                       (list 0 2 0)
                       (list 0 0 2))
                 (list (list 2 4 6)
                       (list 8 10 12)
                       (list 14 16 18)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 30 36 42)
                       (list 66 81 96)
                       (list 102 126 150)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (a2-list-list (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list))
                    (m2
                     (matrix-modules:make-list-list-array
                      a2-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-multiply
                        m1 m2)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-addition-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-addition-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0)
                       (list 0 1))
                 (list (list 2 0)
                       (list 0 2)))
           (list (list (list 1 1)
                       (list 1 1))
                 (list (list 1 2)
                       (list 3 4))
                 (list (list 2 3)
                       (list 4 5)))
           (list (list (list 1 2)
                       (list 3 4))
                 (list (list 1 1)
                       (list 1 1))
                 (list (list 2 3)
                       (list 4 5)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 2 0 0)
                       (list 0 2 0)
                       (list 0 0 2)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 2 2 3)
                       (list 4 6 6)
                       (list 7 8 10)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 2 2 3)
                       (list 4 6 6)
                       (list 7 8 10)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 2 4 6)
                       (list 8 10 12)
                       (list 14 16 18)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (a2-list-list (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list))
                    (m2
                     (matrix-modules:make-list-list-array
                      a2-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-addition m1 m2)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-subtraction-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-subtraction-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0)
                       (list 0 1))
                 (list (list 0 0)
                       (list 0 0)))
           (list (list (list 1 1)
                       (list 1 1))
                 (list (list 1 2)
                       (list 3 4))
                 (list (list 0 -1)
                       (list -2 -3)))
           (list (list (list 1 2)
                       (list 3 4))
                 (list (list 1 1)
                       (list 1 1))
                 (list (list 0 1)
                       (list 2 3)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 0 0 0)
                       (list 0 0 0)
                       (list 0 0 0)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 0 -2 -3)
                       (list -4 -4 -6)
                       (list -7 -8 -8)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 0 2 3)
                       (list 4 4 6)
                       (list 7 8 8)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 (list (list 0 0 0)
                       (list 0 0 0)
                       (list 0 0 0)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (a2-list-list (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list))
                    (m2
                     (matrix-modules:make-list-list-array
                      a2-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-subtraction m1 m2)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-scalar-multiply-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-scalar-multiply-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 2
                 (list (list 2 0)
                       (list 0 2)))
           (list (list (list 1 1)
                       (list 1 1))
                 3
                 (list (list 3 3)
                       (list 3 3)))
           (list (list (list 1 2)
                       (list 3 4))
                 5
                 (list (list 5 10)
                       (list 15 20)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 10
                 (list (list 10 0 0)
                       (list 0 10 0)
                       (list 0 0 10)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 2
                 (list (list 2 4 6)
                       (list 8 10 12)
                       (list 14 16 18)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (scalar (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-scalar-multiply
                        m1 scalar)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-scalar-divide-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-scalar-divide-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 2 0)
                       (list 0 2))
                 2
                 (list (list 1 0)
                       (list 0 1)))
           (list (list (list 3 3)
                       (list 3 3))
                 3
                 (list (list 1 1)
                       (list 1 1)))
           (list (list (list 5 10)
                       (list 15 20))
                 5
                 (list (list 1 2)
                       (list 3 4)))
           (list (list (list 10 0 0)
                       (list 0 10 0)
                       (list 0 0 10))
                 10
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1)))
           (list (list (list 2 4 6)
                       (list 8 10 12)
                       (list 14 16 18))
                 2
                 (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (scalar (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-scalar-divide
                        m1 scalar)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-transpose-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-transpose-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0)
                       (list 0 1)))
           (list (list (list 3 4)
                       (list 5 3))
                 (list (list 3 5)
                       (list 4 3)))
           (list (list (list 5 10)
                       (list 15 20))
                 (list (list 5 15)
                       (list 10 20)))
           (list (list (list 10 0 0)
                       (list 0 10 0)
                       (list 0 0 10))
                 (list (list 10 0 0)
                       (list 0 10 0)
                       (list 0 0 10)))
           (list (list (list 2 4 6)
                       (list 8 10 12)
                       (list 14 16 18))
                 (list (list 2 8 14)
                       (list 4 10 16)
                       (list 6 12 18)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-transpose m1)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-augment-with-identity-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-augment-with-identity-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0 1 0)
                       (list 0 1 0 1)))
           (list (list (list 3 4)
                       (list 5 3))
                 (list (list 3 4 1 0)
                       (list 5 3 0 1)))
           (list (list (list 5 10)
                       (list 15 20))
                 (list (list 5 10 1 0)
                       (list 15 20 0 1)))
           (list (list (list 10 0 0)
                       (list 0 10 0)
                       (list 0 0 10))
                 (list (list 10 0 0 1 0 0)
                       (list 0 10 0 0 1 0)
                       (list 0 0 10 0 0 1)))
           (list (list (list 2 4 6)
                       (list 8 10 12)
                       (list 14 16 18))
                 (list (list 2 4 6 1 0 0)
                       (list 8 10 12 0 1 0)
                       (list 14 16 18 0 0 1)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-augment-with-identity
                        m1)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-switch-two-rows-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-switch-two-rows-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 0 1
                 (list (list 0 1)
                       (list 1 0)))
           (list (list (list 3 4)
                       (list 5 3))
                 0 1
                 (list (list 5 3)
                       (list 3 4)))
           (list (list (list 10 0 0)
                       (list 0 10 0)
                       (list 0 0 10))
                 1 2
                 (list (list 10 0 0)
                       (list 0 0 10)
                       (list 0 10 0)))
           (list (list (list 2 4 6)
                       (list 8 10 12)
                       (list 14 16 18))
                 0 2
                 (list (list 14 16 18)
                       (list 8 10 12)
                       (list 2 4 6)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (row-1 (list-ref this-list 1))
                  (row-2 (list-ref this-list 2))
                  (shouldbe-list-list (list-ref this-list 3)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-switch-two-rows
                        m1 row-1 row-2)))
                  (begin
                    (assert-2d-arrays-identical
                     shouldbe-list-list result-array ftolerance
                     sub-name test-label-index result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-inverse-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-inverse-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0)
                       (list 0 1)))
           (list (list (list 4 7)
                       (list 2 6))
                 (list (list 6/10 -7/10)
                       (list -2/10 4/10)))
           (list (list (list 1 1)
                       (list 1 1))
                 #f)
           (list (list (list 4 3)
                       (list 3 2))
                 (list (list -2 3)
                       (list 3 -4)))
           (list (list (list 2 -1 0)
                       (list -1 2 -1)
                       (list 0 -1 2))
                 (list (list 3/4 1/2 1/4)
                       (list 1/2 1 1/2)
                       (list 1/4 1/2 3/4)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1)))
           (list (list (list 1 0 0)
                       (list 0 0 1)
                       (list 0 1 0))
                 (list (list 1 0 0)
                       (list 0 0 1)
                       (list 0 1 0)))
           ))
         (pivot-tolerance 1e-6)
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result-array
                       (matrix-modules:matrix-inverse
                        m1 pivot-tolerance)))
                  (begin
                    (if (array? result-array)
                        (begin
                          (assert-2d-arrays-identical
                           shouldbe-list-list result-array ftolerance
                           sub-name test-label-index result-hash-table))
                        (begin
                          (let ((err-1
                                 (format
                                  #f "~a : error (~a) : "
                                  sub-name test-label-index))
                                (err-2
                                 (format
                                  #f "shouldbe=~a, result=~a"
                                  shouldbe-list-list
                                  result-array)))
                            (begin
                              (unittest2:assert?
                               (equal? shouldbe-list-list
                                       result-array)
                               sub-name
                               (string-append err-1 err-2)
                               result-hash-table)
                              ))
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-trace-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-trace-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 2)
           (list (list (list 4 7)
                       (list 2 6))
                 10)
           (list (list (list 1 1)
                       (list 1 1))
                 2)
           (list (list (list 4 3)
                       (list 3 2))
                 6)
           (list (list (list 4 3 7)
                       (list 3 2 8))
                 #f)
           (list (list (list 2 -1 0)
                       (list -1 2 -1)
                       (list 0 -1 2))
                 6)
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 3)
           (list (list (list 1 0 0)
                       (list 0 0 1)
                       (list 0 1 0))
                 1)
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result
                       (matrix-modules:matrix-trace m1)))
                  (let ((string-1 "matrix"))
                    (begin
                      (if (and
                           (number? shouldbe)
                           (number? result))
                          (begin
                            (matrix-float-tolerance-check
                             shouldbe result
                             ftolerance
                             string-1 a1-list-list
                             sub-name test-label-index
                             result-hash-table))
                          (begin
                            (matrix-simple-test-check
                             shouldbe result
                             string-1 a1-list-list
                             sub-name test-label-index
                             result-hash-table)
                            ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-reduced-array-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-make-reduced-array-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list 7 8 9))
                 0 0
                 (list (list 5 6)
                       (list 8 9)))
           (list (list (list 1 2 3 4)
                       (list 5 6 7 8)
                       (list 9 10 11 12)
                       (list 13 14 15 16))
                 0 0
                 (list (list 6 7 8)
                       (list 10 11 12)
                       (list 14 15 16)))
           (list (list (list 1 2 3 4)
                       (list 5 6 7 8)
                       (list 9 10 11 12)
                       (list 13 14 15 16))
                 1 2
                 (list (list 1 2 4)
                       (list 9 10 12)
                       (list 13 14 16)))
           (list (list (list 1 2 3 4)
                       (list 5 6 7 8)
                       (list 9 10 11 12)
                       (list 13 14 15 16))
                 2 3
                 (list (list 1 2 3)
                       (list 5 6 7)
                       (list 13 14 15)))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (elim-row (list-ref this-list 1))
                  (elim-col (list-ref this-list 2))
                  (shouldbe-list-list (list-ref this-list 3)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list))
                    (max-rows (length a1-list-list))
                    (max-cols (length (list-ref a1-list-list 0))))
                (let ((result-array
                       (matrix-modules:make-reduced-array
                        m1 max-rows max-cols elim-row elim-col)))
                  (begin
                    (if (array? result-array)
                        (begin
                          (assert-2d-arrays-identical
                           shouldbe-list-list result-array
                           ftolerance
                           sub-name test-label-index
                           result-hash-table))
                        (begin
                          (let ((string-1 "a1-list-list"))
                            (begin
                              (matrix-simple-test-check
                               shouldbe-list-list result-array
                               string-1 a1-list-list
                               sub-name test-label-index
                               result-hash-table)
                              ))
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-matrix-determinant-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-matrix-determinant-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 1)
           (list (list (list 4 7)
                       (list 2 6))
                 10)
           (list (list (list 1 1)
                       (list 1 1))
                 0)
           (list (list (list 4 3)
                       (list 3 2))
                 -1)
           (list (list (list 4 3 7)
                       (list 3 2 8))
                 #f)
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 1)
           (list (list (list 2 -1 0)
                       (list -1 2 -1)
                       (list 0 -1 2))
                 4)
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list -1 3 9))
                 -6)
           (list (list (list 1 2 3 4)
                       (list 5 6 7 8)
                       (list 9 10 11 12)
                       (list 13 14 15 16))
                 0)
           (list (list (list 1 2 3 4)
                       (list -5 6 7 8)
                       (list 9 10 11 12)
                       (list 13 14 -15 16))
                 -4800)
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a1-list-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((m1
                     (matrix-modules:make-list-list-array
                      a1-list-list)))
                (let ((result
                       (matrix-modules:matrix-determinant m1)))
                  (let ((string-1 "matrix"))
                    (begin
                      (if (number? result)
                          (begin
                            (matrix-float-tolerance-check
                             shouldbe result
                             ftolerance
                             string-1 a1-list-list
                             sub-name test-label-index
                             result-hash-table))
                          (begin
                            (matrix-simple-test-check
                             shouldbe result
                             string-1 a1-list-list
                             sub-name test-label-index
                             result-hash-table)
                            ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-vector-dot-product-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-vector-dot-product-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 2)
                 (list 3 7)
                 17)
           (list (list 1 2 3)
                 (list 3 7 2)
                 23)
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu-list (list-ref this-list 0))
                  (vv-list (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((uu (list->array 1 uu-list))
                    (vv (list->array 1 vv-list)))
                (let ((result
                       (matrix-modules:vector-dot-product
                        uu vv)))
                  (let ((string-1
                         (format #f "uu=~a, vv" uu)))
                    (begin
                      (matrix-float-tolerance-check
                       shouldbe result
                       ftolerance
                       string-1 vv
                       sub-name test-label-index
                       result-hash-table)
                      )))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-vector-scalar-product-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-vector-scalar-product-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 0) 1 (list 1 0))
           (list (list 1 0) 2 (list 2 0))
           (list (list 1 2) 3 (list 3 6))
           (list (list 1 2 4) 3 (list 3 6 12))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu-list (list-ref this-list 0))
                  (scalar (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((uu (list->array 1 uu-list)))
                (let ((result-array
                       (matrix-modules:vector-scalar-product
                        uu scalar)))
                  (begin
                    (if (array? result-array)
                        (begin
                          (assert-1d-arrays-identical
                           shouldbe-list result-array ftolerance
                           sub-name test-label-index result-hash-table))
                        (begin
                          (let ((string-1 "scalar"))
                            (begin
                              (matrix-simple-test-check
                               shouldbe-list result-array
                               string-1 scalar
                               sub-name test-label-index
                               result-hash-table)
                              ))
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-vector-gram-schmidt-proj-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-vector-gram-schmidt-proj-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2 2) (list 1 3) (list 2 2))
           (list (list 1 1) (list 7 3) (list 5 5))
           (list (list 2 3) (list 1 1) (list 10/13 15/13))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu-list (list-ref this-list 0))
                  (vv-list (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((uu (list->array 1 uu-list))
                    (vv (list->array 1 vv-list)))
                (let ((result-array
                       (matrix-modules:vector-gram-schmidt-proj
                        uu vv))
                      (string-1 (format #f "uu=~a, vv" uu)))
                  (begin
                    (if (array? result-array)
                        (begin
                          (assert-1d-arrays-identical
                           shouldbe-list result-array ftolerance
                           sub-name test-label-index result-hash-table))
                        (begin
                          (matrix-simple-test-check
                           shouldbe-list result-array
                           string-1 vv
                           sub-name test-label-index
                           result-hash-table)
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-vector-normalize-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-vector-normalize-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 0) (list 1 0))
           (list (list 2 0) (list 1 0))
           (list (list 0 2) (list 0 1))
           (list (list 0 0 2) (list 0 0 1))
           (list (list 0 0 2 0) (list 0 0 1 0))
           (list (list 3 4) (list 3/5 4/5))
           ))
         (ftolerance 1e-9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((uu (list->array 1 uu-list)))
                (let ((result-array
                       (matrix-modules:vector-normalize uu))
                      (string-1 "uu"))
                  (begin
                    (if (array? result-array)
                        (begin
                          (assert-1d-arrays-identical
                           shouldbe-list result-array ftolerance
                           sub-name test-label-index result-hash-table))
                        (begin
                          (matrix-simple-test-check
                           shouldbe-list result-array
                           string-1 uu
                           sub-name test-label-index
                           result-hash-table)
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mgs-normalized-vector-list-columns-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-mgs-normalized-vector-list-columns-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0)
                       (list 0 1)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1)))
           (list (list (list 12 -51 4)
                       (list 6 167 -68)
                       (list -4 24 -41))
                 (list (list 6/7 3/7 -2/7)
                       (list -69/175 158/175 6/35)
                       (list -58/175 6/175 -33/35)))
           (list (list (list 1 1 0 2)
                       (list -1 -1 0 -2))
                 (list (list 0.7071067811865475 -0.7071067811865475)
                       (list 0 0)))
           (list (list (list 1 -1)
                       (list 1 -1)
                       (list 0 0)
                       (list 2 -2))
                 (list (list 0.4082482904638631
                             0.4082482904638631
                             0.0
                             0.8164965809277261)
                       (list 0 0 0 0)))
           ))
         (ftolerance 1e-6)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((result-list
                       (matrix-modules:mgs-normalized-vector-list-columns
                        aa-array ftolerance)))
                  (begin
                    (if (list? result-list)
                        (begin
                          (let ((slen (length shouldbe-list-list))
                                (rlen (length result-list)))
                            (let ((err-1
                                   (format
                                    #f "~a : error (~a) : "
                                    sub-name test-label-index))
                                  (err-2
                                   (format
                                    #f "shouldbe=~a, result=~a, "
                                    shouldbe-list-list result-list))
                                  (err-3
                                   (format
                                    #f "length shouldbe=~a, result=~a"
                                    slen rlen)))
                              (begin
                                (unittest2:assert?
                                 (equal? slen rlen)
                                 sub-name
                                 (string-append
                                  err-1 err-2 err-3)
                                 result-hash-table)

                                (if (equal? slen rlen)
                                    (begin
                                      (do ((ii 0 (1+ ii)))
                                          ((>= ii slen))
                                        (begin
                                          (let ((slist (list-ref shouldbe-list-list ii))
                                                (rarray (list-ref result-list ii)))
                                            (begin
                                              (assert-1d-arrays-identical
                                               slist rarray ftolerance
                                               sub-name test-label-index
                                               result-hash-table)
                                              ))
                                          ))
                                      ))
                                ))
                            ))
                        (begin
                          (let ((string-1 "aa-list-list"))
                            (begin
                              (matrix-simple-test-check
                               shouldbe-list-list result-list
                               string-1 aa-list-list
                               sub-name test-label-index
                               result-hash-table)
                              ))
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mgs-calc-q-columns-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-mgs-calc-q-columns-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0)
                       (list 0 1)))
           (list (list (list 3 2)
                       (list 1 2))
                 (list (list 0.948683298 -0.316227766)
                       (list 0.316227766 0.948683298)))
           (list (list (list 1 2)
                       (list 3 4))
                 (list (list 0.316227766017  0.948683298051)
                       (list 0.948683298051 -0.316227766017)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1)))
           (list (list (list 12 -51 4)
                       (list 6 167 -68)
                       (list -4 24 -41))
                 (list (list 6/7 -69/175 -58/175)
                       (list 3/7 158/175 6/175)
                       (list -2/7 6/35 -33/35)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list -1 3 9))
                 (list (list 0.235702260  0.222952018 -0.945905303)
                       (list 0.942809042  0.183607544  0.278207442)
                       (list -0.235702260  0.957382195  0.166924465)))
           (list (list (list 1 1 0 2)
                       (list -1 -1 0 -2))
                 (list (list 0.7071067811865475 0)
                       (list -0.7071067811865475 0)))
           (list (list (list 1 -1)
                       (list 1 -1)
                       (list 0 0)
                       (list 2 -2))
                 (list (list 0.4082482904638631 0)
                       (list 0.4082482904638631 0)
                       (list 0.0 0)
                       (list 0.8164965809277261 0)))
           ))
         (ftolerance 1e-6)
         (qtolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((result-array
                       (matrix-modules:mgs-calc-q-columns
                        aa-array qtolerance)))
                  (begin
                    (if (array? result-array)
                        (begin
                          (assert-2d-arrays-identical
                           shouldbe-list-list result-array
                           ftolerance
                           sub-name test-label-index
                           result-hash-table))
                        (begin
                          (let ((string-1 "aa-list-list"))
                            (begin
                              (matrix-simple-test-check
                               shouldbe-list-list result-array
                               string-1 aa-list-list
                               sub-name test-label-index
                               result-hash-table)
                              ))
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mgs-calc-r-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-mgs-calc-r-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 (list (list 1 0)
                       (list 0 1)))
           (list (list (list 1 2)
                       (list 3 4))
                 (list (list 3.162277660168 4.427188724236)
                       (list 0.000000000000 0.632455532034)))
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1)))
           (list (list (list 12 -51 4)
                       (list 6 167 -68)
                       (list -4 24 -41))
                 (list (list 14 21 -14)
                       (list 0 175 -70)
                       (list 0 0 35)))
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list -1 3 9))
                 (list (list 4.242640687  4.478342948  4.242640687)
                       (list 0.000000000  4.236088342 10.386941075)
                       (list 0.000000000  0.000000000  0.333848930)))
           ))
         (ftolerance 1e-6)
         (qtolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((qq-array
                       (matrix-modules:mgs-calc-q-columns
                        aa-array qtolerance)))
                  (begin
                    (if (array? qq-array)
                        (begin
                          (let ((result-array
                                 (matrix-modules:mgs-calc-r
                                  aa-array qq-array)))
                            (begin
                              (if (array? result-array)
                                  (begin
                                    (assert-2d-arrays-identical
                                     shouldbe-list-list result-array
                                     ftolerance
                                     sub-name test-label-index
                                     result-hash-table))
                                  (begin
                                    (let ((string-1 "aa-list-list"))
                                      (begin
                                        (matrix-simple-test-check
                                         shouldbe-list-list result-array
                                         string-1 aa-list-list
                                         sub-name test-label-index
                                         result-hash-table)
                                        ))
                                    ))
                              )))
                        (begin
                          (display
                           (format
                            #f "~a error(~a) : cannot construct "
                            sub-name test-label-index))
                          (display
                           (format
                            #f "Q array from matrix ~a~%"
                            aa-array))
                          (force-output)
                          ))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mgs-matrix-rank-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-mgs-matrix-rank-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 1 0)
                       (list 0 1))
                 2)
           (list (list (list 1 2)
                       (list 3 4))
                 2)
           (list (list (list 1 0 0)
                       (list 0 1 0)
                       (list 0 0 1))
                 3)
           (list (list (list 12 -51 4)
                       (list 6 167 -68)
                       (list -4 24 -41))
                 3)
           (list (list (list 1 2 3)
                       (list 4 5 6)
                       (list -1 3 9))
                 3)
           (list (list (list 1 2 1)
                       (list -2 -3 1)
                       (list 3 5 0))
                 2)
           (list (list (list 1 1 0 2)
                       (list -1 -1 0 -2))
                 1)
           (list (list (list 1 -1)
                       (list 1 -1)
                       (list 0 0)
                       (list 2 -2))
                 1)
           ))
         (rtolerance 1e-12)
         (debug-flag #f)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (shouldbe-rank (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((qq-array
                       (matrix-modules:mgs-calc-q-columns
                        aa-array rtolerance)))
                  (let ((rr-array
                         (matrix-modules:mgs-calc-r
                          aa-array qq-array)))
                    (let ((result-rank
                           (matrix-modules:mgs-matrix-rank
                            rr-array rtolerance)))
                      (let ((string-1 "array"))
                        (begin
                          (if (equal? debug-flag #t)
                              (begin
                                (display
                                 (format #f "debug~%"))
                                (display
                                 (format #f "aa-array~%"))
                                (matrix-modules:matrix-display aa-array)
                                (display
                                 (format #f "qq-array~%"))
                                (matrix-modules:matrix-display qq-array)
                                (display
                                 (format #f "rr-array~%"))
                                (matrix-modules:matrix-display rr-array)
                                (let ((tmp-array
                                       (matrix-modules:matrix-multiply
                                        qq-array rr-array)))
                                  (begin
                                    (display
                                     (format #f "qq-array x rr-array~%"))
                                    (matrix-modules:matrix-display tmp-array)
                                    ))
                                ))

                          (matrix-simple-test-check
                           shouldbe-rank result-rank
                           string-1 aa-array
                           sub-name test-label-index
                           result-hash-table)
                          )))
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
