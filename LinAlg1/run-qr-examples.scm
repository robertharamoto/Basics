#! /usr/bin/guile \
--no-auto-compile --debug -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  run some qr examples                                 ###
;;;###                                                       ###
;;;###  last updated August 5, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  modules                                              ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "tests"))

(set! %load-compiled-path
      (append %load-compiled-path (list "." "objects")))

;;;#############################################################
;;;#############################################################
;;;### getopt-long used for command-line option arguments processing
(use-modules ((ice-9 getopt-long)
              :renamer (symbol-prefix-proc 'ice-9-getopt:)))

;;;### ice-9-format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((matrix-modules)
              :renamer (symbol-prefix-proc 'matrix-modules:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin example subroutines                            ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define-syntax display-integer-with-text-macro
  (syntax-rules ()
    ((display-integer-with-text-macro init-text num)
     (begin
       (if (integer? num)
           (begin
             (display
              (ice-9-format:format
               #f "~a = ~:d~%" init-text num)))
           (begin
             (display
              (format
               #f "~a = ~a~%" init-text num))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax run-mgs-qr-examples
  (syntax-rules ()
    ((run-mgs-qr-examples
      aa-array msg-calc-q-func qr-tolerance)
     (begin
       (let ((qq-array
              (msg-calc-q-func aa-array qr-tolerance)))
         (let ((rr-array
                (matrix-modules:mgs-calc-r aa-array qq-array)))
           (let ((result-rank
                  (matrix-modules:mgs-matrix-rank
                   rr-array qr-tolerance)))
             (begin
               (display (format #f "aa-array~%"))
               (matrix-modules:matrix-display aa-array)
               (display (format #f "qq-array~%"))
               (matrix-modules:matrix-display qq-array)
               (display (format #f "rr-array~%"))
               (matrix-modules:matrix-display rr-array)
               (let ((tmp-array
                      (matrix-modules:matrix-multiply
                       qq-array rr-array)))
                 (begin
                   (display (format #f "qq-array x rr-array~%"))
                   (matrix-modules:matrix-display tmp-array)
                   ))
               (let ((tmp-array
                      (matrix-modules:matrix-multiply
                       qq-array
                       (matrix-modules:matrix-transpose qq-array))))
                 (begin
                   (display (format #f "qq-array x qq-array^T~%"))
                   (matrix-modules:matrix-display tmp-array)
                   ))

               (display-integer-with-text-macro
                "rank" result-rank)
               (let ((aa-dim-list (array-dimensions aa-array)))
                 (let ((aa-rows (list-ref aa-dim-list 0))
                       (aa-cols (list-ref aa-dim-list 1)))
                   (begin
                     (if (and (equal? aa-rows aa-cols)
                              (equal? aa-rows result-rank))
                         (begin
                           (let ((aa-trace
                                  (matrix-modules:matrix-trace aa-array))
                                 (aa-det
                                  (matrix-modules:matrix-determinant aa-array)))
                             (begin
                               (display-integer-with-text-macro
                                "trace" aa-trace)
                               (display-integer-with-text-macro
                                "determinant" aa-det)
                               ))
                           ))
                     )))
               (force-output)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (example-mgs-qr-1)
  (begin
    (let ((sub-name
           (format #f "~a:example-mgs-qr-1"
                   (utils-module:get-basename (current-filename))))
          (test-list
           (list
            (list (list 1 0)
                  (list 0 1))
            (list (list 1 2)
                  (list 3 4))
            (list (list 3 2)
                  (list 1 2))
            (list (list 1 0 0)
                  (list 0 1 0)
                  (list 0 0 1))
            (list (list 1 2 3)
                  (list 4 5 6)
                  (list -1 3 9))
            (list (list 12 -51 4)
                  (list 6 167 -68)
                  (list -4 24 -41))
            (list (list 1 1 0 2)
                  (list -1 -1 0 -2))
            (list (list 1 -1)
                  (list 1 -1)
                  (list 0 0)
                  (list 2 -2))
            ))
          (separator-line (make-string 64 #\=))
          (qr-tolerance 1e-12)
          (test-label-index 0))
      (begin
        (display
         (format
          #f "Examples for the qr decomposition using the"))
        (display
         (format
          #f " modified gram-schmidt method.~%"))
        (display
         (format
          #f "aa-array is the original matrix, qq-array contains"))
        (display
         (format
          #f " the orthonormal basis,~%"))
        (display
         (format
          #f "rr-array contains the upper triangular matrix~%"))
        (display
         (format #f "note: qq-array x rr-array = aa-array~%"))
        (display
         (format #f "      qq-array x qq-array^Transpose"))
        (display
         (format #f " = identity matrix if the rank is full.~%"))
        (force-output)

        (for-each
         (lambda (aa-list-list)
           (begin
             (let ((aa-array (list->array 2 aa-list-list)))
               (begin
                 (display
                  (format #f "~a~%" separator-line))
                 (force-output)
                 (run-mgs-qr-examples
                  aa-array matrix-modules:mgs-calc-q-columns
                  qr-tolerance)
                 ))

             (set! test-label-index (1+ test-label-index))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main code                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help ospec title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (display (format #f "options available~%"))
      (for-each
       (lambda (llist)
         (begin
           (display
            (format
             #f "  --~a, -~a~%"
             (car llist) (cadr (cadr llist))))
           )) ospec)
      (force-output)
      ))
  (begin
    (let ((version-string "2024-08-05"))
      (let ((title-string
             (format
              #f "Simple matrix algebra module examples (version ~a)"
              version-string))
            (option-spec
             (list (list 'version '(single-char #\v) '(value #f))
                   (list 'help '(single-char #\h) '(value #f))
                   (list 'details '(single-char #\d) '(value #f))
                   )))
        (let ((options
               (ice-9-getopt:getopt-long args option-spec))
              (separator-line (make-string 64 #\=)))
          (begin
            (let ((help-flag
                   (ice-9-getopt:option-ref options 'help #f)))
              (begin
                (if (equal? help-flag #t)
                    (begin
                      (local-display-help option-spec title-string)
                      (quit)
                      ))
                ))

            (let ((version-flag
                   (ice-9-getopt:option-ref
                    options 'version #f)))
              (begin
                (if (equal? version-flag #t)
                    (begin
                      (local-display-version title-string)
                      (quit)
                      ))
                ))

            (newline)
            (timer-module:time-code-macro
             (begin
               (display
                (format #f "~a~%" separator-line))
               (force-output)
               (example-mgs-qr-1)
               (display
                (format #f "~a~%" separator-line))
               (newline)
               (force-output)
               ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
