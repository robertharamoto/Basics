;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for monopoly-module.scm                   ###
;;;###                                                       ###
;;;###  last updated December 4, 2024                        ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;###
;;;###  created July 20, 2024
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### srfi-19 used for time/date library
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 copy-tree for copy-tree
(use-modules ((ice-9 copy-tree)
              :renamer (symbol-prefix-proc 'ice-9-copy-tree:)))

(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((monopoly-module)
              :renamer (symbol-prefix-proc 'monopoly-module:)))

;;;#############################################################
;;;#############################################################
;;; (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-update-position-data-1 result-hash-table)
 (begin
   (let ((sub-name "test-update-position-data-1")
         (test-list
          (list
           (list 0 1 1)
           (list 1 2 2)
           (list 2 3 3)
           (list 3 1 1)
           ))
         (test-label-index 0)
         (board-list (list))
         (board-desc-htable (make-hash-table 100))
         (single-board-sum-htable (make-hash-table 100))
         (board-sum-htable (make-hash-table 100))
         (board-s2-htable (make-hash-table 100)))
     (begin
       (monopoly-module:initialize-list-data
        board-desc-htable board-sum-htable
        board-s2-htable)

       (monopoly-module:generate-board-list-macro
        board-desc-htable board-list)

       (for-each
        (lambda (a-list)
          (begin
            (let ((position (list-ref a-list 0))
                  (max-apply (list-ref a-list 1))
                  (shouldbe (list-ref a-list 2)))
              (begin
                (monopoly-module:initialize-single-board-sum
                 board-list single-board-sum-htable)

                (do ((ii 0 (1+ ii)))
                    ((>= ii max-apply))
                  (begin
                    (monopoly-module:update-position-data-macro
                     single-board-sum-htable position)
                    ))

                (let ((result
                       (hash-ref
                        single-board-sum-htable
                        position 0)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : position=~a, "
                          sub-name test-label-index position))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-top-card-to-bottom-1 result-hash-table)
 (begin
   (let ((sub-name "test-top-card-to-bottom-1")
         (test-list
          (list
           (list
            (list (list 15 -1 "nop")
                  (list 1 0 "go")
                  (list 2 10 "jail"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 15 -1 "nop"))
            )))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((test-deck (list-ref a-list 0))
                  (shouldbe-deck (list-ref a-list 1)))
              (let ((old-test-deck
                     (ice-9-copy-tree:copy-tree test-deck)))
                (begin
                  (monopoly-module:top-card-to-bottom-macro
                   test-deck)

                  (let ((err-1
                         (format
                          #f "~a : error (~a) : old-test-deck=~a, "
                          sub-name test-label-index old-test-deck))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe-deck test-deck)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-deck test-deck)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))


;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-find-next-position-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-next-position-1")
         (test-list
          (list
           (list 0 "r" 5) (list 1 "r" 5)
           (list 2 "r" 5) (list 3 "r" 5)
           (list 4 "r" 5) (list 5 "r" 15)
           (list 16 "r" 25) (list 26 "r" 35)
           (list 36 "r" 5) (list 39 "r" 5)
           (list 1 "u" 12) (list 11 "u" 12)
           (list 12 "u" 28) (list 29 "u" 12)
           ))
         (test-label-index 0)
         (board-list (list))
         (board-desc-htable (make-hash-table 100))
         (board-sum-htable (make-hash-table 100))
         (board-s2-htable (make-hash-table 100)))
     (begin
       (monopoly-module:initialize-list-data
        board-desc-htable board-sum-htable
        board-s2-htable)

       (monopoly-module:generate-board-list-macro
        board-desc-htable board-list)

       (for-each
        (lambda (a-list)
          (begin
            (let ((curr-pos (list-ref a-list 0))
                  (code-string (list-ref a-list 1))
                  (shouldbe-position (list-ref a-list 2)))
              (let ((result-position
                     (monopoly-module:find-next-position
                      curr-pos code-string)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "curr-pos=~a, code-string=~a, "
                        curr-pos code-string))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-position result-position)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-position
                             result-position)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-handle-community-chest-1 result-hash-table)
 (begin
   (let ((sub-name "test-handle-community-chest-1")
         (test-list
          (list
           (list
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            0 5 0
            0 0 0 1)
           (list
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 5 0
            0 0 0 1)
           (list
            (list (list 2 10 "jail")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            1 7 1
            10 0 1 0)
           (list
            (list (list 2 10 "jail")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            12 18 1
            10 0 1 0)
           ))
         (test-label-index 0)
         (orbit-counter 0)
         (board-list (list))
         (board-desc-htable (make-hash-table 100))
         (single-board-sum-htable (make-hash-table 100))
         (board-sum-htable (make-hash-table 100))
         (board-s2-htable (make-hash-table 100)))
     (begin
       (monopoly-module:initialize-list-data
        board-desc-htable board-sum-htable
        board-s2-htable)

       (monopoly-module:generate-board-list-macro
        board-desc-htable board-list)

       (for-each
        (lambda (a-list)
          (begin
            (let ((cc-deck (list-ref a-list 0))
                  (position-counter (list-ref a-list 1))
                  (next-position (list-ref a-list 2))
                  (consecutive-doubles-count (list-ref a-list 3))
                  (shouldbe-position (list-ref a-list 4))
                  (shouldbe-consecutive-doubles-count (list-ref a-list 5))
                  (shouldbe-position-count (list-ref a-list 6))
                  (shouldbe-board-count (list-ref a-list 7)))
              (begin
                (monopoly-module:initialize-single-board-sum
                 board-list single-board-sum-htable)

                (monopoly-module:initialize-single-board-sum
                 board-list board-sum-htable)

                (monopoly-module:handle-community-chest-macro
                 board-list
                 single-board-sum-htable
                 board-sum-htable board-s2-htable
                 position-counter next-position
                 orbit-counter
                 consecutive-doubles-count
                 cc-deck)

                (let ((result-position-count
                       (hash-ref
                        single-board-sum-htable
                        shouldbe-position 0))
                      (result-board-count
                       (hash-ref
                        board-sum-htable
                        shouldbe-position 0)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : cc-deck=~a : "
                          sub-name test-label-index cc-deck))
                        (err-2
                         (format
                          #f "shouldbe-position=~a, result position=~a, "
                          shouldbe-position position-counter))
                        (err-3
                         (format
                          #f "shouldbe-consecutive-doubles-count=~a, "
                          shouldbe-consecutive-doubles-count))
                        (err-4
                         (format
                          #f "result-consecuritve-doubles-count=~a"
                          consecutive-doubles-count))
                        (err-5
                         (format
                          #f "shouldbe-position count=~a, result count=~a"
                          shouldbe-position-count
                          result-position-count))
                        (err-6
                         (format
                          #f "shouldbe-board count=~a, result count=~a"
                          shouldbe-board-count
                          result-board-count)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-position
                               position-counter)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? shouldbe-consecutive-doubles-count
                               consecutive-doubles-count)
                       sub-name
                       (string-append
                        err-1 err-2 err-3 err-4)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? shouldbe-position-count
                               result-position-count)
                       sub-name
                       (string-append
                        err-1 err-2 err-5)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? shouldbe-board-count
                               result-board-count)
                       sub-name
                       (string-append
                        err-1 err-2 err-6)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-handle-chance-1 result-hash-table)
 (begin
   (let ((sub-name "test-handle-chance-1")
         (test-list
          (list
            ;;; test 0
           (list
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            0 5 0
            0 0 0 1)
           (list
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 12 0
            0 0 0 1)
           (list
            (list (list 2 10 "jail")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 12 1
            10 0 1 0)
           (list
            (list (list 2 10 "jail")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            12 20 1
            10 0 1 0)
           (list
            (list (list 3 11 "c1")
                  (list 7 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 10 0
            11 0 1 0)
            ;;; test 5
           (list
            (list (list 3 11 "c1")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            12 15 0
            11 0 1 1)
           (list
            (list (list 4 24 "e3")
                  (list 7 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 15 0
            24 0 1 0)
           (list
            (list (list 4 24 "e3")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            25 35 0
            24 0 1 1)
           (list
            (list (list 5 39 "h2")
                  (list 7 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 5 0
            39 0 1 0)
           (list
            (list (list 5 39 "h2")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            39 5 0
            39 0 1 0)
            ;;; test 10
           (list
            (list (list 6 5 "r1")
                  (list 7 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 6 0
            5 0 1 0)
           (list
            (list (list 6 5 "r1")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            6 7 0
            5 0 1 1)
           (list
            (list (list 7 -1 "r")
                  (list 7 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 8 0
            5 0 1 0)
           (list
            (list (list 7 -1 "r")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            6 9 0
            15 0 1 0)
           (list
            (list (list 7 -1 "r")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            16 10 0
            25 0 1 0)
            ;;; test 15
           (list
            (list (list 7 -1 "r")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            26 31 0
            35 0 1 0)
           (list
            (list (list 8 -1 "r")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            36 38 0
            5 0 1 0)
           (list
            (list (list 9 -1 "u")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            1 39 0
            12 0 1 0)
           (list
            (list (list 9 -1 "u")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            13 23 0
            28 0 1 0)
           (list
            (list (list 9 -1 "u")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            29 33 0
            12 0 1 0)
            ;;; test 20
           (list
            (list (list 10 -3 "back 3")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            5 7 0
            4 0 1 0)
           (list
            (list (list 10 -3 "back 3")
                  (list 3 -1 "nop")
                  (list 1 0 "go"))
            (list (list 1 0 "go")
                  (list 2 10 "jail")
                  (list 3 -1 "nop"))
            39 7 0
            4 0 1 0)
           ))
         (test-label-index 0)
         (orbit-counter 0)
         (board-list (list))
         (board-desc-htable (make-hash-table 100))
         (single-board-sum-htable (make-hash-table 100))
         (board-sum-htable (make-hash-table 100))
         (board-s2-htable (make-hash-table 100)))
     (begin
       (monopoly-module:initialize-list-data
        board-desc-htable board-sum-htable
        board-s2-htable)

       (monopoly-module:generate-board-list-macro
        board-desc-htable board-list)

       (let ((board-length (length board-list)))
         (begin
           (for-each
            (lambda (a-list)
              (begin
                (let ((chance-deck (list-ref a-list 0))
                      (cc-deck (list-ref a-list 1))
                      (position-counter (list-ref a-list 2))
                      (next-position (list-ref a-list 3))
                      (consecutive-doubles-count (list-ref a-list 4))
                      (shouldbe-position (list-ref a-list 5))
                      (shouldbe-consecutive-doubles-count (list-ref a-list 6))
                      (shouldbe-position-count (list-ref a-list 7))
                      (shouldbe-board-count (list-ref a-list 8)))
                  (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "chance-deck=~a, cc-deck=~a, "
                        chance-deck cc-deck)))
                  (begin
                    (monopoly-module:initialize-single-board-sum
                     board-list single-board-sum-htable)

                    (monopoly-module:initialize-single-board-sum
                     board-list board-sum-htable)

                    (monopoly-module:handle-chance-macro
                     board-list board-length
                     single-board-sum-htable
                     board-sum-htable board-s2-htable
                     position-counter next-position
                     orbit-counter
                     consecutive-doubles-count
                     chance-deck cc-deck)

                    (let ((err-3
                           (format
                            #f "shouldbe-position=~a, result=~a"
                            shouldbe-position position-counter)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-position
                                 position-counter)
                         sub-name
                         (string-append
                          err-1 err-2 err-3)
                         result-hash-table)
                        ))

                    (let ((err-4
                           (format
                            #f "shouldbe-consecutive-doubles-count=~a, "
                            shouldbe-consecutive-doubles-count))
                          (err-5
                           (format
                            #f "result count=~a"
                            consecutive-doubles-count)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-consecutive-doubles-count
                                 consecutive-doubles-count)
                         sub-name
                         (string-append
                          err-1 err-2 err-4 err-5)
                         result-hash-table)
                        ))

                    (let ((result-position-count
                           (hash-ref
                            single-board-sum-htable
                            shouldbe-position 0)))
                      (let ((err-5
                             (format
                              #f "shouldbe=~a, result=~a"
                              shouldbe-position-count
                              result-position-count)))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-position-count
                                   result-position-count)
                           sub-name
                           (string-append
                            err-1 err-2 err-5)
                           result-hash-table)
                          )))

                    (let ((result-board-count
                           (hash-ref
                            board-sum-htable
                            shouldbe-position 0)))
                      (let ((err-6
                             (format
                              #f "shouldbe board count=~a, result=~a"
                              shouldbe-board-count
                              result-board-count)))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-board-count
                                   result-board-count)
                           sub-name
                           (string-append
                            err-1 err-2 err-6)
                           result-hash-table)
                          )))
                    )))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
           ))
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-calculate-std-deviation-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-std-deviation-1")
         (test-list
          (list
           (list
            10
            (list 0 1 2)
            (list
             (list 0 "a1" 2)
             (list 1 "a2" 3)
             (list 2 "a3" 4))
            (list
             (list 0 "a1" 4)
             (list 1 "a2" 6)
             (list 2 "a3" 10))
            (list 0.60 0.71414 0.9165)
            )))
         (test-label-index 0)
         (tolerance 0.00010)
         (board-sum-htable (make-hash-table 100))
         (board-s2-htable (make-hash-table 100))
         (std-deviation-htable (make-hash-table 100)))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((num-orbits (list-ref a-list 0))
                  (board-list (list-ref a-list 1))
                  (board-sum-list (list-ref a-list 2))
                  (board-s2-list (list-ref a-list 3))
                  (shouldbe-stddev-list (list-ref a-list 4))
                  (err-1
                   (format
                    #f "~a : error (~a) : "
                    sub-name test-label-index)))
              (let ((board-length (length board-list)))
                (begin
                  (hash-clear! board-sum-htable)
                  (hash-clear! board-s2-htable)
                  (hash-clear! std-deviation-htable)
                  (do ((ii 0 (1+ ii)))
                      ((>= ii board-length))
                    (begin
                      (let ((position
                             (list-ref board-list ii))
                            (sum-ii-list
                             (list-ref board-sum-list ii))
                            (s2-ii-list
                             (list-ref board-s2-list ii)))
                        (let ((bsum (list-ref sum-ii-list 2))
                              (b2sum (list-ref s2-ii-list 2)))
                          (begin
                            (hash-set!
                             board-sum-htable
                             position bsum)
                            (hash-set!
                             board-s2-htable
                             position b2sum)
                            )))
                      ))

                (monopoly-module:calc-std-deviation
                 board-list board-sum-htable
                 board-s2-htable std-deviation-htable
                 num-orbits)

                (do ((ii 0 (1+ ii)))
                    ((>= ii board-length))
                  (begin
                    (let ((position
                           (list-ref board-list ii)))
                      (let ((result-std-dev
                             (hash-ref
                              std-deviation-htable
                              position 0))
                            (shouldbe-std-dev
                             (list-ref
                              shouldbe-stddev-list ii)))
                        (let ((err-2
                               (format
                                #f "board-list=~a, num-orbits=~a, "
                                board-list num-orbits))
                              (err-3
                               (format
                                #f "shouldbe-std-dev=~a, result=~a"
                                shouldbe-std-dev result-std-dev)))
                          (begin
                            (unittest2:assert?
                             (<= (abs
                                  (- shouldbe-std-dev
                                     result-std-dev))
                                 tolerance)
                             sub-name
                             (string-append
                              err-1 err-2 err-3)
                             result-hash-table)
                            ))
                        ))
                    ))
                )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-top-three-list-to-number-code-1 result-hash-table)
 (begin
   (let ((sub-name "test-top-three-list-to-number-code-1")
         (test-list
          (list
           (list
            (list
             (list 5 5)
             (list 2 4)
             (list 1 3))
            "050201")
           (list
            (list
             (list 7 5)
             (list 8 4)
             (list 22 3))
            "070822")))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((three-list (list-ref a-list 0))
                  (shouldbe-string (list-ref a-list 1)))
              (let ((result-string
                     (monopoly-module:top-three-list-to-number-code
                      three-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : three-list=~a, "
                        sub-name test-label-index three-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe-string
                                  result-string)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
