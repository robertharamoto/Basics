;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  read-in configuration files                          ###
;;;###                                                       ###
;;;###  last updated September 26, 2024                      ###
;;;###                                                       ###
;;;###  updated May 4, 2022                                  ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start config modules

(define-module (config-module)
  #:export (read-config-file
            write-config-file
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### read/write functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### advanced format function
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### utils-module for commafy-number function
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main functions                                 ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (process-input-line sline output-htable comment-htable)
  (begin
    (let ((sarray (string-split sline #\=)))
      (let ((key (string-trim-both (list-ref sarray 0)))
            (rest-of-line (string-trim-both (list-ref sarray 1))))
        (let ((semi-index (string-index rest-of-line #\;))
              (vstring rest-of-line)
              (comment rest-of-line))
          (begin
            (if (not (equal? semi-index #f))
                (begin
                  (let ((tmp-value
                         (substring rest-of-line 0 semi-index))
                        (tmp-comment
                         (substring rest-of-line semi-index)))
                    (begin
                      (set! vstring
                            (string-trim-both tmp-value))
                      (set! comment
                            (string-trim-both tmp-comment))
                      (hash-set!
                       comment-htable key comment)
                      )))
                (begin
                  (set! vstring
                        (string-trim-both rest-of-line))
                  (set! comment #f)
                  ))

            (set!
             vstring
             (string-trim-both
              (ice-9-regex:regexp-substitute/global
               #f "," vstring 'pre "" 'post)))

            (let ((val-1 (string->number vstring)))
              (begin
                (if (not (equal? val-1 #f))
                    (begin
                      (let ((nvalue (inexact->exact val-1)))
                        (begin
                          (hash-set! output-htable key nvalue)
                          )))
                    (begin
                      (hash-set! output-htable key vstring)
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (read-config-file filename output-htable comment-htable)
  (begin
    (hash-clear! output-htable)
    (hash-clear! comment-htable)

    (if (file-exists? filename)
        (begin
          (with-input-from-file filename
            (lambda ()
              (begin
                (let ((comment-regex
                       (make-regexp "^[ \t;]*;" regexp/extended))
                      (empty-line-regex
                       (make-regexp "^$"))
                      (valid-line-regex
                       (make-regexp "[=]")))
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-line)
                          (ice-9-rdelim:read-line)))
                        ((eof-object? line))
                      (begin
                        (if (and (not (eof-object? line))
                                 (not (regexp-exec comment-regex line))
                                 (not (regexp-exec empty-line-regex line))
                                 (regexp-exec valid-line-regex line))
                            (begin
                              (process-input-line
                               line output-htable comment-htable)
                              ))
                        ))
                    ))
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-single-line-comment-string-macro
  (syntax-rules ()
    ((display-single-line-comment-string-macro
      var-comment var-length init-short-comment
      short-comment)
     (begin
       (let ((var-clen
              (string-length var-comment))
             (local-comment var-comment)
             (max-allowed-len (- var-length 10)))
         (begin
           (if (not (equal? var-clen #f))
               (begin
                 (if (> var-clen max-allowed-len)
                     (begin
                       (set! local-comment
                             (substring
                              var-comment 0 max-allowed-len))
                       (set! var-clen
                             (string-length local-comment))
                       ))

                 (let ((var-space-len
                        (- max-allowed-len var-clen)))
                   (begin
                     (if (> var-space-len 0)
                         (begin
                           (let ((spacer-string
                                  (make-string
                                   var-space-len #\space)))
                             (begin
                               (display
                                (format
                                 #f "~a  ~a~a  ~a~%"
                                 init-short-comment
                                 local-comment
                                 spacer-string
                                 short-comment))
                               )))
                         (begin
                           (display
                            (format
                             #f "~a  ~a  ~a~%"
                             init-short-comment local-comment
                             short-comment))
                           ))
                     )))
               (begin
                 (let ((spacer-string
                        (make-string
                         (- var-length 10) #\space)))
                   (begin
                     (display
                      (format
                       #f "~a  ~a  ~a~%"
                       init-short-comment
                       spacer-string
                       short-comment))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-boxed-2-strings-macro
  (syntax-rules ()
    ((display-boxed-2-strings-macro
      separator-comment-string
      init-short-comment
      short-comment
      comment-line-1-string comment-line-2-string
      comment-length)
     (begin
       (let ((blank-string " "))
         (begin
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))

           (display-single-line-comment-string-macro
            blank-string comment-length
            init-short-comment short-comment)

           (display-single-line-comment-string-macro
            comment-line-1-string comment-length
            init-short-comment short-comment)

           (display-single-line-comment-string-macro
            blank-string comment-length
            init-short-comment short-comment)

           (display-single-line-comment-string-macro
            comment-line-2-string comment-length
            init-short-comment short-comment)

           (display-single-line-comment-string-macro
            blank-string comment-length
            init-short-comment short-comment)

           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-header-block)
  (begin
    (let ((max-clen 61))
      (let ((init-short-comment ";;;###")
            (pound-sign-comment-string
             (make-string (- max-clen 3) #\#))
            (short-comment "###"))
        (let ((separator-comment-string
               (format
                #f "~a~a"
                init-short-comment
                pound-sign-comment-string))
              (comment-line-1-string "configuration file")
              (comment-line-2-string
               (format #f "last updated ~a"
                       (srfi-19:date->string
                        (srfi-19:current-date)
                        "~A, ~B ~d, ~Y"))))
          (begin
            (display-boxed-2-strings-macro
             separator-comment-string
             init-short-comment short-comment
             comment-line-1-string
             comment-line-2-string
             max-clen)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-key-value-string key value comment)
  (begin
    (let ((vstring value))
      (begin
        (if (number? value)
            (begin
              (let ((tmp-vstring
                     (utils-module:commafy-number value)))
                (begin
                  (set! vstring tmp-vstring)
                  ))
              ))

        (let ((ll1 (format #f "~a=~a" key vstring)))
          (let ((nspaces5
                 (- 40 (string-length ll1))))
            (let ((spaces5 (make-string nspaces5 #\space)))
              (let ((ll2
                     (format #f "~a~a~a" ll1 spaces5 comment)))
                (begin
                  ll2
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (write-config-file filename output-htable comment-htable)
  (begin
    (let ((key-list
           (sort
            (hash-map->list
             (lambda (key value)
               (begin
                 key
                 )) output-htable)
            string-ci<?)))
      (begin
        (with-output-to-file filename
          (lambda ()
            (begin
              ;;; first print comment header
              (display-header-block)

              (for-each
               (lambda (key)
                 (begin
                   (let ((value
                          (hash-ref output-htable key))
                         (comment
                          (hash-ref comment-htable key #f)))
                     (let ((vstring value))
                       (begin
                         (if (number? value)
                             (begin
                               (set!
                                vstring
                                (ice-9-format:format #f "~:d" value))
                               ))

                         (if (equal? comment #f)
                             (begin
                               (set! comment
                                     (format #f ";;; ~a" key)))
                             (begin
                               (set! comment
                                     (string-trim-both comment))
                               ))

                         (let ((new-line
                                (make-key-value-string
                                 key value comment)))
                           (begin
                             (display
                              (format #f "~a~%" new-line))
                             (force-output)
                             ))
                         )))
                   )) key-list)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
