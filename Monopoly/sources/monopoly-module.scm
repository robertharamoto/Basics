;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  monopoly-module - monopoly simulation                ###
;;;###                                                       ###
;;;###  last updated December 11, 2024                       ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;###
;;;###  created July 20, 2024
;;;###

;;;#############################################################
;;;#############################################################
;;;### start monopoly modules
(define-module (monopoly-module)
  #:export (initialize-random-state
            initialize-list-data
            initialize-single-board-sum

            generate-board-list-macro
            main-loop

            update-position-data-macro
            top-card-to-bottom-macro
            find-next-position

            handle-community-chest-macro
            handle-chance-macro
            calc-std-deviation

            top-three-list-to-number-code
            display-random-two-dice-tests
            ))

;;;#############################################################
;;;#############################################################
;;;###  include modules

;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 copy-tree for copy-tree
(use-modules ((ice-9 copy-tree)
              :renamer (symbol-prefix-proc 'ice-9-copy-tree:)))

;;;### ice-9-format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;#############################################################
;;;#############################################################
;;;###  begin main functions

;;;#############################################################
;;;#############################################################
(define (initialize-random-state)
  (begin
    (let ((time (gettimeofday)))
      (begin
        (set!
         *random-state*
         (seed->random-state
          (+ (car time)
             (cdr time))))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (roll-two-dice max-num)
  (begin
    (let ((dnum1
           (inexact->exact
            (truncate (+ 1.0 (random max-num)))))
          (dnum2
           (inexact->exact
            (truncate (+ 1.0 (random max-num))))))
      (let ((sum (+ dnum1 dnum2)))
        (begin
          (if (= dnum1 dnum2)
              (begin
                (list sum #t))
              (begin
                (list sum #f)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (display-random-two-dice-tests max-rolls)
  (begin
    (let ((max-dice-num 6)
          (results-htable (make-hash-table 20)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-rolls))
          (begin
            (let ((roll-list
                   (roll-two-dice max-dice-num)))
              (let ((this-roll
                     (car roll-list)))
                (let ((this-count
                       (hash-ref
                        results-htable this-roll 0)))
                  (let ((new-htable
                         (hash-set!
                          results-htable this-roll (1+ this-count))))
                    (begin
                      (set! results-htable new-htable)
                      )))
                ))
            ))
        (hash-for-each
         (lambda(key value)
           (begin
             (display
              (ice-9-format:format
               #f "(~a) roll = ~:d~%" key value))
             )) results-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; data-list format
;;; board format (list place-number place-code count)
;;; checked - 12/23/2011, 12/28/2011, 3/11/2012
(define (initialize-list-data
         board-desc-htable board-sum-htable
         board-s2-htable)
  (begin
    (let ((code-list
           (list
            (list 0 "go") (list 1 "a1")
            (list 2 "cc1") (list 3 "a2")
            (list 4 "t1") (list 5  "r1")
            (list 6 "b1") (list 7 "ch1")
            (list 8 "b2") (list 9 "b3")
            (list 10 "jail") (list 11 "c1")
            (list 12 "u1") (list 13 "c2")
            (list 14 "c3") (list 15 "r2")
            (list 16 "d1") (list 17 "cc2")
            (list 18 "d2") (list 19 "d3")
            (list 20 "fp") (list 21 "e1")
            (list 22 "ch2") (list 23 "e2")
            (list 24 "e3") (list 25 "r3")
            (list 26 "f1") (list 27 "f2")
            (list 28 "u2") (list 29 "f3")
            (list 30 "g2j") (list 31 "g1")
            (list 32 "g2") (list 33 "cc3")
            (list 34 "g3") (list 35 "r4")
            (list 36 "ch3") (list 37 "h1")
            (list 38 "t2") (list 39 "h2")
            )))
      (begin
        (hash-clear! board-desc-htable)
        (hash-clear! board-sum-htable)
        (hash-clear! board-s2-htable)

        (for-each
         (lambda (alist)
           (begin
             (let ((asquare (list-ref alist 0))
                   (adesc (list-ref alist 1)))
               (begin
                 (hash-set!
                  board-desc-htable asquare adesc)
                 (hash-set!
                  board-sum-htable asquare 0)
                 (hash-set!
                  board-s2-htable asquare 0)
                 ))
             )) code-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify simulation loop
;;; board format (htable place-number count)
(define-syntax randomize-card-deck-macro
  (syntax-rules ()
    ((randomize-card-deck-macro
      card-deck-list
      final-list)
     (begin
       (let ((nlist-items (length card-deck-list))
             (remaining-list
              (ice-9-copy-tree:copy-tree card-deck-list)))
         (let ((remaining-items nlist-items))
           (begin
             (do ((ii 0 (1+ ii)))
                 ((>= ii nlist-items))
               (begin
                 (if (> remaining-items 1)
                     (begin
                       (let ((this-index
                              (inexact->exact
                               (truncate (random remaining-items)))))
                         (let ((this-item
                                (list-ref remaining-list this-index)))
                           (begin
                             (set!
                              final-list (cons this-item final-list))
                             (set!
                              remaining-list
                              (delete1! this-item remaining-list))
                             (set!
                              remaining-items (1- remaining-items))
                             ))))
                     (begin
                       (let ((this-item (car remaining-list)))
                         (begin
                           (set!
                            final-list (cons this-item final-list))
                           (set!
                            remaining-list
                            (delete1!
                             this-item remaining-list))
                           (set!
                            remaining-items (1- remaining-items))
                           ))
                       ))
                 ))
             (set! final-list (reverse final-list))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; cc-deck format (list op-code go-to-destination code-name)
(define (initialize-community-chest-deck)
  (begin
    (let ((cc-list
           (list
            (list 1 0 "go")
            (list 2 10 "jail")
            (list 3 -1 "nop")
            (list 4 -1 "nop")
            (list 5 -1 "nop")
            (list 6 -1 "nop")
            (list 7 -1 "nop")
            (list 8 -1 "nop")
            (list 9 -1 "nop")
            (list 10 -1 "nop")
            (list 11 -1 "nop")
            (list 12 -1 "nop")
            (list 13 -1 "nop")
            (list 14 -1 "nop")
            (list 15 -1 "nop")
            (list 16 -1 "nop")))
          (final-list (list)))
      (begin
        (randomize-card-deck-macro
         cc-list
         final-list)

        (reverse final-list)
        ))
    ))


;;;#############################################################
;;;#############################################################
;;; cc-deck format (list op-code go-to-destination code-name)
(define (initialize-chance-deck)
  (begin
    (let ((move-list
           (list
            (list 1 0 "go")
            (list 2 10 "jail")
            (list 3 11 "c1")
            (list 4 24 "e3")
            (list 5 39 "h2")
            (list 6 5 "r1")
            (list 7 -1 "r")
            (list 8 -1 "r")
            (list 9 -1 "u")
            (list 10 -3 "back 3")
            (list 11 -1 "nop")
            (list 12 -1 "nop")
            (list 13 -1 "nop")
            (list 14 -1 "nop")
            (list 15 -1 "nop")
            (list 16 -1 "nop")
            ))
          (final-list (list)))
      (begin
        (randomize-card-deck-macro
         move-list
         final-list)

        (reverse final-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify simulation loop
;;; board format (htable place-number count)
(define-syntax update-position-data-macro
  (syntax-rules ()
    ((update-position-data-macro
      single-board-sum-htable position)
     (begin
       (let ((this-count
              (hash-ref
               single-board-sum-htable position 0)))
         (let ((next-count (1+ this-count)))
           (begin
             (hash-set!
              single-board-sum-htable
              position next-count)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify simulation loop
(define-syntax top-card-to-bottom-macro
  (syntax-rules ()
    ((top-card-to-bottom-macro card-list)
     (begin
       (let ((top-card (car card-list))
             (tail-list (cdr card-list)))
         (let ((next-list
                (append tail-list (list top-card))))
           (begin
             (set! card-list next-list)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
;;; speed up the simulation by hard-wiring the goto-next r or u cards
;;; (r-pos-list (list 5 15 25 35))
;;; (u-pos-list (list 12 28)))
(define (find-next-position
         current-position code-string)
  (begin
    (let ((next-position -1))
      (begin
        (cond
         ((string-ci=? code-string "r")
          (begin
            (cond
             ((< current-position 5)
              (begin
                (set! next-position 5)
                ))
             ((< current-position 15)
              (begin
                (set! next-position 15)
                ))
             ((< current-position 25)
              (begin
                (set! next-position 25)
                ))
             ((< current-position 35)
              (begin
                (set! next-position 35)
                ))
             (else
              (begin
                (set! next-position 5)
                )))
            ))
         ((string-ci=? code-string "u")
          (begin
            (cond
             ((< current-position 12)
              (begin
                (set! next-position 12)
                ))
             ((< current-position 28)
              (begin
                (set! next-position 28)
                ))
             (else
              (begin
                (set! next-position 12)
                )))
            )))
        next-position
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; chance-deck format (list op-code go-to-destination code-name)
(define-syntax go-forward-macro
  (syntax-rules ()
    ((go-forward-macro
      board-list board-length
      single-board-sum-htable
      board-sum-htable board-s2-htable
      position-counter next-position
      orbits-number)
     (begin
       (update-position-data-macro
        single-board-sum-htable position-counter)

       (set! orbits-number (1+ orbits-number))

       (compile-single-orbit-statistics
        board-list
        single-board-sum-htable
        board-sum-htable board-s2-htable)

       (initialize-single-board-sum
        board-list single-board-sum-htable)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; community-chest-deck format (list op-code go-to-destination code-name)
(define-syntax handle-community-chest-macro
  (syntax-rules ()
    ((handle-community-chest-macro
      board-list
      single-board-sum-htable
      board-sum-htable board-s2-htable
      position-counter next-position
      orbits-number
      consecutive-doubles-count
      community-deck)
     (begin
       (let ((top-card (car community-deck)))
         (let ((dest (list-ref top-card 1))
               (code-name (list-ref top-card 2)))
           (begin
             (cond
              ((string-ci=? code-name "go")
               (begin
                 (set! position-counter dest)

                 (go-forward-macro
                  board-list board-length
                  single-board-sum-htable
                  board-sum-htable board-s2-htable
                  position-counter next-position
                  orbits-number)
                 ))
              ((string-ci=? code-name "jail")
               (begin
                 (set! consecutive-doubles-count 0)
                 (set! position-counter dest)
                 (update-position-data-macro
                  single-board-sum-htable
                  position-counter)
                 ))
              (else
               (begin
                 (set! position-counter next-position)
                 (update-position-data-macro
                  single-board-sum-htable
                  position-counter)
                 )))
             )))
       (top-card-to-bottom-macro community-deck)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; chance-deck format (list op-code go-to-destination code-name)
(define-syntax handle-chance-macro
  (syntax-rules ()
    ((handle-chance-macro
      board-list board-length
      single-board-sum-htable
      board-sum-htable board-s2-htable
      position-counter next-position
      orbits-number
      consecutive-doubles-count
      chance-deck cc-deck)
     (begin
       (let ((top-card (car chance-deck)))
         (let ((dest (list-ref top-card 1))
               (code-name (list-ref top-card 2)))
           (begin
             (cond
              ((string-ci=? code-name "go")
               (begin
                 (set! position-counter dest)

                 (go-forward-macro
                  board-list board-length
                  single-board-sum-htable
                  board-sum-htable board-s2-htable
                  position-counter next-position
                  orbits-number)
                 ))
              ((string-ci=? code-name "jail")
               (begin
                 (set! consecutive-doubles-count 0)
                 (set! position-counter dest)
                 (update-position-data-macro
                  single-board-sum-htable position-counter)
                 ))
              ((or (string-ci=? code-name "c1")
                   (string-ci=? code-name "e3")
                   (string-ci=? code-name "h2")
                   (string-ci=? code-name "r1"))
               (begin
                 (if (> position-counter dest)
                     (begin
                       (set! position-counter dest)
                       (go-forward-macro
                        board-list board-length
                        single-board-sum-htable
                        board-sum-htable board-s2-htable
                        position-counter next-position
                        orbits-number))
                     (begin
                       (set! position-counter dest)
                       ))
                 (update-position-data-macro
                  single-board-sum-htable position-counter)
                 ))
              ((or (string-ci=? code-name "r")
                   (string-ci=? code-name "u"))
               (begin
                 (let ((next-position-counter
                        (find-next-position
                         position-counter code-name)))
                   (begin
                     (if (> position-counter dest)
                         (begin
                           (set! position-counter dest)
                           (go-forward-macro
                            board-list board-length
                            single-board-sum-htable
                            board-sum-htable board-s2-htable
                            position-counter next-position
                            orbits-number))
                         (begin
                           (set! position-counter dest)
                           ))

                     (set! position-counter next-position-counter)
                     (update-position-data-macro
                      single-board-sum-htable position-counter)
                     ))
                 ))
              ((string-ci=? code-name "back 3")
               (begin
                 (let ((next-position-counter (- next-position 3)))
                   (begin
                     (if (< next-position-counter 0)
                         (begin
                           (set!
                            next-position-counter
                            (+ next-position-counter board-length))
                           ))

                     (cond
                      ((= next-position-counter 30)
                       (begin
                         ;;; go to jail
                         (set! consecutive-doubles-count 0)
                         (set! next-position-counter 10)
                         ))
                      ((or (= next-position-counter 2)
                           (= next-position-counter 17)
                           (= next-position-counter 33))
                       (begin
                         ;;; chance deck says go back 3 spaces,
                         ;;; and we land on community chest
                         (handle-community-chest-macro
                          board-list
                          single-board-sum-htable
                          board-sum-htable board-s2-htable
                          position-counter next-position-counter
                          orbits-number
                          consecutive-doubles-count
                          cc-deck)
                         ))
                      (else
                       (begin
                         (set!
                          position-counter
                          next-position-counter)
                         (update-position-data-macro
                          single-board-sum-htable
                          position-counter)
                         )))
                     ))
                 ))
              (else
               (begin
                 (set! position-counter next-position)
                 (update-position-data-macro
                  single-board-sum-htable
                  position-counter)
                 )))

             (top-card-to-bottom-macro chance-deck)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; board-list format
;;; (list place-number place-code count total pcnt-double)
(define (top-three-list-to-number-code three-list)
  (begin
    (let ((result-string ""))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii 3))
          (begin
            (let ((this-elem (list-ref three-list ii)))
              (let ((this-num (list-ref this-elem 0)))
                (let ((num-string
                       (ice-9-format:format #f "~2,'0d" this-num)))
                  (begin
                    (set!
                     result-string
                     (string-append result-string num-string))
                    ))
                ))
            ))
        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (initialize-single-board-sum
         board-list single-board-sum-htable)
  (begin
    (for-each
     (lambda (key)
       (begin
         (hash-set!
          single-board-sum-htable key 0)
         )) board-list)
    ))

;;;#############################################################
;;;#############################################################
(define (compile-single-orbit-statistics
         board-list
         single-board-sum-htable
         board-sum-htable
         board-s2-htable)
  (begin
    (for-each
     (lambda (this-position)
       (begin
         (let ((current-count
                (hash-ref
                 single-board-sum-htable this-position 0))
               (subtotal-count
                (hash-ref
                 board-sum-htable this-position 0))
               (s2-count
                (hash-ref
                 board-s2-htable this-position 0)))
           (begin
             (hash-set!
              board-sum-htable this-position
              (+ subtotal-count
                 current-count))
             (hash-set!
              board-s2-htable this-position
              (+ s2-count
                 (* current-count current-count)))
             ))
         )) board-list)
    ))

;;;#############################################################
;;;#############################################################
(define-syntax status-loop-macro
  (syntax-rules ()
    ((status-loop-macro
      orbit end-num
      start-jday status-minutes)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (let ((elapsed-mins
                (timer-module:julian-day-difference-in-minutes
                 end-jday start-jday)))
           (begin
             (if (>= elapsed-mins status-minutes)
                 (begin
                   (display
                    (ice-9-format:format
                     #f "status ~:d / ~:d : "
                     orbit end-num))
                   (display
                    (format
                     #f "elapsed time = ~a : ~a~%"
                     (timer-module:julian-day-difference-to-string
                      end-jday start-jday)
                     (timer-module:current-date-time-string)))
                   (force-output)
                   (set! start-jday end-jday)
                   (gc)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-single-dice-roll-macro
  (syntax-rules ()
    ((process-single-dice-roll-macro
      is-double
      board-list
      single-board-sum-htable
      board-desc-htable
      board-sum-htable board-s2-htable
      board-length
      position-counter next-position
      consecutive-doubles-count
      orbit-counter max-orbits
      continue-loop-flag
      chance-deck community-deck)
     (begin
       (if (equal? is-double #t)
           (begin
             (set!
              consecutive-doubles-count
              (1+ consecutive-doubles-count)))
           (begin
             (set! consecutive-doubles-count 0)
             ))

       (if (>= consecutive-doubles-count 3)
           (begin
             ;;; go directly to jail
             (set! consecutive-doubles-count 0)
             (set! position-counter 10)
             (set! next-position 10)
             (update-position-data-macro
              single-board-sum-htable
              position-counter))
           (begin
             ;;; consecutive-doubles-count < 3
             (if (>= next-position board-length)
                 (begin
                   (go-forward-macro
                    board-list board-length
                    single-board-sum-htable
                    board-sum-htable board-s2-htable
                    position-counter next-position
                    orbit-counter)

                   (set! next-position
                         (modulo next-position board-length))

                   (set! position-counter next-position)

                   (initialize-single-board-sum
                    board-list single-board-sum-htable)

                   (if (>= orbit-counter max-orbits)
                       (begin
                         (set! continue-loop-flag #f)
                         ))
                   ))

             (let ((position-code-name
                    (hash-ref
                     board-desc-htable next-position "")))
               (begin
                 ;;; handle exceptions (community chest, chance, and goto jail)
                 (cond
                  ((string-ci=?
                    position-code-name "g2j")
                   (begin
                     (set! consecutive-doubles-count 0)
                     (set! position-counter 10)
                     (update-position-data-macro
                      single-board-sum-htable
                      position-counter)
                     ))
                  ((not
                    (equal?
                     (string-contains-ci
                      position-code-name "cc")
                     #f))
                   (begin
                     ;;; community chest
                     (handle-community-chest-macro
                      board-list
                      single-board-sum-htable
                      board-sum-htable board-s2-htable
                      position-counter next-position
                      orbit-counter
                      consecutive-doubles-count
                      community-deck)
                     ))
                  ((not
                    (equal?
                     (string-contains-ci
                      position-code-name "ch")
                     #f))
                   (begin
                     ;;; chance
                     (handle-chance-macro
                      board-list board-length
                      single-board-sum-htable
                      board-sum-htable board-s2-htable
                      position-counter next-position
                      orbit-counter
                      consecutive-doubles-count
                      chance-deck community-deck)
                     ))
                  (else
                   (begin
                     (set!
                      position-counter next-position)
                     ;;; count orbits and update-stats
                     (update-position-data-macro
                      single-board-sum-htable
                      position-counter)
                     )))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(define (simulation-loop
         board-list board-desc-htable
         board-sum-htable board-s2-htable
         max-dice-rolls max-dice-number
         start-jday
         status-minutes)
  (begin
    (let ((board-length
           (length board-list))
          (chance-deck
           (initialize-chance-deck))
          (community-deck
           (initialize-community-chest-deck))
          (single-board-sum-htable
           (make-hash-table 100))
          (position-counter 0)
          (orbit-counter 0)
          (status-check-orbit 100)
          (continue-loop-flag #t)
          (consecutive-doubles-count 0))
      (begin
        (initialize-single-board-sum
         board-list board-sum-htable)

        (initialize-single-board-sum
         board-list board-s2-htable)

        (initialize-single-board-sum
         board-list single-board-sum-htable)

        (do ((ii 0 (1+ ii)))
            ((>= ii max-dice-rolls))
          (begin
            (let ((roll-list (roll-two-dice max-dice-number)))
              (let ((roll-sum (list-ref roll-list 0))
                    (is-double (list-ref roll-list 1)))
                (let ((next-position
                       (+ position-counter roll-sum)))
                  (begin
                    (process-single-dice-roll-macro
                     is-double
                     board-list
                     single-board-sum-htable
                     board-desc-htable
                     board-sum-htable board-s2-htable
                     board-length
                     position-counter next-position
                     consecutive-doubles-count
                     orbit-counter max-dice-rolls
                     continue-loop-flag
                     chance-deck community-deck)
                    ))
                ))

            (if (zero?
                 (modulo ii status-check-orbit))
                (begin
                  ;;; every 100 dice rolls check the status
                  (status-loop-macro
                   ii max-dice-rolls
                   start-jday status-minutes)
                  ))
            ))

        orbit-counter
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-std-deviation
         board-list board-sum-htable board-s2-htable
         std-deviation-htable number-of-orbits)
  (begin
    (for-each
     (lambda (position)
       (begin
         (let ((favg
                (exact->inexact
                 (/ (hash-ref
                     board-sum-htable position 0)
                    number-of-orbits)))
               (f2avg
                (exact->inexact
                 (/ (hash-ref
                     board-s2-htable position 0)
                    number-of-orbits))))
           (let ((avg-sqr (* favg favg)))
             (begin
               (if (>= f2avg avg-sqr)
                   (begin
                     (let ((std-dev
                            (sqrt (- f2avg avg-sqr))))
                       (begin
                         (hash-set!
                          std-deviation-htable
                          position std-dev)
                         )))
                   (begin
                     (display
                      (format
                       #f "calc-std-deviation : error : "))
                     (display
                      (format
                       #f "position=~a, sum squares=~a, avg^2=~a~%"
                       position f2avg avg-sqr))
                     (force-output)
                     (hash-set!
                      std-deviation-htable
                      position -1.0)
                     ))
               )))
           )) board-list)
    ))

;;;#############################################################
;;;#############################################################
(define (display-single-board-element
         position board-desc-htable
         board-sum-htable board-s2-htable
         std-deviation-htable
         num-orbits
         max-dice-rolls)
  (begin
    (let ((place-name
           (hash-ref
            board-desc-htable position "missing"))
          (count
           (hash-ref
            board-sum-htable position 0))
          (std-dev
           (hash-ref
            std-deviation-htable position 0)))
      (let ((pcnt (* 100.0
                     (exact->inexact
                      (/ count max-dice-rolls)))))
        (begin
          (display
           (ice-9-format:format
            #f "    ~a (~1,2f%) = square ~2,'0d~%"
            place-name pcnt position))
          (display
           (ice-9-format:format
            #f "    (~:d out of ~:d), orbits = ~:d, "
            count max-dice-rolls num-orbits))
          (display
           (ice-9-format:format
            #f "std dev = ~4,2e)~%"
            std-dev))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; generate board list
(define-syntax generate-board-list-macro
  (syntax-rules ()
    ((generate-board-list-macro
      board-desc-htable
      results-board-list)
     (begin
       (hash-for-each
        (lambda (key value)
          (begin
            (set! results-board-list
                  (cons key results-board-list))
            )) board-desc-htable)

       (set! results-board-list
             (sort results-board-list <))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (summarize-results
         board-desc-htable
         board-sum-htable board-s2-htable
         std-deviation-htable
         num-orbits
         max-dice-rolls)
  (begin
    (let ((sum-list
           (hash-map->list
            (lambda (key value)
              (begin
                (list key value)
                )) board-sum-htable)))
      (let ((sorted-list
             (sort
              sum-list
              (lambda (a b)
                (begin
                  (> (list-ref a 1) (list-ref b 1))
                  ))
              ))
            (board-entries-number
             (length sum-list)))
        (let ((top-three-list
               (list-head sorted-list 3))
              (last-four-list
               (list-tail
                sorted-list (- board-entries-number 4))))
          (let ((top-three-code
                 (top-three-list-to-number-code
                  top-three-list)))
            (begin
              (display
               (format
                #f "three most popular squares~%"))
              (for-each
               (lambda (a-list)
                 (begin
                   (let ((position
                          (list-ref a-list 0)))
                     (begin
                       (display-single-board-element
                        position board-desc-htable
                        board-sum-htable board-s2-htable
                        std-deviation-htable
                        num-orbits
                        max-dice-rolls)
                       ))
                   )) top-three-list)

              (display
               (format
                #f "three most popular squares modal "))
              (display
               (format
                #f "string = ~a~%"
                top-three-code))
              (force-output)

              (display
               (format
                #f "least popular squares~%"))
              (for-each
               (lambda (a-list)
                 (begin
                   (let ((position
                          (list-ref a-list 0)))
                     (begin
                       (display-single-board-element
                        position board-desc-htable
                        board-sum-htable board-s2-htable
                        std-deviation-htable
                        num-orbits
                        max-dice-rolls)
                       ))
                   )) last-four-list)
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop
         max-dice-number max-dice-rolls status-minutes)
  (begin
    (initialize-random-state)

    (let ((board-desc-htable (make-hash-table 100))
          (board-sum-htable (make-hash-table 100))
          (board-s2-htable (make-hash-table 100))
          (std-deviation-htable (make-hash-table 100))
          (board-list (list))
          (start-jday (srfi-19:current-julian-day)))
      (begin
        (initialize-list-data
         board-desc-htable
         board-sum-htable
         board-s2-htable)

        (generate-board-list-macro
         board-desc-htable board-list)

        (let ((num-orbits
               (simulation-loop
                board-list board-desc-htable
                board-sum-htable board-s2-htable
                max-dice-rolls max-dice-number
                start-jday
                status-minutes)))
          (begin

            (calc-std-deviation
             board-list board-sum-htable board-s2-htable
             std-deviation-htable num-orbits)

            (summarize-results
             board-desc-htable
             board-sum-htable board-s2-htable
             std-deviation-htable
             num-orbits
             max-dice-rolls)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
