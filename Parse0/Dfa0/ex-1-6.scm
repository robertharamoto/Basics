#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  accept at most 2 consecutive b's                     ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;###  last updated May 18, 2022                            ###
;;;###                                                       ###
;;;###  updated February 18, 2020                            ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ice-9 rdelim for read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ls - find all test subroutines to run
(use-modules ((ice-9 ls)
              :renamer (symbol-prefix-proc 'ice-9-ls:)))

;;;### srfi-9 - records
(use-modules ((srfi srfi-9)
              :renamer (symbol-prefix-proc 'srfi-srfi9:)))

;;;#############################################################
;;;#############################################################
;;;### expects 2 julian days (plain numbers)
;;;### differences between 2 julian days is in days (or a fraction of a day)
(define (julian-day-difference-to-string dend dstart)
  (define (local-process-sub-day day-fraction)
    (begin
      (let ((nsecs (* day-fraction 24.0 60.0 60.0))
            (nmins (truncate (* day-fraction 24.0 60.0)))
            (nhours
             (inexact->exact
              (truncate (* day-fraction 24.0)))))
        (let ((nminutes
               (inexact->exact
                (truncate (- nmins (* nhours 60.0))))))
          (let ((nseconds
                 (* 0.0010
                    (truncate
                     (* 1000.0
                        (- nsecs
                           (+ (* nhours 60.0 60.0)
                              (* nminutes 60.0))))))))
            (let ((seconds-string
                   (string-trim
                    (ice-9-format:format #f "~5,3f seconds" nseconds)))
                  (minutes-string
                   (format #f "~a minutes" nminutes))
                  (hours-string
                   (format #f "~a hours" nhours)))
              (begin
                (if (= nseconds 1.0)
                    (begin
                      (set! seconds-string "1 second")
                      ))
                (if (= nminutes 1)
                    (begin
                      (set! minutes-string "1 minute")
                      ))
                (if (= nhours 1.0)
                    (begin
                      (set! hours-string "1 hour")
                      ))

                (if (<= nhours 0)
                    (begin
                      (if (<= nminutes 0.0)
                          (begin
                            seconds-string)
                          (begin
                            (format
                             #f "~a, ~a"
                             minutes-string seconds-string)
                            )))
                    (begin
                      (if (<= nminutes 0)
                          (begin
                            (string-trim
                             (format
                              #f "~a, ~a"
                              hours-string seconds-string)))
                          (begin
                            (string-trim
                             (format
                              #f "~a, ~a, ~a"
                              hours-string minutes-string
                              seconds-string))
                            ))
                      ))
                )))
          ))
      ))
  (begin
    (if (and (number? dend) (number? dstart))
        (begin
          (let ((jd-diff (exact->inexact (- dend dstart))))
            (begin
              (if (< jd-diff 1.0)
                  (begin
                    (let ((tstring
                           (local-process-sub-day jd-diff)))
                      (begin
                        tstring
                        )))
                  (begin
                    (let ((ndays
                           (inexact->exact (truncate jd-diff))))
                      (let ((dfract-diff (- jd-diff ndays)))
                        (let ((tstring (local-process-sub-day dfract-diff)))
                          (let ((ttstring
                                 (string-trim
                                  (format #f "~a days, ~a" ndays tstring))))
                            (begin
                              (if (= ndays 1)
                                  (begin
                                    (set! ttstring
                                          (format #f "1 day, ~a" tstring))
                                    ))

                              ttstring
                              )))
                        ))
                    ))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (current-date-time-string)
  (begin
    (let ((this-datetime (srfi-19:current-date)))
      (let ((s1
             (srfi-19:date->string
              this-datetime
              "~A, ~B ~d, ~Y"))
            (s2
             (string-downcase
              (srfi-19:date->string
               this-datetime "~I:~M:~S ~p"))))
        (begin
          (format #f "~a,  ~a" s1 s2)
          )))
    ))

;;;#############################################################
;;;#############################################################
(srfi-srfi9:define-record-type
 <transistion-element>
 (make-transistion-element state-name next-value next-state)
 transistion-element?
 (state-name get-state-name set-state-name!)
 (next-value get-next-value set-next-value!)
 (next-state get-next-state set-next-state!))

;;;#############################################################
;;;#############################################################
(define (make-a-b-c-transistion-list)
  (begin
    (let ((tlist
           (list
            (make-transistion-element "q0" "a" "q0")
            (make-transistion-element "q0" "b" "q1")
            (make-transistion-element "q1" "a" "q0")
            (make-transistion-element "q1" "b" "q2")
            (make-transistion-element "q2" "a" "q0")
            (make-transistion-element "q2" "b" "q3")
            (make-transistion-element "q3" "a" "q3")
            (make-transistion-element "q3" "b" "q3")
            )))
      (begin
        tlist
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-accept-list)
  (begin
    (list "q0" "q1" "q2")
    ))

;;;#############################################################
;;;#############################################################
(define (is-in-accept-list? current-state accept-state-list)
  (begin
    (let ((accept-flag #f))
      (begin
        (for-each
         (lambda (a-state)
           (begin
             (if (equal? accept-flag #f)
                 (begin
                   (if (equal? a-state current-state)
                       (begin
                         (set! accept-flag #t)
                         ))
                   ))
             )) accept-state-list)

        accept-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (test-is-in-accept-list-1)
  (begin
    (let ((sub-name "test-is-in-accept-list-1")
          (test-list
           (list
            (list "q0" (list "q0" "q1") #t)
            (list "q1" (list "q0" "q1") #t)
            (list "q2" (list "q0" "q1") #f)
            ))
          (test-label-index 0)
          (ok-flag #t))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((a-state (list-ref alist 0))
                   (accept-list (list-ref alist 1))
                   (shouldbe-bool (list-ref alist 2)))
               (let ((result-bool
                      (is-in-accept-list? a-state accept-list)))
                 (begin
                   (if (not (equal? shouldbe-bool result-bool))
                       (begin
                         (display
                          (format
                           #f "~a : (~a) : error : input state = ~a, "
                           sub-name test-label-index a-state))
                         (display
                          (format
                           #f "accept-list = ~a, shouldbe = ~a, result = ~a~%"
                           accept-list
                           (if shouldbe-bool "true" "false")
                           (if result-bool "true" "false")))
                         (force-output)
                         (set! ok-flag #f)
                         ))
                   )))

             (set! test-label-index (1+ test-label-index))
             )) test-list)

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-next-transistion-name
         current-state next-value transistion-list)
  (begin
    (let ((next-state #f))
      (begin
        (for-each
         (lambda (telement)
           (begin
             (if (equal? next-state #f)
                 (begin
                   (let ((state-name (get-state-name telement)))
                     (begin
                       (if (equal? state-name current-state)
                           (begin
                             (let ((state-next-value
                                    (get-next-value telement)))
                               (begin
                                 (if (equal? state-next-value next-value)
                                     (begin
                                       (set!
                                        next-state
                                        (get-next-state telement))
                                       ))
                                 ))
                             ))
                       ))
                   ))
             )) transistion-list)

        next-state
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (test-get-next-transistion-name-1)
  (begin
    (let ((sub-name "test-get-next-transistion-name-1")
          (test-list
           (list
            (list "q0" "a" "q0")
            (list "q0" "b" "q1")
            (list "q0" "c" #f)
            (list "q0" "0" #f)
            (list "q1" "a" "q0")
            (list "q1" "b" "q2")
            (list "q1" "c" #f)
            (list "q2" "a" "q0")
            (list "q2" "b" "q3")
            (list "q3" "a" "q3")
            (list "q3" "b" "q3")
            (list "q3" "c" #f)
            ))
          (transistion-list-a-b-c (make-a-b-c-transistion-list))
          (test-label-index 0)
          (ok-flag #t))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((test-string (list-ref alist 0))
                   (next-value (list-ref alist 1))
                   (shouldbe-name (list-ref alist 2)))
               (let ((result-name
                      (get-next-transistion-name
                       test-string next-value
                       transistion-list-a-b-c)))
                 (begin
                   (if (not (equal? shouldbe-name result-name))
                       (begin
                         (display
                          (format
                           #f "~a : (~a) : error : input string = ~a, "
                           sub-name test-label-index test-string))
                         (display
                          (format
                           #f "next-value = ~a, shouldbe = ~a, result = ~a~%"
                           next-value shouldbe-name result-name))
                         (set! ok-flag #f)
                         ))
                   )))

             (set! test-label-index (1+ test-label-index))
             )) test-list)

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (accept-string?
         init-string init-state accept-state-list
         transistion-list display-flag)
  (begin
    (let ((init-list
           (map
            (lambda (achar)
              (begin
                (string (char-downcase achar))
                )) (string->list init-string)))
          (ok-flag #t)
          (current-state init-state))
      (begin
        (if (equal? display-flag #t)
            (begin
              (display
               (format
                #f "init-string=~s~%" init-string))
              (force-output)
              ))

        (for-each
         (lambda (val-elem)
           (begin
             (if (equal? ok-flag #t)
                 (begin
                   (let ((next-state
                          (get-next-transistion-name
                           current-state val-elem transistion-list)))
                     (begin
                       (if (not (equal? next-state #f))
                           (begin
                             (if (equal? display-flag #t)
                                 (begin
                                   (display
                                    (format
                                     #f "    character=~a, current-state=~a"
                                     val-elem current-state))
                                   (display
                                    (format
                                     #f " -> next-state=~a~%"
                                     next-state))
                                   (force-output)
                                   ))
                             (set! current-state next-state))
                           (begin
                             (if (equal? display-flag #t)
                                 (begin
                                   (display
                                    (format
                                     #f "accept-string? error: "))
                                   (display
                                    (format
                                     #f "character=~a, current-state=~a~%"
                                     val-elem current-state))
                                   (force-output)
                                   ))
                             (set! ok-flag #f)
                             ))
                       ))
                   ))
             )) init-list)

        (if (equal? ok-flag #t)
            (begin
              (let ((accept-flag
                     (is-in-accept-list?
                      current-state accept-state-list)))
                (begin
                  (if (equal? display-flag #t)
                      (begin
                        (display
                         (format
                          #f "    accept=~a~%"
                          (if accept-flag "true" "false")))
                        (force-output)
                        ))

                  accept-flag
                  )))
            (begin
              (if (equal? display-flag #t)
                  (begin
                    (display
                     (format
                      #f "    accept=false~%"))
                    (force-output)
                    ))
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (test-accept-string-1)
  (begin
    (let ((sub-name "test-accept-string-1")
          (test-list
           (list
            (list "ab" #t)
            (list "abb" #t)
            (list "abba" #t)
            (list "abbba" #f)
            (list "abbbb" #f)
            (list "abbabba" #t)
            (list "abbabbab" #t)
            (list "bababbabb" #t)
            (list "babbc" #f)
            ))
          (init-state "q0")
          (accept-state-list (make-accept-list))
          (transistion-list-a-b-c (make-a-b-c-transistion-list))
          (display-flag #f)
          (test-label-index 0)
          (ok-flag #t))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((test-string (list-ref alist 0))
                   (shouldbe-bool (list-ref alist 1)))
               (let ((result-bool
                      (accept-string?
                       test-string init-state accept-state-list
                       transistion-list-a-b-c display-flag)))
                 (begin
                   (if (not (equal? shouldbe-bool result-bool))
                       (begin
                         (display
                          (format
                           #f "~a : (~a) : error : input string = ~a, "
                           sub-name test-label-index test-string))
                         (display
                          (format
                           #f "shouldbe = ~a, result = ~a~%"
                           (if shouldbe-bool "true" "false")
                           (if result-bool "true" "false")))
                         (force-output)
                         (set! ok-flag #f)
                         ))
                   )))

             (set! test-label-index (1+ test-label-index))
             )) test-list)

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((count 0)
          (continue-loop-flag #t)
          (init-state "q0")
          (accept-state-list (make-accept-list))
          (transistion-list-a-b-c (make-a-b-c-transistion-list))
          (display-flag #t))
      (begin
        (while
            (equal? continue-loop-flag #t)
          (begin
            (display
             (format #f "Accept at most 2 consecutive b's~%"))
            (display
             (format
              #f "(~a) Enter string (q to quit) : "
              count))
            (force-output)
            (let ((in-line
                   (ice-9-rdelim:read-line )))
              (let ((quit-line
                     (string-filter char-alphabetic? in-line)))
                (begin
                  (if (or (string-ci=? quit-line "q")
                          (string-ci=? quit-line "quit"))
                      (begin
                        (set! continue-loop-flag #f))
                      (begin
                        (set! count (1+ count))
                        (let ((result-bool
                               (accept-string?
                                in-line init-state accept-state-list
                                transistion-list-a-b-c display-flag)))
                          (begin
                            (newline)
                            ))
                        ))
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  find-all-tests - returns a list of "test-*"
;;;###  methods
(define-public (find-all-tests)
  (define (local-make-test-list input-list result-list)
    (begin
      (if (or
           (null? input-list)
           (not (list? input-list))
           (< (length input-list) 1))
          (begin
            result-list)
          (begin
            (let ((this-func (car input-list))
                  (tail-list (cdr input-list)))
              (begin
                (if (symbol? this-func)
                    (begin
                      (let ((tstring (symbol->string this-func)))
                        (begin
                          (if (string-prefix-ci? "test-" tstring)
                              (begin
                                (local-make-test-list
                                 tail-list
                                 (append (list this-func) result-list)))
                              (begin
                                (local-make-test-list
                                 tail-list result-list)
                                ))
                          )))
                    (begin
                      (local-make-test-list
                       tail-list result-list)
                      ))
                ))
            ))
      ))
  (begin
    (let ((lfunc-list (ice-9-ls:lls)))
      (begin
        (let ((test-func-list
               (local-make-test-list lfunc-list (list))))
          (begin
            test-func-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-all-test-names st-list test-htable)
  (begin
    (let ((count (length st-list)))
      (begin
        (for-each
         (lambda (atest)
           (begin
             (let ((sname (symbol->string atest)))
               (let ((pr-name (hash-ref test-htable sname sname)))
                 (begin
                   (display (format #f "  ~a~%" pr-name))
                   )))
             )) st-list)

        (display
         (ice-9-format:format
          #f "total number of tests = ~:d~%" count))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (run-all-tests debug-flag)
  (begin
    (let ((nsuccess 0)
          (nfailures 0)
          (ntotals 0)
          (separator-line (make-string 64 #\=))
          (test-htable (make-hash-table 50))
          (t-list (find-all-tests)))
      (let ((st-list
             (sort
              t-list
              (lambda (a b)
                (begin
                  (string-ci<? (symbol->string a)
                               (symbol->string b))
                  ))))
            (count (length t-list)))
        (begin
          (hash-clear! test-htable)

          (for-each
           (lambda (atest)
             (begin
               (let ((a-func (primitive-eval atest)))
                 (let ((bresult (a-func))
                       (s-name (symbol->string atest)))
                   (begin
                     (if (equal? bresult #t)
                         (begin
                           (set! nsuccess (1+ nsuccess))
                           (hash-set!
                            test-htable
                            s-name
                            (format #f "ok! ~a" s-name)))
                         (begin
                           (set! nfailures (1+ nfailures))
                           (hash-set!
                            test-htable
                            s-name
                            (format #f "error! *** ~a ***" s-name))
                           ))
                     (set! ntotals (1+ ntotals))
                     )))
               )) st-list)

          (if (equal? debug-flag #t)
              (begin
                (display-all-test-names st-list test-htable)
                ))

          (display
           (format #f "~a~%" separator-line))
          (display (format #f "Test Summary~%"))
          (display (ice-9-format:format
                    #f "successful tests = ~:d (~,1f%)~%"
                    nsuccess (* 100.0 (/ nsuccess ntotals))))
          (display (ice-9-format:format
                    #f "failed tests = ~:d (~,1f%)~%"
                    nfailures (* 100.0 (/ nfailures ntotals))))
          (display (ice-9-format:format
                    #f "total tests = ~:d (~,1f%)~%"
                    (+ nsuccess nfailures)
                    (* 100.0 (/ (+ nsuccess nfailures) ntotals))))
          (display
           (format #f "~a~%" separator-line))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax time-code-macro
  (syntax-rules ()
    ((time-code-macro body)
     (begin
       (let ((start-jday (srfi-19:current-julian-day)))
         (begin
           body

           (let ((end-jday (srfi-19:current-julian-day)))
             (begin
               (display
                (format #f "elapsed time = ~a : ~a~%"
                        (julian-day-difference-to-string
                         end-jday start-jday)
                        (current-date-time-string)))
               (force-output)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  main code                                            ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help title-string)
    (begin
      (local-display-version title-string)
      (display
       (format #f "options~%"))
      (display
       (format #f "-h, --help    :  print this message~%"))
      (display
       (format #f "-v, --version :  display program version~%"))
      (force-output)
      ))
  (begin
    (let ((version-string "2022-05-18")
          (alen (length args)))
      (let ((title-string
             (format #f "dfa ex-1-6 ~a" version-string)))
        (begin
          (if (> alen 1)
              (begin
                (let ((a1 (list-ref args 1)))
                  (begin
                    (if (or
                         (string-ci=? a1 "-v")
                         (string-ci=? a1 "--version")
                         (string-ci=? a1 "-version"))
                        (begin
                          (local-display-version title-string)
                          (quit)
                          ))
                    (if (or
                         (string-ci=? a1 "-h")
                         (string-ci=? a1 "--help")
                         (string-ci=? a1 "-help"))
                        (begin
                          (local-display-help title-string)
                          (quit)
                          ))
                    (local-display-help title-string)
                    ))
                ))

          ;;; run tests
          (let ((debug-flag #t))
            (begin
              (time-code-macro
               (begin
                 (run-all-tests debug-flag)
                 ))
              ))

          (main-loop)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
