#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  pi spigot test                                       ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;###  last updated May 16, 2022                            ###
;;;###                                                       ###
;;;###  updated February 18, 2020                            ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;; The Computer Language Benchmarks Game
;;; http://benchmarksgame.alioth.debian.org/
;;; language = guile 3.0.7
;;; contributed by robert haramoto
;;;
;;;
;;; adapted from: https://benchmarksgame-team.pages.debian.net/benchmarksgame/program/pidigits-racket-1.html
;;;
;;;

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9-format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin support function definitions                   ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (current-date-time-string)
  (begin
    (let ((this-date (srfi-19:current-date)))
      (begin
        (string-downcase
         (srfi-19:date->string this-date "~B ~d, ~Y  ~I:~M:~S ~p"))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### expects 2 julian days (plain numbers)
;;;### differences between 2 julian days is in days (or a fraction of a day)
(define (julian-day-difference-to-string dend dstart)
  (define (local-process-sub-day day-fraction)
    (begin
      (let ((nsecs (* day-fraction 24.0 60.0 60.0))
            (nmins (truncate (* day-fraction 24.0 60.0)))
            (nhours
             (inexact->exact
              (truncate (* day-fraction 24.0)))))
        (let ((nminutes
               (inexact->exact
                (truncate (- nmins (* nhours 60.0))))))
          (let ((nseconds
                 (* 0.0010
                    (truncate
                     (* 1000.0
                        (- nsecs
                           (+ (* nhours 60.0 60.0)
                              (* nminutes 60.0))))))))
            (let ((seconds-string
                   (string-trim
                    (ice-9-format:format #f "~5,3f seconds" nseconds)))
                  (minutes-string
                   (format #f "~a minutes" nminutes))
                  (hours-string
                   (format #f "~a hours" nhours)))
              (begin
                (if (= nseconds 1.0)
                    (begin
                      (set! seconds-string "1 second")
                      ))
                (if (= nminutes 1)
                    (begin
                      (set! minutes-string "1 minute")
                      ))
                (if (= nhours 1.0)
                    (begin
                      (set! hours-string "1 hour")
                      ))

                (if (<= nhours 0)
                    (begin
                      (if (<= nminutes 0.0)
                          (begin
                            seconds-string)
                          (begin
                            (format
                             #f "~a, ~a"
                             minutes-string seconds-string)
                            )))
                    (begin
                      (if (<= nminutes 0)
                          (begin
                            (string-trim
                             (format
                              #f "~a, ~a"
                              hours-string seconds-string)))
                          (begin
                            (string-trim
                             (format
                              #f "~a, ~a, ~a"
                              hours-string minutes-string
                              seconds-string))
                            ))
                      ))
                ))
            )))
      ))
  (begin
    (if (and (number? dend) (number? dstart))
        (begin
          (let ((jd-diff (exact->inexact (- dend dstart))))
            (begin
              (if (< jd-diff 1.0)
                  (begin
                    (let ((tstring
                           (local-process-sub-day jd-diff)))
                      (begin
                        tstring
                        )))
                  (begin
                    (let ((ndays
                           (inexact->exact (truncate jd-diff))))
                      (let ((dfract-diff (- jd-diff ndays)))
                        (let ((tstring (local-process-sub-day dfract-diff)))
                          (let ((ttstring
                                 (string-trim
                                  (format #f "~a days, ~a" ndays tstring))))
                            (begin
                              (if (= ndays 1)
                                  (begin
                                    (set! ttstring
                                          (format #f "1 day, ~a" tstring))
                                    ))

                              ttstring
                              )))
                        ))
                    ))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax time-code-macro
  (syntax-rules ()
    ((time-code-macro body)
     (begin
       (let ((start-jday (srfi-19:current-julian-day)))
         (begin
           body

           (let ((end-jday (srfi-19:current-julian-day)))
             (let ((day-diff-string
                    (julian-day-difference-to-string
                     end-jday start-jday))
                   (current-string
                    (current-date-time-string)))
               (begin
                 (display
                  (format
                   #f "elapsed time = ~a : ~a~%"
                   day-diff-string current-string))
                 (force-output)
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin pi-spigot function definitions                 ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (floor-ev qq rr ss tt nth-digit)
  (begin
    (let ((result
           (quotient
            (+ (* qq nth-digit) rr)
            (+ (* ss nth-digit) tt)
            )))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (compose qq rr ss tt qq2 rr2 ss2 tt2)
  (begin
    (let ((result-1 (+ (* qq qq2) (* rr ss2)))
          (result-2 (+ (* qq rr2) (* rr tt2)))
          (result-3 (+ (* ss qq2) (* tt ss2)))
          (result-4 (+ (* ss rr2) (* tt tt2))))
      (begin
        (list result-1 result-2 result-3 result-4)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (next qq rr ss tt)
  (begin
    (floor-ev qq rr ss tt 3)
    ))

;;;#############################################################
;;;#############################################################
(define (safe? qq rr ss tt nn)
  (begin
    (= nn (floor-ev qq rr ss tt 4))
    ))

;;;#############################################################
;;;#############################################################
(define (prod qq rr ss tt nn)
  (begin
    (let ((result-list
           (compose 10 (* -10 nn) 0 1 qq rr ss tt)))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (mk qq rr ss tt kk)
  (begin
    (let ((result-list
           (compose qq rr ss tt kk
                    (* 2 (1+ (* 2 kk)))
                    0
                    (1+ (* 2 kk)))))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (digit-spigot kk qq rr ss tt nn row col acc-string)
  (begin
    (if (> nn 0)
        (begin
          (let ((yy (next qq rr ss tt)))
            (begin
              (if (safe? qq rr ss tt yy)
                  (begin
                    (let ((rlist
                           (prod qq rr ss tt yy)))
                      (let ((qq-2 (list-ref rlist 0))
                            (rr-2 (list-ref rlist 1))
                            (ss-2 (list-ref rlist 2))
                            (tt-2 (list-ref rlist 3))
                            (next-nn (- nn 1))
                            (next-row (+ row 10)))
                        (begin
                          (if (= col 10)
                              (begin
                                ;;; end of row
                                (display
                                 (ice-9-format:format
                                  #f "~a      :~:d~%"
                                  acc-string next-row))
                                (force-output)
                                (let ((next-acc-string
                                       (format #f "~a" yy)))
                                  (begin
                                    (digit-spigot
                                     kk qq-2 rr-2 ss-2 tt-2
                                     next-nn next-row 1
                                     next-acc-string)
                                    )))
                              (begin
                                (let ((next-acc-string
                                       (string-append
                                        acc-string
                                        (format #f "~a" yy)
                                        )))
                                  (begin
                                    (digit-spigot
                                     kk qq-2 rr-2 ss-2 tt-2
                                     next-nn row (1+ col)
                                     next-acc-string)
                                    ))
                                ))
                          ))))
                  (begin
                    (let ((rlist (mk qq rr ss tt kk)))
                      (let ((qq-2 (list-ref rlist 0))
                            (rr-2 (list-ref rlist 1))
                            (ss-2 (list-ref rlist 2))
                            (tt-2 (list-ref rlist 3)))
                        (begin
                          (digit-spigot
                           (1+ kk) qq-2 rr-2 ss-2 tt-2
                           nn row col acc-string)
                          )))
                    ))
              )))
        (begin
          (let ((next-row (+ row col)))
            (begin
              (display
               (ice-9-format:format
                #f "~a~a      :~:d~%"
                acc-string (make-string (- 10 col) #\space)
                next-row))
              (force-output)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (pi-spigot-racket-version ndigits)
  (begin
    (digit-spigot 1 1 0 0 1 ndigits 0 0 "")
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main code                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((ndigits 27)
          (args (command-line))
          (version-string "2022-05-16"))
      (begin
        (if (and (not (equal? args #f))
                 (> (length args) 1))
            (begin
              (let ((nstring (string-delete #\, (list-ref args 1))))
                (let ((next-ndigits (string->number nstring)))
                  (begin
                    (if (and (not (equal? next-ndigits #f))
                             (number? next-ndigits))
                        (begin
                          (set! ndigits next-ndigits)
                          ))
                    )))
              ))

        (display
         (format #f "starting pi-spigots program (version ~a)~%"
                 version-string))
        (display
         (ice-9-format:format
          #f "compute the first ~:d digits of pi : ~a~%"
          ndigits (current-date-time-string)))
        (newline)
        (force-output)

        (time-code-macro
         (begin
           (pi-spigot-racket-version ndigits)
           ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
