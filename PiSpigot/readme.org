################################################################
################################################################
###                                                          ###
###  Pi Spigot                                               ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################
Generate Digits of Pi

Pi-spigot is an algorithm that computes the digits of pi. Spigot denotes
the fact that once the program outputs a digit it is no longer needed.

There is a fun comparison of many different languages at the benchmarksgame
pi-digits website.

My implementation is translated from a racket script, and executed in about
1.4 seconds, approximately the same speed.

The fastest implementation was in Rust, and took 0.71 seconds.
see also: https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/pidigits.html

################################################################
################################################################
Notes:
(1) to check that it's correct
    ./ps.scm > tmp-27.txt
    sdiff -s tmp-27.txt output-check.txt

    where the output-check.txt file is from the benchmarkgames website:
    https://benchmarksgame-team.pages.debian.net/benchmarksgame/download/pidigits-output.txt
    see also the first 1 million digits of pi: https://www.piday.org/million/

(2) to run this program for the first 100 digits of pi
    ./ps.scm 100 > out.txt


################################################################
################################################################
For more:
About Pi
https://oeis.org/wiki/Pi
sequence at: https://oeis.org/A000796

See the full comparison against all languages:
https://benchmarksgame-team.pages.debian.net/benchmarksgame/performance/pidigits.html
see also: https://rosettacode.org/wiki/Pi

The racket version, from which this guile program was adapted:
https://benchmarksgame-team.pages.debian.net/benchmarksgame/program/pidigits-racket-1.html

Unbounded Spigot Algorithms for the Digits of Pi
https://www.cs.ox.ac.uk/people/jeremy.gibbons/publications/spigot.pdf

The spigot algorithm
http://www.pi314.net/eng/goutte.php
see also: https://en.wikipedia.org/wiki/Spigot_algorithm
and: https://oeis.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula

For a good explanation of how the Bailey-Borwein-Plouffe spigot formula was found:
https://www.ams.org/samplings/feature-column/fcarc-pi
see also: https://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula


################################################################
################################################################

Assumes that guile 3.0 is installed in /usr/bin.

Uses the unlicense public domain license.
See the UNLICENSE file, or https://unlicense.org/


################################################################
################################################################
History

last updated <2025-02-17 Mon>
updated <2024-07-22 Mon>
updated <2022-05-16 Mon>
updated <2020-02-18 Tue>


################################################################
################################################################

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
