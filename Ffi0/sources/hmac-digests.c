#include <stdio.h>
#include <string.h>
#include <openssl/crypto.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <libguile.h>

/*
  This is free and unencumbered software released into the public domain.

  Anyone is free to copy, modify, publish, use, compile, sell, or
  distribute this software, either in source code form or as a compiled
  binary, for any purpose, commercial or non-commercial, and by any
  means.

  In jurisdictions that recognize copyright laws, the author or authors
  of this software dedicate any and all copyright interest in the
  software to the public domain. We make this dedication for the benefit
  of the public at large and to the detriment of our heirs and
  successors. We intend this dedication to be an overt act of
  relinquishment in perpetuity of all present and future rights to this
  software under copyright law.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
  OTHER DEALINGS IN THE SOFTWARE.

  For more information, please refer to <http://unlicense.org/>
*/

SCM scm_digest_wrapper(SCM digest_name, SCM s1) {
  EVP_MD_CTX *mdctx;
  char *c_dname, *c_s1;
  const EVP_MD *md;
  unsigned char md_value[EVP_MAX_MD_SIZE];
  const MAX_SIZE = 2 * EVP_MAX_MD_SIZE + 1;
  char c_md_string[MAX_SIZE];
  int md_len, i;

  scm_dynwind_begin (0);
  c_dname = scm_to_locale_string (digest_name);
  /* Call 'free (c_dname)' when the dynwind context is left. */
  scm_dynwind_free (c_dname);

  c_s1 = scm_to_locale_string (s1);
  /* Call 'free (c_s1)' when the dynwind context is left. */
  scm_dynwind_free (c_s1);

  OPENSSL_init_crypto(OPENSSL_INIT_LOAD_CONFIG, NULL);
  OpenSSL_add_all_algorithms();
  md = EVP_get_digestbyname(c_dname);
  if(!md) {
    int max_char = 80;
    char err_msg[max_char];
    snprintf(err_msg, max_char - 1, "error: unknown message digest %s\n", c_dname);
    printf("%s\n", err_msg);

    return scm_take_locale_string(err_msg);;
  }

  mdctx = EVP_MD_CTX_create();
  EVP_DigestInit_ex(mdctx, md, NULL);
  EVP_DigestUpdate(mdctx, c_s1, strlen(c_s1));
  EVP_DigestFinal_ex(mdctx, md_value, &md_len);
  EVP_MD_CTX_destroy(mdctx);

  char c_stmp[4];
  int c_len = 0;
  c_md_string[0] = 0;
  for(i = 0; i < md_len; i++) {
    snprintf(c_stmp, 3, "%02x", md_value[i]);
    strncat(c_md_string, c_stmp, 3);
    c_len = c_len + 2;
  }

  EVP_cleanup();

  scm_dynwind_end();

  return scm_from_locale_stringn(c_md_string, c_len);
}

void init_digests() {
  scm_c_define_gsubr("digests", 2, 0, 0, scm_digest_wrapper);
  scm_c_export("digests", NULL);
}

/*
void scm_init_hash_digests_module ()
{
  scm_c_define_module ("hash digests", init_digests, NULL);
}
*/
