#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sha1sum and md5sum test                              ###
;;;###                                                       ###
;;;###  last updated May 31, 2022                            ###
;;;###                                                       ###
;;;###  updated February 10, 2020                            ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "../sources" "objects"))

(set! %load-compiled-path
      (append %load-compiled-path
              (list "." "objects" "../objects"
                    "sources" "../sources")))

;;;#############################################################
;;;#############################################################
(use-modules (system foreign))
(use-modules (system foreign-library))

;;; (load-extension "sources/libhmac-digests" "scm_init_hash_digests_module")
(load-extension "objects/libhmac-digests" "init_digests")

;;; (load-extension "./sources/libhmac-digests.a" "init_digests")
;;; (load-extension
;;; "sources/libhmac-digests"
;;; "scm_init_hash_digests_module")

;;;#############################################################
;;;#############################################################
(define-syntax display-code-macro
  (syntax-rules ()
    ((display-code-macro d1 text1)
     (begin
       (let ((result-text (digests d1 text1)))
         (begin
           (display
            (format
             #f "~a(~s) = ~s~%"
             d1 text1 result-text))
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (define hashsum-lib
      (dynamic-link "objects/libhmac-digests.so"))
    (dynamic-call "init_digests" hashsum-lib)

    (display (format #f "begin guile test~%"))
    (let ((text1 "abc\n"))
      (begin
        (display-code-macro "sha1" text1)
        (system "sha1sum abc.txt")

        (newline)
        (display-code-macro "md5" text1)
        (system "md5sum abc.txt")
        ))

    (newline)
    (let ((text1 "badg3r5"))
      (begin
        (display-code-macro "sha1" text1)
        (display-code-macro "sha256" text1)
        ))

    (newline)
    (display (format #f "end guile test~%"))
    ))
