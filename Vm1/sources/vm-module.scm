;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  virtual machine functions                            ###
;;;###                                                       ###
;;;###  last updated July 25, 2024                           ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start vm-module
(define-module (vm-module)
  #:export (make-desc-hash
            make-nopends-hash

            array-to-string
            opcode-to-string
            cpu
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - advanced formatting command
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;#############################################################
;;;#############################################################
;;; define global machine instructions
(define-public instr-iadd 0001)
(define-public instr-isub 0002)
(define-public instr-imul 0003)
(define-public instr-ilt 0004)
(define-public instr-ieq 0005)
(define-public instr-br 0006)
(define-public instr-brt 0007)
(define-public instr-brf 0008)
(define-public instr-iconst 0009)
(define-public instr-load 0010)
(define-public instr-gload 0011)
(define-public instr-store 0012)
(define-public instr-gstore 0013)
(define-public instr-print 0014)
(define-public instr-pop 0015)
(define-public instr-call 0016)
(define-public instr-ret 0017)
(define-public instr-halt 0018)

;;;#############################################################
;;;#############################################################
(define (make-desc-hash)
  (begin
    (let ((instr-desc-htable (make-hash-table 20)))
      (begin
        (hash-set! instr-desc-htable instr-iadd "iadd")
        (hash-set! instr-desc-htable instr-isub "isub")
        (hash-set! instr-desc-htable instr-imul "imul")
        (hash-set! instr-desc-htable instr-ilt "ilt")
        (hash-set! instr-desc-htable instr-ieq "ieq")
        (hash-set! instr-desc-htable instr-br "br")
        (hash-set! instr-desc-htable instr-brt "brt")
        (hash-set! instr-desc-htable instr-brf "brf")
        (hash-set! instr-desc-htable instr-iconst "iconst")
        (hash-set! instr-desc-htable instr-load "load")
        (hash-set! instr-desc-htable instr-gload "gload")
        (hash-set! instr-desc-htable instr-store "store")
        (hash-set! instr-desc-htable instr-gstore "gstore")
        (hash-set! instr-desc-htable instr-print "print")
        (hash-set! instr-desc-htable instr-pop "pop")
        (hash-set! instr-desc-htable instr-call "call")
        (hash-set! instr-desc-htable instr-ret "ret")
        (hash-set! instr-desc-htable instr-halt "halt")

        instr-desc-htable
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-nopends-hash)
  (begin
    (let ((instr-nopends-htable (make-hash-table 20)))
      (begin
        (hash-set! instr-nopends-htable instr-iadd 0)
        (hash-set! instr-nopends-htable instr-isub 0)
        (hash-set! instr-nopends-htable instr-imul 0)
        (hash-set! instr-nopends-htable instr-ilt 0)
        (hash-set! instr-nopends-htable instr-ieq 0)
        (hash-set! instr-nopends-htable instr-br 1)
        (hash-set! instr-nopends-htable instr-brt 1)
        (hash-set! instr-nopends-htable instr-brf 1)
        (hash-set! instr-nopends-htable instr-iconst 1)
        (hash-set! instr-nopends-htable instr-load 1)
        (hash-set! instr-nopends-htable instr-gload 1)
        (hash-set! instr-nopends-htable instr-store 1)
        (hash-set! instr-nopends-htable instr-gstore 1)
        (hash-set! instr-nopends-htable instr-print 0)
        (hash-set! instr-nopends-htable instr-pop 0)
        (hash-set! instr-nopends-htable instr-call 2)
        (hash-set! instr-nopends-htable instr-ret 0)
        (hash-set! instr-nopends-htable instr-halt 0)

        instr-nopends-htable
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### turn stack into a string
(define (array-to-string an-array arr-max)
  (begin
    (let ((result "["))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii arr-max))
          (begin
            (let ((elem (array-ref an-array ii)))
              (begin
                (set!
                 result
                 (string-append
                  result
                  (format #f " ~a" elem)))
                ))
            ))
        (set! result (string-append result " ]"))
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  disassemble op codes
(define (opcode-to-string
         op-code ip instructions-array
         instr-desc-htable instr-nopends-htable)
  (begin
    (let ((op-desc (hash-ref instr-desc-htable op-code ""))
          (nops (hash-ref instr-nopends-htable op-code 0))
          (alen (car (array-dimensions instructions-array))))
      (let ((result
             (ice-9-format:format #f "~5,'0d : ~a" ip op-desc)))
        (begin
          (cond
           ((= nops 1)
            (begin
              (if (< ip alen)
                  (begin
                    (set! result
                          (string-append
                           result
                           (ice-9-format:format
                            #f " ~a"
                            (array-ref instructions-array (1+ ip)))))
                    ))
              ))
           ((= nops 2)
            (begin
              (if (< ip alen)
                  (begin
                    (set!
                     result
                     (string-append
                      result
                      (ice-9-format:format
                       #f " ~a, ~a"
                       (array-ref instructions-array (1+ ip))
                       (array-ref instructions-array (+ ip 2)))))
                    ))
              )))

          result
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (cpu instr-array main-ip
             globals-array max-gl-count
             stack-size trace-flag)
  (begin
    (let ((desc-htable (make-desc-hash))
          (nopends-htable (make-nopends-hash))
          (stack-array (make-array 0 stack-size))
          (ilen (car (array-dimensions instr-array)))
          (sp -1)
          (fp 0)
          (ip main-ip)
          (gl-count max-gl-count)
          (op-code (array-ref instr-array main-ip)))
      (begin
        (while (and (not (equal? op-code instr-halt))
                    (< ip ilen))
          (begin
            (if (equal? trace-flag #t)
                (begin
                  (let ((op-string
                         (opcode-to-string
                          op-code ip instr-array
                          desc-htable nopends-htable)))
                    (begin
                      (display (format #f "~a" op-string))
                      ))
                  ))

            (set! ip (1+ ip))
            (cond
             ((= op-code instr-iconst)
              (begin
                (set! sp (1+ sp))
                (let ((arg (array-ref instr-array ip)))
                  (begin
                    (array-set! stack-array arg sp)
                    ))
                (set! ip (1+ ip))
                ))
             ((= op-code instr-iadd)
              (begin
                (let ((bb (array-ref stack-array sp))
                      (aa (array-ref stack-array (- sp 1))))
                  (let ((cc (+ aa bb)))
                    (begin
                      (set! sp (1- sp))
                      (array-set! stack-array cc sp)
                      )))
                ))
             ((= op-code instr-isub)
              (begin
                (let ((bb (array-ref stack-array sp))
                      (aa (array-ref stack-array (- sp 1))))
                  (let ((cc (- aa bb)))
                    (begin
                      (set! sp (1- sp))
                      (array-set! stack-array cc sp)
                      )))
                ))
             ((= op-code instr-imul)
              (begin
                (let ((bb (array-ref stack-array sp))
                      (aa (array-ref stack-array (- sp 1))))
                  (let ((cc (* aa bb)))
                    (begin
                      (set! sp (1- sp))
                      (array-set! stack-array cc sp)
                      )))
                ))
             ((= op-code instr-ilt)
              (begin
                (let ((bb (array-ref stack-array sp))
                      (aa (array-ref stack-array (- sp 1))))
                  (let ((cc (< aa bb)))
                    (begin
                      (set! sp (1- sp))
                      (array-set! stack-array cc sp)
                      )))
                ))
             ((= op-code instr-ieq)
              (begin
                (let ((bb (array-ref stack-array sp))
                      (aa (array-ref stack-array (- sp 1))))
                  (let ((cc (= aa bb)))
                    (begin
                      (set! sp (1- sp))
                      (array-set! stack-array cc sp)
                      )))
                ))
             ((= op-code instr-br)
              (begin
                (set! ip (array-ref instr-array ip))
                ))
             ((= op-code instr-brt)
              (begin
                (let ((addr (array-ref instr-array ip))
                      (st-val (array-ref stack-array sp)))
                  (begin
                    (set! sp (1- sp))
                    (if (equal? st-val #t)
                        (begin
                          (set! ip addr))
                        (begin
                          (set! ip (1+ ip))
                          ))
                    ))
                ))
             ((= op-code instr-brf)
              (begin
                (let ((addr (array-ref instr-array ip))
                      (st-val (array-ref stack-array sp)))
                  (begin
                    (set! sp (1- sp))
                    (if (equal? st-val #f)
                        (begin
                          (set! ip addr))
                        (begin
                          (set! ip (1+ ip))
                          ))
                    ))
                ))
             ((= op-code instr-store)
              (begin
                (let ((offset (array-ref instr-array ip))
                      (val (array-ref stack-array sp)))
                  (begin
                    (set! sp (1- sp))
                    (array-set! stack-array val (+ fp offset))
                    (set! ip (1+ ip))
                    ))
                ))
             ((= op-code instr-load)
              (begin
                (let ((offset (array-ref instr-array ip)))
                  (let ((val (array-ref stack-array (+ fp offset))))
                    (begin
                      (set! sp (1+ sp))
                      (array-set! stack-array val sp)
                      (set! ip (1+ ip))
                      )))
                ))
             ((= op-code instr-gstore)
              (begin
                (let ((g-addr (array-ref instr-array ip))
                      (aa (array-ref stack-array sp)))
                  (begin
                    (array-set! globals-array aa g-addr)
                    (set! gl-count (1+ gl-count))
                    (set! sp (1- sp))
                    (set! ip (1+ ip))
                    ))
                ))
             ((= op-code instr-gload)
              (begin
                (let ((g-addr (array-ref instr-array ip)))
                  (let ((aa (array-ref globals-array g-addr)))
                    (begin
                      (set! sp (1+ sp))
                      (array-set! stack-array aa sp)
                      (set! ip (1+ ip))
                      )))
                ))
             ((= op-code instr-print)
              (begin
                (let ((st-top (array-ref stack-array sp)))
                  (begin
                    (if (equal? trace-flag #t)
                        (begin
                          (newline)
                          ))
                    (display (format #f "~a~%" st-top))
                    (force-output)
                    (set! sp (1- sp))
                    ))
                ))
             ((= op-code instr-pop)
              (begin
                (set! sp (1- sp))
                ))
             ((= op-code instr-call)
              (begin
                (let ((addr (array-ref instr-array ip))
                      (num-args (array-ref instr-array (1+ ip))))
                  (begin
                    (set! ip (+ ip 2))
                    (set! sp (1+ sp))
                    (array-set! stack-array num-args sp)
                    (set! sp (1+ sp))
                    (array-set! stack-array fp sp)
                    (set! sp (1+ sp))
                    (array-set! stack-array ip sp)
                    (set! fp sp)
                    (set! ip addr)
                    ))
                ))
             ((= op-code instr-ret)
              (begin
                (let ((rvalue (array-ref stack-array sp))
                      (num-args #f))
                  (begin
                    (set! sp fp)
                    (set! ip (array-ref stack-array sp))
                    (set! sp (1- sp))
                    (set! fp (array-ref stack-array sp))
                    (set! sp (1- sp))
                    (set! num-args (array-ref stack-array sp))
                    (set! sp (- sp num-args))
                    (array-set! stack-array rvalue sp)
                    ))
                ))
             ((= op-code instr-halt)
              (begin
                (break)
                ))
             (else
              (begin
                (display
                 (ice-9-format:format
                  #f "invalid instruction at ip = ~5,'0d : instr = ~a~%"
                  ip (hash-ref desc-htable op-code op-code)))
                (force-output)
                )))

            (if (equal? trace-flag #t)
                (begin
                  (let ((stack-string
                         (array-to-string stack-array (1+ sp)))
                        (globals-string
                         (array-to-string globals-array gl-count)))
                    (begin
                      (if (not (string-ci=? stack-string "[ ]"))
                          (begin
                            (display
                             (format #f " : stack = ~a" stack-string))
                            ))
                      (if (not (string-ci=? globals-string "[ ]"))
                          (begin
                            (display
                             (format #f " : globals = ~a" globals-string))
                            ))
                      (newline)
                      (force-output)
                      ))
                  ))

            (set! op-code (array-ref instr-array ip))
            ))

        ;;; display halt instruction
        (if (equal? trace-flag #t)
            (begin
              (let ((op-string
                     (opcode-to-string
                      op-code ip instr-array
                      desc-htable nopends-htable))
                    (stack-string
                     (array-to-string stack-array (1+ sp)))
                    (globals-string
                     (array-to-string globals-array gl-count)))
                (begin
                  (display
                   (format #f "~a" op-string))
                  (if (not (string-ci=? stack-string "[ ]"))
                      (begin
                        (display
                         (format #f " : stack = ~a" stack-string))
                        ))
                  (if (not (string-ci=? globals-string "[ ]"))
                      (begin
                        (display
                         (format #f " : globals = ~a" globals-string))
                        ))
                  (newline)
                  (force-output)
                  ))
              ))

        (set! op-code (array-ref instr-array ip))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
