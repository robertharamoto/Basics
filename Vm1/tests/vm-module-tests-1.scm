;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for vm-module.scm                         ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((vm-module)
              :renamer (symbol-prefix-proc 'vm-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-array-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-array-to-string-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list->array 1 (list 0 1))
                 2 "[ 0 1 ]")
           (list (list->array 1 (list 1 2))
                 2 "[ 1 2 ]")
           (list (list->array 1 (list 1 2 3))
                 3 "[ 1 2 3 ]")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((an-array (list-ref alist 0))
                  (sp-max (list-ref alist 1))
                  (shouldbe-string (list-ref alist 2)))
              (let ((result-string
                     (vm-module:array-to-string an-array sp-max)))
                (let ((err-1
                       (format
                        #f "~a : (~a) : error "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "for array = ~a, sp-max = ~a, "
                        an-array sp-max))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe-string
                                  result-string)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-opcode-to-string-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-opcode-to-string-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            vm-module:instr-iconst 0
            (list->array
             1 (list vm-module:instr-iconst 99 vm-module:instr-print))
            "00000 : iconst 99")
           (list
            vm-module:instr-print 2
            (list->array
             1 (list vm-module:instr-iconst 99 vm-module:instr-print))
            "00002 : print")
           (list
            vm-module:instr-iconst 0
            (list->array
             1 (list vm-module:instr-iconst 2
                     vm-module:instr-iconst 3
                     vm-module:instr-iadd
                     vm-module:instr-print
                     vm-module:instr-halt))
            "00000 : iconst 2")
           (list
            vm-module:instr-iconst 2
            (list->array
             1 (list vm-module:instr-iconst 2
                     vm-module:instr-iconst 3
                     vm-module:instr-iadd
                     vm-module:instr-print
                     vm-module:instr-halt))
            "00002 : iconst 3")
           (list
            vm-module:instr-iadd 4
            (list->array
             1 (list vm-module:instr-iconst 2
                     vm-module:instr-iconst 3
                     vm-module:instr-iadd
                     vm-module:instr-print
                     vm-module:instr-halt))
            "00004 : iadd")
           (list
            vm-module:instr-print 5
            (list->array
             1 (list vm-module:instr-iconst 2
                     vm-module:instr-iconst 3
                     vm-module:instr-iadd
                     vm-module:instr-print
                     vm-module:instr-halt))
            "00005 : print")
           (list
            vm-module:instr-halt 6
            (list->array
             1 (list vm-module:instr-iconst 2
                     vm-module:instr-iconst 3
                     vm-module:instr-iadd
                     vm-module:instr-print
                     vm-module:instr-halt))
            "00006 : halt")
           ))
         (desc-htable (vm-module:make-desc-hash))
         (nopends-htable (vm-module:make-nopends-hash))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((opcode (list-ref alist 0))
                  (ip (list-ref alist 1))
                  (instr-array (list-ref alist 2))
                  (shouldbe-string (list-ref alist 3)))
              (let ((result-string
                     (vm-module:opcode-to-string
                      opcode ip instr-array
                      desc-htable nopends-htable)))
                (let ((err-1
                       (format
                        #f "~a : (~a) : error for "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "opcode =  ~a, ip = ~a, "
                        opcode ip))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe-string
                                  result-string)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
