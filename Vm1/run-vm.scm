#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  simple virtual machine test                          ###
;;;###                                                       ###
;;;###  last updated August 5, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "tests"))

(set! %load-compiled-path
      (append %load-compiled-path (list "." "objects")))

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### vm-module for virtual machine functions
(use-modules ((vm-module)
              :renamer (symbol-prefix-proc 'vm-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin vm functions                                   ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;; test iconst and print
(define (simple-test-1)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 4)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 99 1)
          (array-set! instr-array vm-module:instr-print 2)
          (array-set! instr-array vm-module:instr-halt 3)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test iadd/isub/imul
(define (simple-test-2)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 19)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 5 1)
          (array-set! instr-array vm-module:instr-iconst 2)
          (array-set! instr-array 6 3)
          (array-set! instr-array vm-module:instr-iadd 4)
          (array-set! instr-array vm-module:instr-print 5)
          (array-set! instr-array vm-module:instr-iconst 6)
          (array-set! instr-array 5 7)
          (array-set! instr-array vm-module:instr-iconst 8)
          (array-set! instr-array 6 9)
          (array-set! instr-array vm-module:instr-isub 10)
          (array-set! instr-array vm-module:instr-print 11)
          (array-set! instr-array vm-module:instr-iconst 12)
          (array-set! instr-array 5 13)
          (array-set! instr-array vm-module:instr-iconst 14)
          (array-set! instr-array 6 15)
          (array-set! instr-array vm-module:instr-imul 16)
          (array-set! instr-array vm-module:instr-print 17)
          (array-set! instr-array vm-module:instr-halt 18)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test gload/gstore
(define (simple-test-3)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 8)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 99 1)
          (array-set! instr-array vm-module:instr-gstore 2)
          (array-set! instr-array 0 3)
          (array-set! instr-array vm-module:instr-gload 4)
          (array-set! instr-array 0 5)
          (array-set! instr-array vm-module:instr-print 6)
          (array-set! instr-array vm-module:instr-halt 7)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test load/store
(define (simple-test-4)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 8)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 99 1)
          (array-set! instr-array vm-module:instr-store 2)
          (array-set! instr-array 0 3)
          (array-set! instr-array vm-module:instr-load 4)
          (array-set! instr-array 0 5)
          (array-set! instr-array vm-module:instr-print 6)
          (array-set! instr-array vm-module:instr-halt 7)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test branch
(define (simple-test-5)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 10)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 99 1)
          (array-set! instr-array vm-module:instr-br 2)
          (array-set! instr-array 6 3)
          (array-set! instr-array vm-module:instr-iconst 4)
          (array-set! instr-array 55 5)
          (array-set! instr-array vm-module:instr-iconst 6)
          (array-set! instr-array 88 7)
          (array-set! instr-array vm-module:instr-print 8)
          (array-set! instr-array vm-module:instr-halt 9)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test branch if true
(define (simple-test-6)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 10)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 99 1)
          (array-set! instr-array vm-module:instr-brt 2)
          (array-set! instr-array #t 3)
          (array-set! instr-array vm-module:instr-iconst 4)
          (array-set! instr-array 55 5)
          (array-set! instr-array vm-module:instr-iconst 6)
          (array-set! instr-array 88 7)
          (array-set! instr-array vm-module:instr-print 8)
          (array-set! instr-array vm-module:instr-halt 9)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test branch if false
(define (simple-test-7)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 10)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 99 1)
          (array-set! instr-array vm-module:instr-brf 2)
          (array-set! instr-array #f 3)
          (array-set! instr-array vm-module:instr-iconst 4)
          (array-set! instr-array 55 5)
          (array-set! instr-array vm-module:instr-iconst 6)
          (array-set! instr-array 88 7)
          (array-set! instr-array vm-module:instr-print 8)
          (array-set! instr-array vm-module:instr-halt 9)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test ilt/ieq/pop
(define (simple-test-8)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 21)
          (main-ip 0)
          (gl-count 0)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (array-set! instr-array vm-module:instr-iconst 0)
          (array-set! instr-array 9 1)
          (array-set! instr-array vm-module:instr-iconst 2)
          (array-set! instr-array 11 3)
          (array-set! instr-array vm-module:instr-ilt 4)
          (array-set! instr-array vm-module:instr-brt 5)
          (array-set! instr-array 11 6)
          (array-set! instr-array vm-module:instr-iconst 7)
          (array-set! instr-array #f 8)
          (array-set! instr-array vm-module:instr-print 9)
          (array-set! instr-array vm-module:instr-halt 10)
          (array-set! instr-array vm-module:instr-iconst 11)
          (array-set! instr-array 9 12)
          (array-set! instr-array vm-module:instr-iconst 13)
          (array-set! instr-array 9 14)
          (array-set! instr-array vm-module:instr-ieq 15)
          (array-set! instr-array vm-module:instr-pop 16)
          (array-set! instr-array vm-module:instr-iconst 17)
          (array-set! instr-array 9 18)
          (array-set! instr-array vm-module:instr-print 19)
          (array-set! instr-array vm-module:instr-halt 20)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test factorial
(define (simple-test-9)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 29)
          (main-ip 22)
          (gl-count 0)
          (nn 3)
          (trace-flag #t))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (display (format #f "factorial(~a)~%" nn))
          (force-output)

          ;;; .def fact: args=1, locals=0
          ;;; if n < 2 return 1
          (array-set! instr-array vm-module:instr-load 0)
          (array-set! instr-array -3 1)
          (array-set! instr-array vm-module:instr-iconst 2)
          (array-set! instr-array 2 3)
          (array-set! instr-array vm-module:instr-ilt 4)
          (array-set! instr-array vm-module:instr-brf 5)
          (array-set! instr-array 10 6)
          (array-set! instr-array vm-module:instr-iconst 7)
          (array-set! instr-array 1 8)
          (array-set! instr-array vm-module:instr-ret 9)
          ;;; cont:
          ;;; return n = fact (n - 1)
          (array-set! instr-array vm-module:instr-load 10)
          (array-set! instr-array -3 11)
          (array-set! instr-array vm-module:instr-load 12)
          (array-set! instr-array -3 13)
          (array-set! instr-array vm-module:instr-iconst 14)
          (array-set! instr-array 1 15)
          (array-set! instr-array vm-module:instr-isub 16)
          (array-set! instr-array vm-module:instr-call 17)
          (array-set! instr-array 0 18)
          (array-set! instr-array 1 19)
          (array-set! instr-array vm-module:instr-imul 20)
          (array-set! instr-array vm-module:instr-ret 21)
          ;;; .def main: args=0, locals=0
          ;;; print fact( 3 )
          (array-set! instr-array vm-module:instr-iconst 22)
          (array-set! instr-array nn 23)
          (array-set! instr-array vm-module:instr-call 24)
          (array-set! instr-array 0 25)
          (array-set! instr-array 1 26)
          (array-set! instr-array vm-module:instr-print 27)
          (array-set! instr-array vm-module:instr-halt 28)
          (vm-module:cpu
           instr-array main-ip
           globals-array gl-count
           stack-size trace-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; test factorial
(define (simple-test-10)
  (begin
    (sub-test-10 4)
    (sub-test-10 5)
    (sub-test-10 6)
    (sub-test-10 7)
    ))

;;;#############################################################
;;;#############################################################
(define (sub-test-10 nn)
  (begin
    (let ((globals-size 1000)
          (stack-size 1000)
          (instr-size 29)
          (main-ip 22)
          (gl-count 0)
          (trace-flag #f))
      (let ((globals-array (make-array 0 globals-size))
            (instr-array (make-array 0 instr-size)))
        (begin
          (display (format #f "factorial(~a)~%" nn))
          (force-output)

          ;;; .def fact: args=1, locals=0
          ;;; if n < 2 return 1
          (array-set! instr-array vm-module:instr-load 0)
          (array-set! instr-array -3 1)
          (array-set! instr-array vm-module:instr-iconst 2)
          (array-set! instr-array 2 3)
          (array-set! instr-array vm-module:instr-ilt 4)
          (array-set! instr-array vm-module:instr-brf 5)
          (array-set! instr-array 10 6)
          (array-set! instr-array vm-module:instr-iconst 7)
          (array-set! instr-array 1 8)
          (array-set! instr-array vm-module:instr-ret 9)
          ;;; cont:
          ;;; return n = fact (n - 1)
          (array-set! instr-array vm-module:instr-load 10)
          (array-set! instr-array -3 11)
          (array-set! instr-array vm-module:instr-load 12)
          (array-set! instr-array -3 13)
          (array-set! instr-array vm-module:instr-iconst 14)
          (array-set! instr-array 1 15)
          (array-set! instr-array vm-module:instr-isub 16)
          (array-set! instr-array vm-module:instr-call 17)
          (array-set! instr-array 0 18)
          (array-set! instr-array 1 19)
          (array-set! instr-array vm-module:instr-imul 20)
          (array-set! instr-array vm-module:instr-ret 21)
          ;;; .def main: args=0, locals=0
          ;;; print fact( 3 )
          (array-set! instr-array vm-module:instr-iconst 22)
          (array-set! instr-array nn 23)
          (array-set! instr-array vm-module:instr-call 24)
          (array-set! instr-array 0 25)
          (array-set! instr-array 1 26)
          (array-set! instr-array vm-module:instr-print 27)
          (array-set! instr-array vm-module:instr-halt 28)
          (vm-module:cpu instr-array main-ip
                         globals-array gl-count
                         stack-size trace-flag)
          (newline)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main code                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-08-05")
          (globals-size 1000)
          (stack-size 1000)
          (instr-size 1000)
          (trace-flag #t))
      (begin
        (display
         (format
          #f "starting vm program (version ~a)~%"
          version-string))
        (display
         (ice-9-format:format
          #f "~a~%"
          (timer-module:current-date-time-string)))
        (newline)
        (force-output)

        (let ((tlist
               (list
                (list "simple test 1 - iconst/print" simple-test-1)
                (list "simple test 2 - iadd" simple-test-2)
                (list "simple test 3 - global load/store" simple-test-3)
                (list "simple test 4 - load/store" simple-test-4)
                (list "simple test 5 - branch" simple-test-5)
                (list "simple test 6 - branch if true" simple-test-6)
                (list "simple test 7 - branch if false" simple-test-7)
                (list "simple test 8 - if lt/eq/pop" simple-test-8)
                (list "simple test 9 - factorial( 3 )" simple-test-9)
                (list "simple test 10 - factorials" simple-test-10)
                )))
          (begin
            (timer-module:time-code-macro
             (begin
               (for-each
                (lambda (alist)
                  (begin
                    (timer-module:time-code-macro
                     (begin
                       (let ((astring (list-ref alist 0))
                             (afunc (list-ref alist 1)))
                         (begin
                           (display (format #f "~a~%" astring))
                           (afunc)
                           (newline)
                           ))
                       ))
                    (newline)
                    (force-output)
                    )) tlist)
               (display (format #f "total "))
               ))
            ))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
