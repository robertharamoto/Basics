;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for histogram-module.scm                  ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated August 20, 2011                              ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((histogram-module)
              :renamer (symbol-prefix-proc 'histogram-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (assert-hist-lists-equal
         shouldbe-list result-list
         sub-name error-message result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : list sizes not equal : "
              error-message))
            (err-2
             (format
              #f "shouldbe list = ~a, shouldbe size = ~a : "
              shouldbe-list shouldbe-length))
            (err-3
             (format
              #f "result list = ~a, result size = ~a"
              result-list result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append
            err-1 err-2 err-3)
           result-hash-table)

          (if (equal? shouldbe-length result-length)
              (begin
                (for-each
                 (lambda (list-elem)
                   (begin
                     (let ((found-flag
                            (member list-elem result-list)))
                       (let ((err-msg-1
                              (format
                               #f "~a : element not found : "
                               error-message))
                             (err-msg-2
                              (format
                               #f "shouldbe list = '~a', looking for '~a' : "
                               shouldbe-list list-elem))
                             (err-msg-3
                              (format
                               #f "result list = '~a'"
                               result-list)))
                         (begin
                           (unittest2:assert?
                            (not (equal? found-flag #f))
                            sub-name
                            (string-append
                             err-msg-1 err-msg-2 err-msg-3)
                            result-hash-table)
                           )))
                     )) shouldbe-list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-data-list-to-string-list-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-data-list-to-string-list-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 1 2 3 2 2 4 4 2) 10 10 0 10 #t #\#
                 (list "        4 +  #       "
                       "          |  #       "
                       "          |  #       "
                       "          |  #       "
                       "          |  #       "
                       "        2 +  # #     "
                       "          |  # #     "
                       "          | ####     "
                       "          | ####     "
                       "          | ####     "
                       "        0 +----------"
                       "          1    6   10"
                       ))
           (list (list 1 2 3 2 2 4 4 2) 10 10 0 10 #f #\#
                 (list "     10.0 +"
                       "          |"
                       "          |"
                       "          |"
                       "      6.0 +"
                       "          |#####"
                       "          |###"
                       "          |##########"
                       "          |###"
                       "          |"
                       "      1.0 +----------"
                       "          0    2    4"
                       ))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-data (list-ref alist 0))
                  (test-width (list-ref alist 1))
                  (test-height (list-ref alist 2))
                  (test-min (list-ref alist 3))
                  (test-max (list-ref alist 4))
                  (test-vertical (list-ref alist 5))
                  (test-char (list-ref alist 6))
                  (shouldbe-list (list-ref alist 7)))
              (let ((result-list
                     (histogram-module:data-list-to-string-list
                      test-data test-width test-height
                      test-min test-max test-vertical test-char)))
                (let ((err-start
                       (format #f "~a :: (~a) error for data list ~a"
                               sub-name test-label-index test-data)))
                  (let ((err-msg-1
                         (format #f "~a, width=~a, height=~a, "
                                 err-start test-width test-height))
                        (err-msg-2
                         (format #f "min=~a, max=~a, vertical=~a, "
                                 test-min test-max test-vertical))
                        (err-msg-3
                         (format #f "shouldbe-list = ~a, result-list = ~a"
                                 shouldbe-list result-list)))
                    (begin
                      (assert-hist-lists-equal
                       shouldbe-list result-list
                       sub-name
                       (string-append
                        err-msg-1 err-msg-2 err-msg-3)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
