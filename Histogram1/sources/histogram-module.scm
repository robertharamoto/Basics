;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  histogram module                                     ###
;;;###  given a hash table of number/count values            ###
;;;###  display an ascii text histogram                      ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated April 25, 2022                               ###
;;;###                                                       ###
;;;###  updated August 19, 2011                              ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start histogram modules
(define-module (histogram-module)
  #:export (data-list-to-string-list
            display-list-histogram
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### use format for format function
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################
;;; main-data-list a list of lists
;;; each element of main-data-list is (list min max count)
(define-syntax set-min-max-count-macro
  (syntax-rules ()
    ((set-min-max-count-macro
      main-data-list
      min-count max-count)
     (begin
       (for-each
        (lambda (bin-list)
          (begin
            (if (> (length bin-list) 0)
                (begin
                  (let ((this-count (list-ref bin-list 2)))
                    (begin
                      (if (or (= min-count 0)
                              (< this-count min-count))
                          (begin
                            (set! min-count this-count)
                            ))
                      (if (or (= max-count 0)
                              (> this-count max-count))
                          (begin
                            (set! max-count this-count)
                            ))
                      ))
                  ))
            )) main-data-list)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; main-data-list a list of lists
;;; each element of main-data-list is (list min max count)
(define (make-horizontal-x-axis-labels
         main-data-list left-margin nwidth)
  (begin
    (if (and
         (list? main-data-list)
         (> (length main-data-list) 2))
        (begin
          (let ((min-count 0)
                (max-count 0))
            (begin
              (set-min-max-count-macro
               main-data-list
               min-count max-count)

              (let ((nmiddle (quotient nwidth 2))
                    (mid-count (quotient (- max-count min-count) 2)))
                (let ((label-1
                       (utils-module:commafy-number min-count))
                      (label-2
                       (utils-module:commafy-number mid-count))
                      (label-3
                       (utils-module:commafy-number max-count)))
                  (let ((len-1 (string-length label-1))
                        (len-2 (string-length label-2))
                        (len-3 (string-length label-3)))
                    (let ((total-len (+ len-1 len-2 len-3))
                          (mid-pos (- nmiddle (quotient len-2 2)))
                          (last-pos (- nwidth len-3)))
                      (let ((mid-spaces (- mid-pos len-1))
                            (last-spaces
                             (+ (- last-pos nmiddle) (quotient len-2 2))))
                        (begin
                          (cond
                           ;;;### display three labels
                           ((< total-len nwidth)
                            (begin
                              (let ((space-1
                                     (make-string
                                      mid-spaces #\space))
                                    (space-2
                                     (make-string
                                      last-spaces #\space)))
                                (let ((this-row
                                       (string-append
                                        left-margin label-1 space-1
                                        label-2 space-2 label-3)))
                                  this-row
                                  ))
                              ))
                           ((< (+ len-1 len-3) nwidth)
                            (begin
                              (let ((space-1
                                     (make-string
                                      (- last-pos len-1)
                                      #\space)))
                                (let ((this-row
                                       (string-append
                                        left-margin label-1
                                        space-1 label-3)))
                                  (begin
                                    this-row
                                    )))
                              ))
                           (else
                            (begin
                              (let ((this-row
                                     (string-append
                                      left-margin label-1)))
                                (begin
                                  this-row
                                  ))
                              )))
                          )))
                    )))
              )))
        (begin
          ""
          ))
    ))

;;;#############################################################
;;;#############################################################
;;; main-data-list a list of lists
;;; each element of main-data-list is (list min max count)
(define (horizontal-histogram-string-list
         main-data-list nwidth nheight
         data-scale char)
  (begin
    (let ((out-string-list (list))
          (left-margin (make-string 10 #\space))
          (max-height (- nheight 1))
          (half-height (quotient nheight 2)))
      (begin
        (do ((ii-row max-height (- ii-row 1)))
            ((< ii-row 0))
          (begin
            (let ((this-bin-list
                   (list-ref main-data-list ii-row)))
              (let ((this-row
                     (string-append left-margin "|"))
                    (this-width
                     (inexact->exact
                      (truncate
                       (+ 0.50 (* (list-ref this-bin-list 2)
                                  data-scale))))))
                (begin
                  (if (or (= ii-row max-height)
                          (= ii-row half-height))
                      (begin
                        ;;;### add in a vertical axis label
                        (set!
                         this-row
                         (ice-9-format:format
                          #f "~9@a +"
                          (utils-module:round-float
                           (list-ref this-bin-list 1) 1)))
                        ))

                  (let ((row-string
                         (make-string this-width char)))
                    (begin
                      (set!
                       this-row
                       (string-append this-row row-string))
                      ))

                  (set!
                   out-string-list
                   (append out-string-list
                           (list this-row)))
                  )))
            ))

        ;;;### make the bottom axis
        (let ((this-bin-list
               (list-ref main-data-list 0)))
          (let ((this-margin
                 (ice-9-format:format
                  #f "~9@a +"
                  (utils-module:round-float
                   (list-ref this-bin-list 1) 1))))
            (let ((this-row
                   (string-append
                    this-margin
                    (make-string nwidth #\-))))
              (begin
                (set!
                 out-string-list
                 (append
                  out-string-list (list this-row)))
                ))
            ))

        ;;;### make the bottom axis labels
        (let ((this-row
               (make-horizontal-x-axis-labels
                main-data-list left-margin nwidth)))
          (begin
            (set!
             out-string-list
             (append
              out-string-list (list this-row)))
            ))

        out-string-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; main-data-list a list of lists
;;; each element of main-data-list is (list min max count)
(define (make-vertical-x-axis-labels
         main-data-list left-margin nwidth)
  (begin
    (if (and (list? main-data-list)
             (> (length main-data-list) 2))
        (begin
          (let ((nmiddle (quotient nwidth 2)))
            (let ((bin-list-1
                   (list-ref main-data-list 0))
                  (bin-list-2
                   (list-ref main-data-list nmiddle))
                  (bin-list-3
                   (list-ref main-data-list (- nwidth 1))))
              (let ((label-1
                     (utils-module:commafy-number
                      (utils-module:round-float
                       (list-ref bin-list-1 1) 1)))
                    (label-2
                     (utils-module:commafy-number
                      (utils-module:round-float
                       (list-ref bin-list-2 1) 1)))
                    (label-3
                     (utils-module:commafy-number
                      (utils-module:round-float
                       (list-ref bin-list-3 1) 1))))
                (let ((len-1 (string-length label-1))
                      (len-2 (string-length label-2))
                      (len-3 (string-length label-3)))
                  (let ((total-len (+ len-1 len-2 len-3))
                        (mid-pos
                         (- nmiddle (quotient len-2 2)))
                        (last-pos (- nwidth len-3)))
                    (let ((mid-spaces (- mid-pos len-1))
                          (last-spaces
                           (+ (- last-pos nmiddle)
                              (quotient len-2 2))))
                      (begin
                        (cond
                         ;;;### display three labels
                         ((< total-len nwidth)
                          (begin
                            (let ((space-1
                                   (make-string
                                    mid-spaces #\space))
                                  (space-2
                                   (make-string
                                    last-spaces #\space)))
                              (let ((this-row
                                     (string-append
                                      left-margin label-1 space-1
                                      label-2 space-2 label-3)))
                                (begin
                                  this-row
                                  )))
                            ))
                         ((< (+ len-1 len-3) nwidth)
                          (begin
                            (let ((space-1
                                   (make-string
                                    (- last-pos len-1) #\space)))
                              (let ((this-row
                                     (string-append
                                      left-margin label-1
                                      space-1 label-3)))
                                (begin
                                  this-row
                                  )))
                            ))
                         (else
                          (begin
                            (let ((this-row
                                   (string-append
                                    left-margin label-1)))
                              (begin
                                this-row
                                ))
                            )))
                        ))
                    ))
                ))
            ))
        (begin
          ""
          ))
    ))

;;;#############################################################
;;;#############################################################
;;; main-data-list a list of lists
;;; each element of main-data-list is (list min max count)
(define (vertical-histogram-string-list
         main-data-list
         nwidth nheight
         data-scale char)
  (begin
    (let ((out-string-list (list))
          (left-margin (make-string 10 #\space))
          (min-count 0)
          (max-count 0)
          (ncount (length main-data-list)))
      (begin
        (if (not (equal? ncount nwidth))
            (begin
              (display
               (format
                #f
                "histogram-module:"))
              (display
               (format
                #f
                "vertical-histogram-string-list "))
              (display
               (format
                #f
                "warning: number of data bins=~a "
                ncount))
              (display
               (format
                #f
                "not equal to width=~a~%"
                nwidth))
              (force-output)
              ))

        (set-min-max-count-macro
         main-data-list
         min-count max-count)

        (let ((max-row (- nheight 1))
              (mid-row (- (quotient nheight 2) 1))
              (mid-count
               (exact->inexact
                (+ (/ (- max-count min-count) 2)
                   min-count)))
              (dscale
               (exact->inexact (/ nheight max-count)))
              (this-row ""))
          (begin
            (do ((ii-row max-row (- ii-row 1)))
                ((< ii-row 0))
              (begin
                (cond
                 ((= ii-row mid-row)
                  (begin
                    (set!
                     this-row
                     (ice-9-format:format
                      #f "~9@a +"
                      (utils-module:commafy-number
                       mid-count)))
                    ))
                 ((= ii-row max-row)
                  (begin
                    (set! this-row
                          (ice-9-format:format
                           #f "~9@a +"
                           (utils-module:commafy-number
                            max-count)))
                    ))
                 (else
                  (begin
                    (set!
                     this-row
                     (string-append left-margin "|"))
                    )))

                (do ((ii-col 0 (+ ii-col 1)))
                    ((>= ii-col nwidth))
                  (begin
                    (let ((this-bin-list
                           (list-ref
                            main-data-list ii-col)))
                      (let ((this-count
                             (list-ref this-bin-list 2)))
                        (let ((this-height
                               (inexact->exact
                                (truncate
                                 (+ 0.50
                                    (* dscale this-count))))))
                          (begin
                            (if (> this-height ii-row)
                                (begin
                                  (set!
                                   this-row
                                   (string-append
                                    this-row
                                    (make-string 1 char))))
                                (begin
                                  (set!
                                   this-row
                                   (string-append
                                    this-row " "))
                                  ))
                            ))
                        ))
                    ))

                (set!
                 out-string-list
                 (append out-string-list
                         (list this-row)))
                ))

            ;;;### make the bottom axis
            (let ((this-margin
                   (ice-9-format:format
                    #f "~9@a +"
                    (utils-module:commafy-number min-count))))
              (let ((this-row
                     (string-append
                      this-margin
                      (make-string nwidth #\-))))
                (begin
                  (set!
                   out-string-list
                   (append
                    out-string-list
                    (list this-row)))
                  )))

            ;;;### make the bottom axis labels
            (let ((this-row
                   (make-vertical-x-axis-labels
                    main-data-list left-margin nwidth)))
              (begin
                (set!
                 out-string-list
                 (append out-string-list
                         (list this-row)))
                ))
            ))
        out-string-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  display-hash-table-histogram - assume main-data-list
;;;###  is a list of lists
(define (histogram-to-string-list
         main-data-list nwidth nheight
         vertical-orientation char)
  (begin
    (if (and (list? main-data-list)
             (> (length main-data-list) 0))
        (begin
          (let ((data-max 0))
            (begin
              (for-each
               (lambda (bin-list)
                 (begin
                   (let ((this-count
                          (list-ref bin-list 2)))
                     (begin
                       (if (or (= data-max 0)
                               (> this-count data-max))
                           (begin
                             (set! data-max this-count)
                             ))
                       ))
                   )) main-data-list)

              ;;;### first prepare data into lists of lists
              ;;;### horizontal orientation, since it's the easiest
              (if (equal? vertical-orientation #t)
                  (begin
                    (let ((data-scale
                           (exact->inexact
                            (/ nheight data-max))))
                      (let ((result-string-list
                             (vertical-histogram-string-list
                              main-data-list
                              nwidth nheight
                              data-scale char)))
                        (begin
                          result-string-list
                          ))))
                  (begin
                    (let ((data-scale
                           (exact->inexact
                            (/ nwidth data-max))))
                      (let ((result-string-list
                             (horizontal-histogram-string-list
                              main-data-list
                              nwidth nheight
                              data-scale char)))
                        (begin
                          result-string-list
                          )))
                    ))
              )))
        (begin
          (list)
          ))
    ))

;;;#############################################################
;;;#############################################################
;;; main-data-list a list of lists
;;; each element of main-data-list is (list min max count)
(define (add-data-to-main-list-list
         data-point main-data-list)
  (begin
    (if (and (list? main-data-list)
             (> (length main-data-list) 0))
        (begin
          (let ((max-index (- (length main-data-list) 1))
                (fdata (exact->inexact data-point))
                (new-main-data-list (list))
                (ii 0))
            (begin
              (for-each
               (lambda (bin-list)
                 (begin
                   (if (> (length bin-list) 0)
                       (begin
                         (let ((bin-min
                                (list-ref bin-list 0))
                               (bin-max
                                (list-ref bin-list 1))
                               (bin-count
                                (list-ref bin-list 2))
                               (new-bin-list (list)))
                           (begin
                             (cond
                              ((and (= ii 0) (< fdata bin-max))
                               (begin
                                 (set!
                                  bin-count (1+ bin-count))
                                 (set!
                                  new-bin-list
                                  (list bin-min bin-max bin-count))
                                 ))
                              ((and (= ii max-index)
                                    (> fdata bin-min))
                               (begin
                                 (set!
                                  bin-count (1+ bin-count))
                                 (set!
                                  new-bin-list
                                  (list bin-min bin-max bin-count))
                                 ))
                              ((and (>= fdata bin-min)
                                    (< fdata bin-max))
                               (begin
                                 (set!
                                  bin-count (1+ bin-count))
                                 (set!
                                  new-bin-list
                                  (list bin-min bin-max bin-count))
                                 ))
                              (else
                               (begin
                                 (set! new-bin-list bin-list)
                                 )))

                             (set!
                              new-main-data-list
                              (cons new-bin-list
                                    new-main-data-list))
                             (set! ii (1+ ii))
                             ))
                         ))
                   )) main-data-list)
              (reverse new-main-data-list)
              )))
        (begin
          (list)
          ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  display-hash-table-histogram - assume raw-data-htable
;;;###  is pre-binned
(define (data-list-to-string-list
         raw-data-list nwidth nheight
         bin-min bin-max
         vertical-orientation char)
  (begin
    (if (and (list? raw-data-list)
             (> (length raw-data-list) 0))
        (begin
          (let ((main-data-list (list))
                (data-min 0)
                (data-max 0)
                (bin-size 0)
                (ncount (length raw-data-list))
                (nbins nwidth))
            (begin
              (if (equal? vertical-orientation #f)
                  (begin
                    (set! nbins nheight)
                    ))

              (set!
               bin-size
               (exact->inexact
                (/ (- bin-max bin-min) nbins)))

              (do ((ii 0 (+ ii 1)))
                  ((>= ii ncount))
                (begin
                  (let ((this-data
                         (list-ref raw-data-list ii)))
                    (begin
                      (if (or (= data-min 0)
                              (< this-data data-min))
                          (begin
                            (set! data-min this-data)
                            ))
                      (if (or (= data-max 0)
                              (> this-data data-max))
                          (begin
                            (set! data-max this-data)
                            ))
                      ))
                  ))

              ;;;### initialize main data list
              (let ((local-bin-min bin-min))
                (begin
                  (do ((ii 0 (+ ii 1)))
                      ((>= ii nbins))
                    (begin
                      (let ((local-bin-max
                             (exact->inexact
                              (+ local-bin-min bin-size))))
                        (let ((bin-list
                               (list
                                local-bin-min
                                local-bin-max 0)))
                          (begin
                            (set!
                             main-data-list
                             (cons bin-list main-data-list))
                            (set! local-bin-min local-bin-max)
                            )))
                      ))
                  ))

              (set!
               main-data-list
               (reverse main-data-list))

              ;;;### populate binned main data list
              (do ((ii 0 (+ ii 1)))
                  ((>= ii ncount))
                (begin
                  (let ((this-data
                         (list-ref raw-data-list ii)))
                    (let ((new-main-data-list
                           (add-data-to-main-list-list
                            this-data main-data-list)))
                      (begin
                        (set! main-data-list
                              new-main-data-list)
                        )))
                  ))

              (histogram-to-string-list
               main-data-list nwidth nheight
               vertical-orientation char)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-list-histogram
         data-list nwidth nheight
         bin-min bin-max
         vertical-orientation char)
  (begin
    (let ((string-list
           (data-list-to-string-list
            data-list nwidth nheight
            bin-min bin-max
            vertical-orientation char)))
      (begin
        (if (and (list? string-list)
                 (> (length string-list) 0))
            (begin
              (for-each
               (lambda (this-string)
                 (begin
                   (display
                    (format #f "~a~%" this-string))
                   )) string-list)

              (newline)
              (force-output)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
