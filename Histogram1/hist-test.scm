#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  histogram display test                               ###
;;;###                                                       ###
;;;###  last updated August 5, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 17, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "tests"))

(set! %load-compiled-path
      (append
       %load-compiled-path (list "." "objects")))

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### getopt-long used for command-line option arguments processing
(use-modules ((ice-9 getopt-long)
              :renamer (symbol-prefix-proc 'ice-9-getopt:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((histogram-module)
              :renamer (symbol-prefix-proc 'histogram-module:)))

;;;#############################################################
;;;#############################################################
(define (run-test-program)
  (begin
    (let ((test-list
           (list
            (list (list 1 2 3 2 2 4 4 2) 10 10 0 10 #t #\#)
            (list (list 1 2 3 2 2 4 4 2) 10 10 0 10 #f #\#)
            (list (list 1 2 3 2 2 4 4 2 7 8 8) 10 10 0 10 #t #\#)
            (list (list 1 2 3 2 2 4 4 2 7 8 8) 10 10 0 10 #f #\#)
            (list (list 1 2 3 2 2 4 4 2 7 8 8 19 18 18 31 59)
                  15 10 0 30 #t #\#)
            (list (list 5 5 5 5 5 4 4 4 4 6 6 6 6 3 3 3 7 7 7
                        2 2 8 8 1 9) 10 10 0 10 #t #\#)
            )))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((test-list (list-ref alist 0))
                   (test-width (list-ref alist 1))
                   (test-height (list-ref alist 2))
                   (test-min (list-ref alist 3))
                   (test-max (list-ref alist 4))
                   (test-vert-bool (list-ref alist 5))
                   (test-char (list-ref alist 6)))
               (begin
                 (display
                  (format #f "~a~%" (make-string 63 #\=)))
                 (display
                  (format #f "data list = ~a~%" test-list))
                 (display
                  (format #f "width = ~a, height = ~a, "
                          test-width test-height))
                 (display
                  (format #f "min = ~a, max = ~a~%"
                          test-min test-max))
                 (display
                  (format #f "vert bool = ~a, char = ~a~%"
                          test-vert-bool test-char))

                 (histogram-module:display-list-histogram
                  test-list test-width test-height
                  test-min test-max test-vert-bool test-char)
                 (force-output)
                 ))
             )) test-list)
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main code                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help ospec title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (display (format #f "options available~%"))
      (for-each
       (lambda(llist)
         (begin
           (display
            (format #f "  --~a, -~a~%" (car llist) (cadr (cadr llist))))
           )) ospec)
      (force-output)
      ))
  (begin
    (let ((version-flag "2024-08-05"))
      (let ((title-string
             (format #f "Histogram Display Test (version ~a)"
                     version-flag))
            (option-spec
             (list (list 'version '(single-char #\v) '(value #f))
                   (list 'help '(single-char #\h) '(value #f))
                   )))
        (let ((options (ice-9-getopt:getopt-long args option-spec)))
          (begin
            (let ((help-flag
                   (ice-9-getopt:option-ref options 'help #f)))
              (begin
                (if (equal? help-flag #t)
                    (begin
                      (local-display-help option-spec title-string)
                      (quit)
                      ))
                ))
            (let ((version-flag
                   (ice-9-getopt:option-ref options 'version #f)))
              (begin
                (if (equal? version-flag #t)
                    (begin
                      (local-display-version title-string)
                      (quit)
                      ))
                ))

            (timer-module:time-code-macro
             (begin
               (run-test-program)
               (newline)
               ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
