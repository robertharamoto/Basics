;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for pi-module.scm                         ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated June 19, 2024                                ###
;;;###                                                       ###
;;;###  created May 27, 2024                                 ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((pi-module)
              :renamer (symbol-prefix-proc 'pi-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-binary-search-pi-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-binary-search-pi-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 7 14 28 3.141592 2 22)
           (list 64 128 256 3.14159265 3 201)
           (list 106 212 424 3.14159265 4 333)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((denom (list-ref alist 0))
                  (start (list-ref alist 1))
                  (end (list-ref alist 2))
                  (accurate-pi (list-ref alist 3))
                  (max-digits (list-ref alist 4))
                  (shouldbe-numerator (list-ref alist 5)))
              (let ((this-tol (expt 10.0 (* -1.0 max-digits))))
                (let ((result-numerator
                       (pi-module:binary-search-pi
                        denom start end accurate-pi this-tol)))
                  (let ((err-1
                         (format
                          #f "~a : (~a) error : max-digits=~a : "
                          sub-name test-label-index max-digits))
                        (err-2
                         (format
                          #f "shouldbe numerator=~a, result=~a"
                          shouldbe-numerator result-numerator)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-numerator
                               result-numerator)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-first-pi-to-tol-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-find-first-pi-to-tol-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 50 3.141592 2
                 22 7 0.0010)
           (list 2 210 3.14159265 3
                 201 64 0.0010)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((start (list-ref alist 0))
                  (end (list-ref alist 1))
                  (accurate-pi (list-ref alist 2))
                  (max-digits (list-ref alist 3))
                  (shouldbe-numerator (list-ref alist 4))
                  (shouldbe-denominator (list-ref alist 5))
                  (shouldbe-tol (list-ref alist 6)))
              (let ((this-tol (expt 10.0 (* -1.0 max-digits)))
                    (tol-discrepancy (* 1 shouldbe-tol)))
                (let ((result-list
                       (pi-module:find-first-pi-to-tol
                        start end accurate-pi this-tol)))
                  (let ((result-numerator (list-ref result-list 0))
                        (result-denominator (list-ref result-list 1))
                        (result-tol (list-ref result-list 2)))
                    (let ((err-1
                           (format
                            #f "~a : (~a) error : max-digits=~a : "
                            sub-name test-label-index max-digits))
                          (err-2a
                           (format
                            #f "shouldbe numerator=~a, "
                            shouldbe-numerator))
                          (err-2b
                           (format
                            #f "result numerator=~a, "
                            result-numerator))
                          (err-3a
                           (format
                            #f "shouldbe denominator=~a, "
                            shouldbe-denominator))
                          (err-3b
                           (format
                            #f "result denominator=~a, "
                            result-denominator))
                          (err-4
                           (format
                            #f "shouldbe tol=~a, result tol=~a"
                            shouldbe-tol result-tol)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-numerator
                                 result-numerator)
                         sub-name
                         (string-append
                          err-1 err-2a err-2b)
                         result-hash-table)

                        (unittest2:assert?
                         (equal? shouldbe-denominator
                                 result-denominator)
                         sub-name
                         (string-append
                          err-1 err-3a err-3b)
                         result-hash-table)

                        (unittest2:assert?
                         (<= (abs (- shouldbe-tol result-tol))
                             tol-discrepancy)
                         sub-name
                         (string-append
                          err-1 err-4)
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
