;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  pi-module - approximate pi by an rational number     ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated June 19, 2024                                ###
;;;###    changed from nested for loops to a binomial        ###
;;;###    search                                             ###
;;;###                                                       ###
;;;###  created May 27, 2024                                 ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start prime modules
(define-module (pi-module)
  #:export (main-approximate
            find-first-pi-to-tol
            binary-search-pi
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### ice-9 format to automatically commafy using format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###  main-approximate - find approximate rational number
;;;###     to pi
(define (main-approximate start-num end-num correct-pi max-digits)
  (begin
    (let ((tol 1.0))
      (begin
        (do ((dexponent 1 (1+ dexponent)))
            ((> dexponent max-digits))
          (begin
            (let ((new-tol (/ tol 10.0)))
              (let ((result-list
                     (find-first-pi-to-tol
                      start-num end-num correct-pi new-tol)))
                (let ((this-numerator (list-ref result-list 0))
                      (this-denominator (list-ref result-list 1))
                      (this-tol (list-ref result-list 2)))
                  (begin
                    (display-results
                     this-numerator this-denominator
                     this-tol new-tol)
                    (set! tol new-tol)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-results numer denom tol new-tol)
  (begin
    (display
     (ice-9-format:format
      #f "~:d / ~:d  = ~18,15f  :  ~8,4e  :  ~8,4e~%"
      numer denom
      (exact->inexact (/ numer denom))
      tol new-tol))
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
;;; for a given denominator, search for the numerator
(define (binary-search-pi
         denom start-num end-num correct-pi tolerance)
  (begin
    (if (>= end-num start-num)
        (begin
          (let ((mid
                 (inexact->exact
                  (truncate
                   (+ start-num
                      (* 0.50 (- end-num start-num)))))))
            (let ((this-pi (/ mid denom)))
              (let ((this-diff (- this-pi correct-pi)))
                (let ((abs-diff (abs this-diff)))
                  (begin
                    (if (> abs-diff tolerance)
                        (begin
                          (if (= start-num end-num)
                              (begin
                                -1)
                              (begin
                                -1
                                ))
                          (if (> this-diff 0.0)
                              (begin
                                (let ((result
                                       (binary-search-pi
                                        denom start-num
                                        (- mid 1) correct-pi tolerance)
                                       ))
                                  (begin
                                    result
                                    )))
                              (begin
                                (let ((result
                                       (binary-search-pi
                                        denom (+ mid 1)
                                        end-num correct-pi tolerance)
                                       ))
                                  (begin
                                    result
                                    ))
                                )))
                        (begin
                          mid
                          ))
                    ))
                ))
            ))
        (begin
          -1
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-first-pi-to-tol start-num end-num correct-pi tolerance)
  (begin
    (let ((best-numerator 1)
          (best-denominator 1)
          (best-tol 1.0)
          (end-flag #f))
      (begin
        (do ((denom 1 (1+ denom)))
            ((or (> denom end-num)
                 (equal? end-flag #t)))
          (begin
            (let ((numer-start (* 2 denom))
                  (numer-end (* 4 denom)))
              (let ((this-numer
                     (binary-search-pi
                      denom numer-start
                      numer-end correct-pi tolerance)))
                (begin
                  (if (> this-numer 0)
                      (begin
                        (let ((this-pi
                               (exact->inexact
                                (/ this-numer denom))))
                          (let ((this-diff
                                 (abs (- this-pi correct-pi))))
                            (begin
                              (if (> best-tol this-diff)
                                  (begin
                                    (set! best-numerator this-numer)
                                    (set! best-denominator denom)
                                    (set! best-tol this-diff)
                                    ))
                              (if (<= best-tol tolerance)
                                  (begin
                                    (set! end-flag #t)
                                    ))
                              )))
                        ))
                  )))
            ))

        (if (<= best-tol tolerance)
            (begin
              (list best-numerator best-denominator
                    best-tol))
            (begin
              (list -1 -1 -1)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
