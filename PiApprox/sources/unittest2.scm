;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit testing module                                  ###
;;;###                                                       ###
;;;###  last updated September 26, 2024                      ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###
;;;###  Test functions
;;;###    (run-all-tests title-string display-details-flag)
;;;###    (define-tests (name args) exp exp*)
;;;###    (assert? bool-expression test-name
;;;###             error-string result-htable)
;;;###

(define-module (unittest2)
  #:export (find-all-tests
            run-all-tests
            define-tests-macro
            assert?))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  include modules                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ls - find all test subroutines to run
(use-modules ((ice-9 ls)
              :renamer (symbol-prefix-proc 'ice-9-ls:)))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  macro definitions                                    ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define-syntax define-tests-macro
  (syntax-rules ()
    ((define-tests-macro (name args) body)
     (define (name args)
       #((bob-test-flag . #t)
         (documentation . "this is a test function"))
       (begin
         body
         )))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  function definitions                                 ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (assert?
         bool-expression
         test-name
         error-string
         result-test-hash-table)
  (begin
    (if (null? result-test-hash-table)
        (begin
          (set!
           result-test-hash-table
           (make-hash-table 100))
          ))

    (let ((test-htable
           (hash-ref
            result-test-hash-table
            test-name
            (make-hash-table 5))))
      (begin
        (let ((this-success
               (hash-ref test-htable "success" 0))
              (this-failures
               (hash-ref test-htable "failure" 0))
              (this-count
               (hash-ref test-htable "count" 0)))
          (begin
            (if (<= this-count 0)
                (begin
                  (hash-set! test-htable
                             "name" test-name)
                  ))

            (set! this-count (1+ this-count))
            (hash-set!
             test-htable "count" this-count)

            (if (boolean? bool-expression)
                (begin
                  (if bool-expression
                      (begin
                        (set!
                         this-success
                         (1+ this-success))
                        (hash-set!
                         test-htable "success" this-success))
                      (begin
                        (set!
                         this-failures
                         (1+ this-failures))
                        (hash-set!
                         test-htable "failure" this-failures)

                        (display
                         (format
                          #f "~a~%" error-string))
                        (force-output)
                        )))
                (begin
                  (if (bool-expression)
                      (begin
                        (set!
                         this-success
                         (1+ this-success))
                        (hash-set!
                         test-htable "success" this-failures))
                      (begin
                        (set!
                         this-failures (1+ this-failures))
                        (hash-set!
                         test-htable "failure" this-failures)

                        (display
                         (format
                          #f "~a~%" error-string))
                        (force-output)
                        ))
                  ))

            (hash-set!
             result-test-hash-table
             test-name test-htable)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-test-list-macro
  (syntax-rules ()
    ((display-test-list-macro
      tname test-htable)
     (begin
       (if (hash-table? test-htable)
           (begin
             (let ((name
                    (hash-ref test-htable "name" "missing"))
                   (nsuccess
                    (hash-ref test-htable "success" 0))
                   (nfailure
                    (hash-ref test-htable "failure" 0))
                   (ncount
                    (hash-ref test-htable "count" 0)))
               (begin
                 (if (equal? nsuccess ncount)
                     (begin
                       (display
                        (ice-9-format:format
                         #f
                         "~a~30t.......... ok!" name))
                       (display
                        (ice-9-format:format
                         #f " ~:d assertions successful out of ~:d~%"
                         nsuccess ncount)))
                     (begin
                       (display
                        (ice-9-format:format
                         #f
                         "~a~30t.......... failed assertions!"
                         name))
                       (display
                        (ice-9-format:format
                         #f
                         " ~:d asserts failed out of ~:d~%"
                         nfailure ncount))
                       ))
                 )))
           (begin
             (display
              (format
               #f "unittest2 module::display-test-list-macro "))
             (display
              (format
               #f "error :: test name ~s " tname))
             (display
              (format
               #f "not found in hash table~%"))
             (force-output)
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-single-line-comment-string-macro
  (syntax-rules ()
    ((display-single-line-comment-string-macro
      var-comment var-length short-comment)
     (begin
       (let ((var-clen
              (string-length var-comment))
             (local-comment var-comment)
             (max-allowed-len (- var-length 10)))
         (begin
           (if (not (equal? var-clen #f))
               (begin
                 (if (> var-clen max-allowed-len)
                     (begin
                       (set! local-comment
                             (substring
                              var-comment 0 max-allowed-len))
                       (set! var-clen
                             (string-length local-comment))
                       ))

                 (let ((var-space-len
                        (- max-allowed-len var-clen)))
                   (begin
                     (if (> var-space-len 0)
                         (begin
                           (let ((spacer-string
                                  (make-string
                                   var-space-len #\space)))
                             (begin
                               (display
                                (format
                                 #f "~a  ~a~a  ~a~%"
                                 short-comment
                                 local-comment
                                 spacer-string
                                 short-comment))
                               )))
                         (begin
                           (display
                            (format
                             #f "~a  ~a  ~a~%"
                             short-comment local-comment
                             short-comment))
                           ))
                     )))
               (begin
                 (let ((spacer-string
                        (make-string
                         (- var-length 10) #\space)))
                   (begin
                     (display
                      (format
                       #f "~a  ~a  ~a~%"
                       short-comment
                       spacer-string
                       short-comment))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-boxed-5-strings-macro
  (syntax-rules ()
    ((display-boxed-5-strings-macro
      separator-comment-string
      short-separator-string
      title-string
      comment-line-1-string comment-line-2-string
      comment-line-3-string comment-line-4-string
      comment-line-5-string
      comment-length)
     (begin
       (let ((blank-string " "))
         (begin
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))

           (display-single-line-comment-string-macro
            blank-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            title-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            blank-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            comment-line-1-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            blank-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            comment-line-2-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            comment-line-3-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            comment-line-4-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            comment-line-5-string comment-length
            short-separator-string)

           (display-single-line-comment-string-macro
            blank-string comment-length
            short-separator-string)

           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-statistics
         title-string
         unit-version-string
         nn-tests
         nn-success nn-failures
         nn-total-asserts)
  (begin
    (let ((comment-length 64)
          (short-length 3))
      (let ((line-for-totals "---------")
            (pcnt-success
             (exact->inexact
              (* 100.0
                 (/ nn-success nn-total-asserts))))
            (pcnt-failures
             (exact->inexact
              (* 100.0
                 (/ nn-failures nn-total-asserts))))
            (pcnt-total
             (exact->inexact
              (* 100.0
                 (/ nn-success nn-total-asserts))))
            (separator-comment-string
             (make-string comment-length #\#))
            (short-separator-string
             (make-string short-length #\#)))
        (let ((line1
               (ice-9-format:format
                #f "number of tests ~27t~10:d"
                nn-tests))
              (line2
               (ice-9-format:format
                #f "successful asserts ~27t~10:d  ~44t~6,2f%"
                nn-success pcnt-success))
              (line3
               (ice-9-format:format
                #f "failed asserts ~27t~10:d  ~44t~6,2f%"
                nn-failures pcnt-failures))
              (line4
               (ice-9-format:format
                #f "~a~28t~10:a~42t~10:a"
                line-for-totals line-for-totals
                line-for-totals))
              (line5
               (ice-9-format:format
                #f "total asserts ~27t~10:d  ~44t~6,2f%"
                nn-total-asserts pcnt-total)))
          (begin
            (display-boxed-5-strings-macro
             separator-comment-string
             short-separator-string
             title-string
             line1 line2 line3 line4 line5
             comment-length)

            (display
             (format #f "~a~%" unit-version-string))
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-test-results
         title-string test-version-string
         details-flag
         result-test-hash-table)
  (define (local-display-details
           title-string result-test-hash-table)
    (begin
      (let ((test-name-list
             (hash-map->list
              (lambda (key value)
                (begin
                  key
                  ))
              result-test-hash-table)))
        (begin
          (if (> (length test-name-list) 0)
              (begin
                (let ((sorted-list
                       (stable-sort test-name-list string-ci<?)))
                  (begin
                    (display
                     (format
                      #f
                      "~a~%" (make-string 64 #\#)))
                    (display
                     (ice-9-format:format
                      #f "~a - ~:d Individual Tests~%"
                      title-string (length test-name-list)))
                    (for-each
                     (lambda (tname)
                       (begin
                         (let ((test-htable
                                (hash-ref
                                 result-test-hash-table
                                 tname #f)))
                           (begin
                             (display-test-list-macro
                              tname test-htable)
                             ))
                         )) sorted-list)
                    ))
                ))
          ))
      ))
  (define (local-sum-over-tests
           result-test-hash-table)
    (begin
      (let ((total-test-htable (make-hash-table 5)))
        (let ((total-success
               (hash-ref total-test-htable "success" 0))
              (total-failure
               (hash-ref total-test-htable "failure" 0))
              (total-count
               (hash-ref total-test-htable "count" 0)))
          (begin
            (hash-for-each
             (lambda (key value)
               (begin
                 (if (hash-table? value)
                     (begin
                       (let ((nsuccess
                              (hash-ref value "success" 0))
                             (nfailure
                              (hash-ref value "failure" 0))
                             (ncount
                              (hash-ref value "count" 0)))
                         (begin
                           (set!
                            total-success
                            (+ total-success nsuccess))
                           (set!
                            total-failure
                            (+ total-failure nfailure))
                           (set!
                            total-count
                            (+ total-count ncount))
                           ))
                       ))
                 )) result-test-hash-table)

            (hash-set! total-test-htable
                       "name" "totals")
            (hash-set! total-test-htable
                       "success" total-success)
            (hash-set! total-test-htable
                       "failure" total-failure)
            (hash-set! total-test-htable
                       "count" total-count)

            total-test-htable
            )))
      ))
  (define (local-display-summary
           title-string test-version-string
           result-test-hash-table)
    (begin
      (let ((total-test-htable
             (local-sum-over-tests result-test-hash-table)))
        (let ((test-name-list
               (hash-map->list
                (lambda (key value) key)
                result-test-hash-table)))
          (let ((num-tests (length test-name-list))
                (unit-version-string
                 (format #f "(unit test version ~A)"
                         test-version-string)))
            (let ((nsuccess
                   (hash-ref total-test-htable "success" 0))
                  (nfailure
                   (hash-ref total-test-htable "failure" 0))
                  (ncount
                   (hash-ref total-test-htable "count" 0)))
              (begin
                (if (> ncount 0)
                    (begin
                      (display-statistics
                       title-string
                       unit-version-string
                       num-tests
                       nsuccess nfailure ncount)
                      ))
                ))
            )))
      ))
  (begin
    (if (equal? details-flag #t)
        (begin
          (local-display-details
           title-string result-test-hash-table)
          ))

    (local-display-summary
     title-string test-version-string
     result-test-hash-table)
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
;;;###  find-all-tests - returns a list of "test-*" methods
(define (find-all-tests)
  (define (local-make-test-list input-list result-list)
    (begin
      (if (or
           (null? input-list)
           (not (list? input-list))
           (< (length input-list) 1))
          (begin
            result-list)
          (begin
            (let ((this-func (car input-list))
                  (tail-list (cdr input-list)))
              (let ((this-proc
                     (catch
                      #t
                      (lambda ()
                        (begin (eval this-func (current-module))))
                      (lambda (key . parameters)
                        (begin "error no function")))))
                (begin
                  (if (procedure? this-proc)
                      (begin
                        (let ((prop-list
                               (procedure-properties this-proc)))
                          (begin
                            (if (and
                                 (not (null? prop-list))
                                 (list? prop-list)
                                 (> (length prop-list) 0))
                                (begin
                                  (let ((tlist
                                         (filter
                                          (lambda(x)
                                            (equal? (car x) 'bob-test-flag))
                                          prop-list)))
                                    (begin
                                      (if (and
                                           (not (null? tlist))
                                           (list? tlist))
                                          (begin
                                            (local-make-test-list
                                             tail-list
                                             (append (list this-func) result-list)))
                                          (begin
                                            (local-make-test-list
                                             tail-list result-list)
                                            ))
                                      )))
                                (begin
                                  (local-make-test-list
                                   tail-list result-list)
                                  ))
                            )))
                      (begin
                        (local-make-test-list
                         tail-list result-list)
                        ))
                  )))
            ))
      ))
  (begin
    (let ((lfunc-list (ice-9-ls:lls)))
      (begin
        (let ((test-func-list
               (local-make-test-list
                lfunc-list (list))))
          (begin
            test-func-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###  run-all-tests
(define-public (run-all-tests title-string details-flag)
  (define (local-run-all-tests
           input-list result-test-hash-table)
    (begin
      (if (and (list? input-list)
               (> (length input-list) 0))
          (begin
            (let ((this-func (car input-list))
                  (tail-list (cdr input-list)))
              (let ((this-proc
                     (eval this-func (current-module))))
                (begin
                  (if (symbol? this-func)
                      (begin
                        (this-proc result-test-hash-table)
                        ))
                  (local-run-all-tests
                   tail-list result-test-hash-table))
                ))
            ))
      ))
  (begin
    (let ((result-test-hash-table (make-hash-table 100))
          (test-func-list (find-all-tests))
          (test-version-string "2024-09-26"))
      (begin
        (if (and (list? test-func-list)
                 (> (length test-func-list) 0))
            (begin
              (let ((stest-list
                     (sort
                      test-func-list
                      (lambda (a b)
                        (begin
                          (string-ci<?
                           (symbol->string a)
                           (symbol->string b))
                          )))
                     ))
                (begin
                  (local-run-all-tests
                   stest-list result-test-hash-table)
                  ))

              (display-test-results
               title-string
               test-version-string
               details-flag
               result-test-hash-table)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
