;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  prime benchmarks functions                           ###
;;;###                                                       ###
;;;###  last updated August 5, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################


;;;#############################################################
;;;#############################################################
;;;### start sprp prime modules

(define-module (benchmarks-module)
  #:export (sprp-loop
            sieve-loop
            trial-divisor-loop
            ))

;;;#############################################################
;;;#############################################################
;;;###  include modules
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

(use-modules ((sprp-module)
              :renamer (symbol-prefix-proc 'sprp-module:)))

;;;#############################################################
;;;#############################################################
;;;### begin code
(define (sprp-loop end-num)
  (begin
    (let ((prime-count 1)
          (start-num 3))
      (begin
        (do ((ii start-num (+ ii 2)))
            ((> ii end-num))
          (begin
            (if (sprp-module:sprp-prime? ii)
                (begin
                  (set! prime-count (1+ prime-count))
                  ))
            ))
        prime-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sieve-loop end-num max-primes)
  (begin
    (let ((prime-count 1)
          (start-num 3)
          (primes-array
           (prime-module:make-prime-array max-primes)))
      (begin
        (do ((ii start-num (+ ii 2)))
            ((> ii end-num))
          (begin
            (if (prime-module:is-array-prime? ii primes-array)
                (begin
                  (set! prime-count (1+ prime-count))
                  ))
            ))
        prime-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (trial-divisor-loop end-num)
  (begin
    (let ((prime-count 1)
          (start-num 3))
      (begin
        (do ((ii start-num (+ ii 2)))
            ((> ii end-num))
          (begin
            (if (prime-module:is-prime? ii)
                (begin
                  (set! prime-count (1+ prime-count))
                  ))
            ))
        prime-count
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
