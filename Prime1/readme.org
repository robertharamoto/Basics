################################################################
################################################################
###                                                          ###
###  Prime testing comparisons                               ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################

This program counts the number of primes using three different methods.

The fastest method uses the strong probable prime method, also called
the Miller-Rabin primality test.
See: https://t5k.org/prove/prove2_3.html

The next fastest method uses the sieve of Eratosthenes to generate
an array of primes, and uses a binary search of the array if the
number is less than 1 million, and does trial division if the
number is greater than 1 million.  The speed-up over normal trial
division comes from the use of primes less than the square root of
the number.  You only have to examine about 10-20% of the odd
numbers less than the square root of n. The trade-off is more
memory usage, and the more primes stored means faster execution.
See: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes

The final method uses straight trial division, and simply
tests each odd number less than the square root of n.

################################################################
################################################################
Running the program:

(1)  ./gen-make.scm
     gather files in the sources and tests subdirectories and create
     the make file

(2)  make
     build the object files (in objects/), and run the run-tests.scm
     unit tests program

(3)  ./prime-comp.scm > out.log
     run the program, takes a little more than 35 minutes to count all
     primes up to 100 million (using the 3 methods).

(4)  to change the primes count end-num or max-primes (maximum number
     to examine when building the array of primes), edit the scheme
     script prime-comp.scm, lines 94 and 95.  It takes about 10 minutes
     to do the trial division method to find primes less than 100 million,
     and you can expect it to take about 10 times longer to count the
     number of primes less than 1 billion.

|------------------------+-------------+-------------+----------|
| Method                 | 2022        | 2020        | speed-up |
|------------------------+-------------+-------------+----------|
| strongly probable      | 87 seconds  | 189 seconds | x2.2     |
| seive of erathosthenes | 226 seconds | 446 seconds | x2.0     |
| trial division         | 343 seconds | 574 seconds | x1.7     |
|------------------------+-------------+-------------+----------|

################################################################
################################################################
For more:

Prime numbers:
https://oeis.org/wiki/Prime_numbers
see also: https://en.wikipedia.org/wiki/Prime_number

Sequence at:
https://oeis.org/A000040

Formulas for prime numbers
https://oeis.org/wiki/Formulas_for_primes


################################################################
################################################################

Assumes that guile 3.0 is installed in /usr/bin.

Uses the unlicense public domain license.
See the UNLICENSE file, or https://unlicense.org/


################################################################
################################################################
History

last updated <2025-02-17 Mon>
updated <2024-07-24 Wed>
updated <2022-05-19 Thu>
updated <2020-02-10 Mon>


################################################################
################################################################

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################
