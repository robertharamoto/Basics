#! /bin/bash

PROG=mps.scm
LOGFILE=out0.log
LINESTRING="################################################################"

this_process=`ps -e | grep -i "${PROG}"`
while [ -n "${this_process}" ]
do
    echo "${LINESTRING}"
    echo "${LINESTRING}"
    tail "${LOGFILE}"
    echo "${LINESTRING}"

    ls -lrth "${LOGFILE}"
    ps -ef | head -n 1
    ps -ef | grep -i "${PROG}" | grep -vi "grep"
    date
    sleep 120
    this_process=`ps -e | grep -i "${PROG}"`
done
