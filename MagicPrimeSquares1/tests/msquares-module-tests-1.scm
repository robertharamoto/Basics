;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for msquares-module.scm                   ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((msquares-module)
              :renamer (symbol-prefix-proc 'msquares-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-row-sum-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-row-sum-1"
                  (utils-module:get-basename
                   (current-filename))))
         (test-list
          (list
           (list 0 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 1 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 2 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 0 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 1 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 2 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 3 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 0 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 1 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 2 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 3 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 4 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((ii-row (list-ref alist 0))
                  (ii-size (list-ref alist 1))
                  (marray (list-ref alist 2))
                  (shouldbe (list-ref alist 3)))
              (let ((result
                     (msquares-module:row-sum
                      ii-row ii-size marray)))
                (let ((err-1
                       (format #f "~a : error (~a) : "
                               sub-name test-label-index))
                      (err-2
                       (format #f "row=~a, size=~a, array=~a : "
                               ii-row ii-size marray))
                      (err-3
                       (format #f "shouldbe=~a, result=~a"
                               shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-col-sum-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-col-sum-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 1 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 2 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 0 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 1 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 2 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 3 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 0 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 1 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 2 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 3 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           (list 4 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((jj-col (list-ref alist 0))
                  (ii-size (list-ref alist 1))
                  (marray (list-ref alist 2))
                  (shouldbe (list-ref alist 3)))
              (let ((result
                     (msquares-module:col-sum
                      jj-col ii-size marray)))
                (let ((err-1
                       (format #f "~a : error (~a) : "
                               sub-name test-label-index))
                      (err-2
                       (format #f "col=~a, size=~a, array=~a : "
                               jj-col ii-size marray))
                      (err-3
                       (format #f "shouldbe=~a, result=~a"
                               shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-diag-sum-right-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-diag-sum-right-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((ii-size (list-ref alist 0))
                  (marray (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (msquares-module:diag-sum-right ii-size marray)))
                (let ((err-1
                       (format #f "~a : error (~a) : "
                               sub-name test-label-index))
                      (err-2
                       (format #f "size=~a, array=~a : "
                               ii-size marray))
                      (err-3
                       (format #f "shouldbe=~a, result=~a"
                               shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-diag-sum-left-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-diag-sum-left-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 15)
           (list 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 34)
           (list 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 65)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((ii-size (list-ref alist 0))
                  (marray (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (msquares-module:diag-sum-left
                      ii-size marray)))
                (let ((err-1
                       (format #f "~a : error (~a) : "
                               sub-name test-label-index))
                      (err-2
                       (format #f "size=~a, array=~a : "
                               ii-size marray))
                      (err-3
                       (format #f "shouldbe=~a, result=~a"
                               shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-row-sum-equal-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-is-row-sum-equal-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 15 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 #t)
           (list 1 15 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 #t)
           (list 2 15 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 #t)
           (list 0 111 3
                 (list->array
                  2 (list (list 67 1 43)
                          (list 13 37 61)
                          (list 31 73 7)))
                 #t)
           (list 1 111 3
                 (list->array
                  2 (list (list 67 1 43)
                          (list 13 37 61)
                          (list 31 73 7)))
                 #t)
           (list 2 111 3
                 (list->array
                  2 (list (list 67 1 43)
                          (list 13 37 61)
                          (list 31 73 7)))
                 #t)
           (list 0 177 3
                 (list->array
                  2 (list (list 17 89 71)
                          (list 113 59 5)
                          (list 47 29 101)))
                 #t)
           (list 1 177 3
                 (list->array
                  2 (list (list 17 89 71)
                          (list 113 59 5)
                          (list 47 29 101)))
                 #t)
           (list 2 177 3
                 (list->array
                  2 (list (list 17 89 71)
                          (list 113 59 5)
                          (list 47 29 101)))
                 #t)
           (list 0 3117 3
                 (list->array
                  2 (list (list 1669 199 1249)
                          (list 619 1039 1459)
                          (list 829 1879 409)))
                 #t)
           (list 1 3117 3
                 (list->array
                  2 (list (list 1669 199 1249)
                          (list 619 1039 1459)
                          (list 829 1879 409)))
                 #t)
           (list 2 3117 3
                 (list->array
                  2 (list (list 1669 199 1249)
                          (list 619 1039 1459)
                          (list 829 1879 409)))
                 #t)
           (list 0 34 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 #t)
           (list 1 34 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 #t)
           (list 2 34 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 #t)
           (list 3 34 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 #t)
           (list 0 120 4
                 (list->array
                  2 (list (list 3 61 19 37)
                          (list 43 31 5 41)
                          (list 7 11 73 29)
                          (list 67 17 23 13)))
                 #t)
           (list 1 120 4
                 (list->array
                  2 (list (list 3 61 19 37)
                          (list 43 31 5 41)
                          (list 7 11 73 29)
                          (list 67 17 23 13)))
                 #t)
           (list 2 120 4
                 (list->array
                  2 (list (list 3 61 19 37)
                          (list 43 31 5 41)
                          (list 7 11 73 29)
                          (list 67 17 23 13)))
                 #t)
           (list 3 120 4
                 (list->array
                  2 (list (list 3 61 19 37)
                          (list 43 31 5 41)
                          (list 7 11 73 29)
                          (list 67 17 23 13)))
                 #t)
           (list 0 65 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 #t)
           (list 1 65 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 #t)
           (list 2 65 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 #t)
           (list 3 65 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 #t)
           (list 4 65 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((ii-row (list-ref alist 0))
                  (init-sum (list-ref alist 1))
                  (ii-size (list-ref alist 2))
                  (marray (list-ref alist 3))
                  (shouldbe (list-ref alist 4)))
              (let ((result
                     (msquares-module:is-row-sum-equal?
                      ii-row init-sum ii-size marray)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "row=~a, sum=~a, size=~a, "
                        ii-row init-sum ii-size))
                      (err-3
                       (format #f ", array=~a : " marray))
                      (err-4
                       (format #f "shouldbe=~a, result=~a"
                               (if shouldbe "true" "false")
                               (if result "true" "false"))))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3 err-4)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-magic-square-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-is-magic-square-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 1)
                          (list 4 3 8)))
                 #t)
           (list 3
                 (list->array
                  2 (list (list 2 7 6)
                          (list 9 5 11)
                          (list 4 3 8)))
                 #f)
           (list 3
                 (list->array
                  2 (list (list 67 1 43)
                          (list 13 37 61)
                          (list 31 73 7)))
                 #t)
           (list 3
                 (list->array
                  2 (list (list 17 89 71)
                          (list 113 59 5)
                          (list 47 29 101)))
                 #t)
           (list 3
                 (list->array
                  2 (list (list 1669 199 1249)
                          (list 619 1039 1459)
                          (list 829 1879 409)))
                 #t)
           (list 4
                 (list->array
                  2 (list (list 7 12 1 14)
                          (list 2 13 8 11)
                          (list 16 3 10 5)
                          (list 9 6 15 4)))
                 #t)
           (list 4
                 (list->array
                  2 (list (list 3 61 19 37)
                          (list 43 31 5 41)
                          (list 7 11 73 29)
                          (list 67 17 23 13)))
                 #t)
           (list 5
                 (list->array
                  2 (list (list 11 24 7 20 3)
                          (list 4 12 25 8 16)
                          (list 17 5 13 21 9)
                          (list 10 18 1 14 22)
                          (list 23 6 19 2 15)))
                 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((ii-size (list-ref alist 0))
                  (marray (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (msquares-module:is-magic-square?
                      ii-size marray)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : size=~a, "
                        sub-name test-label-index ii-size))
                      (err-2
                       (format #f "array=~a : " marray))
                      (err-3
                       (format #f "shouldbe=~a, result=~a"
                               (if shouldbe "true" "false")
                               (if result "true" "false"))))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-prime-list-magic-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-is-prime-list-magic-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 5 17 29 47 59 71 89 101 113)
                 0 177 0 0 #f #f 3 (make-array 0 3 3)
                 #t)
           (list (list 29 41 53 59 71 83 89 101 113)
                 0 213 0 0 #f #f 3 (make-array 0 3 3)
                 #t)
           (list (list 31 41 53 59 71 83 89 101 113)
                 0 213 0 0 #f #f 3 (make-array 0 3 3)
                 #f)
           (list (list 3 5 7 11 13 17 19 23 29
                       31 37 41 43 61 67 73)
                 0 120 0 0 #f #f 4 (make-array 0 4 4)
                 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((prime-acc-list (list-ref alist 0))
                  (curr-row-sum (list-ref alist 1))
                  (target-row-sum (list-ref alist 2))
                  (curr-row (list-ref alist 3))
                  (curr-col (list-ref alist 4))
                  (find-all-bool (list-ref alist 5))
                  (display-flag (list-ref alist 6))
                  (nn (list-ref alist 7))
                  (marray (list-ref alist 8))
                  (shouldbe (list-ref alist 9)))
              (let ((result
                     (msquares-module:is-prime-list-magic?
                      prime-acc-list curr-row-sum target-row-sum
                      curr-row curr-col find-all-bool display-flag
                      nn marray)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : prime-list=~a, "
                        sub-name test-label-index
                        prime-acc-list))
                      (err-2
                       (format #f "array=~a : " marray))
                      (err-3
                       (format #f "shouldbe=~a, result=~a"
                               (if shouldbe "true" "false")
                               (if result "true" "false"))))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-choose-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-choose-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 0 1) (list 2 1 2)
           (list 2 2 1) (list 3 0 1)
           (list 3 1 3) (list 3 2 3)
           (list 3 3 1) (list 4 0 1)
           (list 4 1 4) (list 4 2 6)
           (list 4 3 4) (list 4 4 1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (kk (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result1
                     (msquares-module:choose nn kk))
                    (result2
                     (msquares-module:choose kk nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, kk=~a, "
                        sub-name test-label-index nn kk))
                      (err-2
                       (format #f "shouldbe=~a, result1=~a, "
                               shouldbe result1))
                      (err-3
                       (format #f "shouldbe=~a, result2=~a, "
                               shouldbe result2)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result1)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)

                    (unittest2:assert?
                     (equal? shouldbe result2)
                     sub-name
                     (string-append err-1 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; test helper functions

;;;#############################################################
;;;#############################################################
(define (assert-lists-equal
         shouldbe-list result-list
         sub-name error-message result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((slen (min shouldbe-length result-length))
            (err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
                     error-message shouldbe-list result-list))
            (err-2
             (format
              #f "shouldbe size=~a, result size=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append
            err-1 err-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((found-flag (memq s-elem result-list))
                      (err-3
                       (format
                        #f "missing element=~a" s-elem)))
                  (begin
                    (unittest2:assert?
                     (not (equal? found-flag #f))
                     sub-name
                     (string-append
                      err-1 err-3)
                     result-hash-table)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-arrays-equal
         shouldbe-array result-array
         sub-name error-message result-hash-table)
  (begin
    (let ((shouldbe-dims (array-dimensions shouldbe-array))
          (result-dims (array-dimensions result-array)))
      (let ((max-rows (list-ref shouldbe-dims 0))
            (max-cols (list-ref shouldbe-dims 1))
            (err-1
             (format
              #f "~a : shouldbe-array=~a, result-array=~a : "
              error-message shouldbe-array result-array))
            (err-2
             (format
              #f "shouldbe size=~a, result size=~a"
              shouldbe-dims result-dims)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-dims result-dims)
           sub-name
           (string-append
            err-1 err-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii max-rows))
            (begin
              (do ((jj 0 (1+ jj)))
                  ((>= jj max-cols))
                (begin
                  (let ((s-elem (array-ref shouldbe-array ii jj))
                        (r-elem (array-ref result-array ii jj)))
                    (let ((err-3
                           (format
                            #f "shouldbe element(~a, ~a) = ~a, "
                            ii jj s-elem))
                          (err-4
                           (format
                            #f "result element = ~a" r-elem)))
                      (begin
                        (unittest2:assert?
                         (equal? s-elem r-elem)
                         sub-name
                         (string-append
                          err-1 err-3 err-4)
                         result-hash-table)
                        )))
                  ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
