;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  magic squares module                                 ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start msquares modules
(define-module (msquares-module)
  #:export (row-sum
            col-sum
            diag-sum-right
            diag-sum-left
            is-row-sum-equal?
            is-magic-square?
            is-prime-list-magic?

            choose

            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### prime-module for make-prime-array functions
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for date functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;#############################################################
;;;#############################################################
(define (row-sum ii-row nn marray)
  (begin
    (let ((this-sum 0))
      (begin
        (do ((jj 0 (1+ jj)))
            ((>= jj nn))
          (begin
            (let ((this-elem
                   (array-ref marray ii-row jj)))
              (begin
                (set!
                 this-sum (+ this-sum this-elem))
                ))
            ))

        this-sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (col-sum jj-col nn marray)
  (begin
    (let ((this-sum 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nn))
          (begin
            (let ((this-elem
                   (array-ref marray ii jj-col)))
              (begin
                (set!
                 this-sum (+ this-sum this-elem))
                ))
            ))

        this-sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (diag-sum-right nn marray)
  (begin
    (let ((this-sum 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nn))
          (begin
            (let ((this-elem
                   (array-ref marray ii ii)))
              (begin
                (set!
                 this-sum (+ this-sum this-elem))
                ))
            ))

        this-sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (diag-sum-left nn marray)
  (begin
    (let ((this-sum 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii nn))
          (begin
            (let ((jj (- nn (1+ ii))))
              (let ((this-elem
                     (array-ref marray ii jj)))
                (begin
                  (set!
                   this-sum (+ this-sum this-elem))
                  )))
            ))

        this-sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-row-sum-equal? ii-row init-sum nn marray)
  (begin
    (let ((this-row-sum
           (row-sum ii-row nn marray)))
      (begin
        (= init-sum this-row-sum)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-magic-square? nn marray)
  (begin
    (let ((row-sum-0 (row-sum 0 nn marray))
          (magic-flag #t))
      (begin
        (do ((ii 1 (1+ ii)))
            ((or (>= ii nn)
                 (equal? magic-flag #f)))
          (begin
            (let ((this-sum (row-sum ii nn marray)))
              (begin
                (if (not (equal? this-sum row-sum-0))
                    (begin
                      (set! magic-flag #f)
                      ))
                ))
            ))

        (if (equal? magic-flag #t)
            (begin
              (do ((jj 0 (1+ jj)))
                  ((or (>= jj nn)
                       (equal? magic-flag #f)))
                (begin
                  (let ((this-sum
                         (col-sum jj nn marray)))
                    (begin
                      (if (not (equal? this-sum row-sum-0))
                          (begin
                            (set! magic-flag #f)
                            ))
                      ))
                  ))
              ))

        (if (equal? magic-flag #t)
            (begin
              (let ((this-sum (diag-sum-right nn marray)))
                (begin
                  (if (not (equal? this-sum row-sum-0))
                      (begin
                        (set! magic-flag #f)
                        ))
                  ))
              ))

        (if (equal? magic-flag #t)
            (begin
              (let ((this-sum (diag-sum-left nn marray)))
                (begin
                  (if (not (equal? this-sum row-sum-0))
                      (begin
                        (set! magic-flag #f)
                        ))
                  ))
              ))

        magic-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-array nn result-array)
  (begin
    (let ((rsum (row-sum 0 nn result-array)))
      (begin
        (display
         (ice-9-format:format
          #f "  ~:d x ~:d : sum = ~a~%" nn nn rsum))
        (force-output)

        (do ((ii 0 (1+ ii)))
            ((>= ii nn))
          (begin
            (do ((jj 0 (1+ jj)))
                ((>= jj nn))
              (begin
                (let ((this-elem
                       (array-ref result-array ii jj)))
                  (begin
                    (if (equal? jj 0)
                        (begin
                          (display
                           (ice-9-format:format
                            #f "~10:d" this-elem)))
                        (begin
                          (display
                           (ice-9-format:format
                            #f " ~10:d" this-elem))
                          ))
                    ))
                ))
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-status-macro
  (syntax-rules ()
    ((display-status-macro
      curr-status max-status
      ssum nn
      status-minutes start-jday)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (let ((nminutes
                (timer-module:julian-day-difference-in-minutes
                 end-jday start-jday)))
           (begin
             (if (>= nminutes status-minutes)
                 (begin
                   (display
                    (ice-9-format:format
                     #f "(~dx~:d) completed ~:d iterations "
                     nn nn curr-status))
                   (display
                    (ice-9-format:format
                     #f "out of ~:d : " max-status))
                   (display
                    (ice-9-format:format
                     #f "current sum of primes (~ax~a) = ~:d~%"
                     nn nn ssum))
                   (display
                    (format
                     #f "  ~a : ~a~%"
                     (timer-module:julian-day-difference-to-string
                      end-jday start-jday)
                     (timer-module:current-date-time-string)))
                   (force-output)
                   (set! start-jday end-jday)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax row-subtract-to-find-next-prime-macro
  (syntax-rules ()
    ((row-subtract-to-find-next-prime-macro
      curr-row curr-col
      curr-row-sum target-row-sum
      end-flag acc-list
      find-all-bool display-flag
      nn result-array)
     (begin
       ;;; compute the difference
       (let ((pdiff (- target-row-sum curr-row-sum)))
         (begin
           (if (equal? (member pdiff acc-list) #f)
               (begin
                 (set! end-flag #t))
               (begin
                 ;;; found pdiff in list, so put it in the last column
                 (let ((next-list
                        (filter
                         (lambda (pp)
                           (begin
                             (not (= pp pdiff))
                             )) acc-list))
                       (next-next-row (1+ curr-row))
                       (next-next-col 0)
                       (next-curr-row-sum 0)
                       (return-value #f))
                   (begin
                     (array-set!
                      result-array pdiff
                      curr-row curr-col)

                     (let ((rvalue
                            (is-prime-list-magic?
                             next-list next-curr-row-sum
                             target-row-sum
                             next-next-row next-next-col
                             find-all-bool display-flag
                             nn result-array)))
                       (begin
                         (set! return-value rvalue)
                         ))

                     (array-set!
                      result-array 0
                      curr-row curr-col)

                     return-value
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax col-subtract-to-find-next-prime-macro
  (syntax-rules ()
    ((col-subtract-to-find-next-prime-macro
      curr-row curr-col
      curr-row-sum target-row-sum
      end-flag acc-list
      find-all-bool display-flag
      nn result-array)
     (begin
       (let ((curr-col-sum 0)
             (last-row (1- nn)))
         (begin
           ;;; find the current column sum so far
           (do ((ii 0 (1+ ii)))
               ((>= ii last-row))
             (begin
               (let ((this-elem
                      (array-ref result-array ii curr-col)))
                 (begin
                   (set! curr-col-sum (+ curr-col-sum this-elem))
                   ))
               ))

           ;;; compute the difference
           (let ((pdiff (- target-row-sum curr-col-sum)))
             (begin
               (if (equal? (member pdiff acc-list) #f)
                   (begin
                     (set! end-flag #t))
                   (begin
                     ;;; found pdiff in list, so put it in the last column
                     (let ((next-list
                            (filter
                             (lambda (pp)
                               (begin
                                 (not (= pp pdiff))
                                 )) acc-list))
                           (next-next-row curr-row)
                           (next-next-col (1+ curr-col))
                           (next-curr-row-sum (+ curr-row-sum pdiff))
                           (return-value #f))
                       (begin
                         (array-set!
                          result-array pdiff
                          curr-row curr-col)

                         (if (> next-next-col last-row)
                             (begin
                               (set! next-next-row (1+ curr-row))
                               (set! next-next-col 0)
                               ))

                         (let ((rvalue
                                (is-prime-list-magic?
                                 next-list next-curr-row-sum
                                 target-row-sum
                                 next-next-row next-next-col
                                 find-all-bool display-flag
                                 nn result-array)))
                           (begin
                             (set! return-value rvalue)
                             ))

                         (array-set!
                          result-array 0
                          curr-row curr-col)

                         return-value
                         ))
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax substitute-to-find-next-prime-macro
  (syntax-rules ()
    ((substitute-to-find-next-prime-macro
      curr-row curr-col
      next-row next-col
      curr-row-sum target-row-sum
      end-flag alen acc-list
      find-all-bool display-flag
      nn result-array)
     (begin
       (let ((found-one-return-value #f))
         (begin
           (do ((kk 0 (1+ kk)))
               ((or (>= kk alen)
                    (equal? end-flag #t)))
             (begin
               (let ((this-prime (list-ref acc-list kk)))
                 (let ((next-list
                        (filter
                         (lambda (p)
                           (begin
                             (not (= p this-prime))
                             )) acc-list))
                       (next-curr-row-sum
                        (+ curr-row-sum this-prime)))
                   (begin
                     (array-set!
                      result-array this-prime
                      curr-row curr-col)

                     (let ((rvalue
                            (is-prime-list-magic?
                             next-list next-curr-row-sum
                             target-row-sum
                             next-row next-col find-all-bool
                             display-flag
                             nn result-array)))
                       (begin
                         (if (equal? rvalue #t)
                             (begin
                               (set! found-one-return-value #t)
                               ))
                         ))

                     (array-set!
                      result-array 0
                      curr-row curr-col)

                     (if (and
                          (equal? find-all-bool #f)
                          (equal? found-one-return-value #t))
                         (begin
                           (set! end-flag #t)
                           ))
                     )))
               ))

           found-one-return-value
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-prime-list-magic?
         prime-acc-list curr-row-sum target-row-sum
         curr-row curr-col find-all-bool display-flag
         nn result-array)
  (begin
    (if (or (not (list? prime-acc-list)) (null? prime-acc-list)
            (<= (length prime-acc-list) 0)
            (>= curr-row nn) (>= curr-col nn))
        (begin
          (if (is-magic-square? nn result-array)
              (begin
                (if (equal? display-flag #t)
                    (begin
                      (display-array nn result-array)
                      ))
                #t)
              (begin
                #f
                )))
        (begin
          (let ((next-row curr-row)
                (last-row (1- nn))
                (last-col (1- nn))
                (next-col (1+ curr-col))
                (next-row-sum curr-row-sum)
                (alen (length prime-acc-list))
                (end-flag #f))
            (begin
              (if (>= next-col nn)
                  (begin
                    (set! next-row (1+ next-row))
                    (set! next-col 0)
                    (set! next-row-sum 0)
                    ))

              (if (= curr-col 0)
                  (begin
                    (set! curr-row-sum 0)
                    ))

              (if (> curr-row-sum target-row-sum)
                  (begin
                    (set! end-flag #t)
                    ))

              (cond
               ((and (equal? end-flag #f)
                     (= curr-col last-col))
                (begin
                  (row-subtract-to-find-next-prime-macro
                   curr-row curr-col
                   curr-row-sum target-row-sum
                   end-flag prime-acc-list
                   find-all-bool display-flag
                   nn result-array)
                  ))
               ((and (equal? end-flag #f)
                     (= curr-row last-row))
                (begin
                  (col-subtract-to-find-next-prime-macro
                   curr-row curr-col
                   curr-row-sum target-row-sum
                   end-flag prime-acc-list
                   find-all-bool display-flag
                   nn result-array)
                  ))
               (else
                (begin
                  (substitute-to-find-next-prime-macro
                   curr-row curr-col
                   next-row next-col
                   curr-row-sum target-row-sum
                   end-flag alen prime-acc-list
                   find-all-bool display-flag
                   nn result-array)
                  )))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (choose nn kk)
  (begin
    (cond
     ((or (= kk 0)
          (= kk nn)
          (= nn 0))
      (begin
        1
        ))
     ((> nn kk)
      (begin
        (let ((result 1))
          (begin
            (do ((ii 1 (1+ ii)))
                ((> ii kk))
              (begin
                (let ((numer (- (+ nn 1) ii))
                      (denom ii))
                  (let ((this-term (/ numer denom)))
                    (begin
                      (set! result (* result this-term))
                      )))
                ))
            result
            ))
        ))
     (else
      (begin
        (let ((result 1))
          (begin
            (do ((ii 1 (1+ ii)))
                ((> ii nn))
              (begin
                (let ((numer (- (+ kk 1) ii))
                      (denom ii))
                  (let ((this-term (/ numer denom)))
                    (begin
                      (set! result (* result this-term))
                      )))
                ))

            result
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-build-loop
         nn pp prime-list find-all-bool
         start-jday status-minutes)
  (define (local-construct-prime-lists
           depth nn-2 pp prime-list acc-list find-all-bool
           prev-status max-status
           display-check-num status-minutes start-jday
           nn result-array)
    (begin
      (if (or (>= depth nn-2)
              (not (list? prime-list))
              (null? prime-list)
              (<= pp 0))
          (begin
            (let ((ssum
                   (srfi-1:fold + 0 acc-list)))
              (begin
                (if (zero? (modulo ssum nn))
                    (begin
                      (let ((target-row-sum
                             (euclidean/ ssum nn)))
                        (begin
                          (if (or
                               (and (odd? nn)
                                    (odd? target-row-sum))
                               (and (even? nn)
                                    (even? target-row-sum)))
                              (begin
                                (let ((rvalue
                                       (is-prime-list-magic?
                                        acc-list 0
                                        (euclidean/ ssum nn)
                                        0 0 find-all-bool #t
                                        nn result-array)))
                                  (begin
                                    (if (equal? rvalue #t)
                                        (begin
                                          (display
                                           (format
                                            #f "~a~%"
                                            (make-string 64 #\=)))
                                          (force-output)
                                          ))
                                    ))
                                ))
                          ))
                      ))

                (let ((curr-status (1+ prev-status)))
                  (begin
                    (if (equal? (length acc-list) nn-2)
                        (begin
                          (if (zero?
                               (modulo
                                curr-status
                                display-check-num))
                              (begin
                                (display-status-macro
                                 curr-status max-status
                                 ssum nn
                                 status-minutes start-jday)
                                ))

                          (set! prev-status curr-status)
                          ))

                    (list prev-status start-jday)
                    ))
                )))
          (begin
            (let ((next-depth (1+ depth)))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((>= ii pp))
                  (begin
                    (let ((this-prime
                           (list-ref prime-list ii))
                          (next-prime-list
                           (list-tail prime-list (1+ ii))))
                      (let ((next-pp
                             (length next-prime-list)))
                        (let ((llist
                               (local-construct-prime-lists
                                next-depth nn-2 next-pp
                                next-prime-list
                                (cons this-prime acc-list)
                                find-all-bool
                                prev-status max-status
                                display-check-num
                                status-minutes start-jday
                                nn result-array)))
                          (let ((next-curr-status (list-ref llist 0))
                                (next-start-jday (list-ref llist 1)))
                            (begin
                              (set! prev-status next-curr-status)
                              (set! start-jday next-start-jday)

                              (if (= depth 0)
                                  (begin
                                    (gc)
                                    ))
                              )))
                        ))
                    ))
                ))

            (list prev-status start-jday)
            ))
      ))
  (begin
    (let ((acc-list (list))
          (display-check-num 100)
          (nsquare (* nn nn))
          (result-array (make-array 0 nn nn)))
      (let ((max-status (choose pp nsquare))
            (prev-status 0))
        (let ((rlist
               (local-construct-prime-lists
                0 nsquare pp prime-list
                acc-list find-all-bool
                prev-status max-status
                display-check-num
                status-minutes start-jday
                nn result-array)
               ))
          (begin
            rlist
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop
         start-nn end-nn max-primes
         find-all-bool status-minutes)
  (begin
    (let ((prime-array
           (prime-module:make-prime-array max-primes))
          (start-jday (srfi-19:current-julian-day)))
      (let ((plist (array->list prime-array)))
        (let ((ptrim-list (list-tail plist 1)))
          (let ((plen (length ptrim-list))
                (separator-line (make-string 63 #\=)))
            (begin
              ;;; get rid of first prime=2, since it cannot be
              ;;; part of the magic square
              ;;; (by odd/even analysis of the row/col sums)
              (do ((nn start-nn (1+ nn)))
                  ((> nn end-nn))
                (begin
                  (newline)
                  (display
                   (format #f "~a~%" separator-line))
                  (display
                   (ice-9-format:format
                    #f "(~:dx~:d) : ~a~%"
                    nn nn
                    (timer-module:current-date-time-string)
                    ))
                  (display
                   (format #f "~a~%" separator-line))
                  (force-output)
                  (timer-module:time-code-macro
                   (begin
                     (let ((rlist
                            (main-build-loop
                             nn plen ptrim-list
                             find-all-bool
                             start-jday status-minutes)
                            ))
                       (let ((next-start-jday
                              (list-ref rlist 1)))
                         (begin
                           (set! start-jday next-start-jday)
                           )))
                     ))
                  ))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
