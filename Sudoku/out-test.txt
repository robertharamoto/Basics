Thu Sep 26 05:04:50 PM HST 2024
load file ./objects/sudoku-module-tests-1.go
load file ./objects/utils-module-tests-1.go
load file ./objects/timer-module-tests-1.go

################################################################
################################################################
###                                                          ###
###  Run unit tests (version 2024-09-12)                     ###
###                                                          ###
###  number of tests                    25                   ###
###                                                          ###
###  successful asserts                746       100.00%     ###
###  failed asserts                      0         0.00%     ###
###  ---------                   ---------     ---------     ###
###  total asserts                     746       100.00%     ###
###                                                          ###
################################################################
################################################################
(unit test version 2024-09-26)

elapsed time = 0.049 seconds : thursday, september 26, 2024  05:04:50 pm


sources loc = 2404 total
tests loc =  2819 total
total loc =  5223 total
