;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sudoku-module - functions to solve sudoku            ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated May 20, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start sudoku modules
(define-module (sudoku-module)
  #:export (find-min-possibilities
            eliminate-other-cells-possibilities
            check-ok-to-add-here?

            first-pass-reduction
            solve-sudoku!
            replace-zeros-with-all!

            are-rows-valid?
            are-cols-valid?
            are-sub-squares-valid?

            is-valid-sudoku-array?
            three-digit-number
            display-sudoku-array

            read-in-file
            main-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ice-9 rdelim for read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

(use-modules ((sudoku-module)
              :renamer (symbol-prefix-proc 'sudoku-module:)))

;;;#############################################################
;;;#############################################################
;;;### begin main functions

;;;#############################################################
;;;#############################################################
(define (find-min-possibilities
         sudoku-array max-rows max-cols this-row this-col)
  (begin
    (let ((min-row -1)
          (min-col -1)
          (min-len -1)
          (min-list (list)))
      (begin
        (do ((ii-row 0 (1+ ii-row)))
            ((>= ii-row max-rows))
          (begin
            (do ((ii-col 0 (1+ ii-col)))
                ((>= ii-col max-cols))
              (begin
                (let ((s-list
                       (array-ref sudoku-array ii-row ii-col)))
                  (begin
                    (if (list? s-list)
                        (begin
                          (let ((s-len (length s-list)))
                            (begin
                              (if (and
                                   (not (= ii-row this-row))
                                   (not (= ii-col this-col)))
                                  (begin
                                    (if (or (< min-len 0)
                                            (<= s-len min-len))
                                        (begin
                                          (set! min-row ii-row)
                                          (set! min-col ii-col)
                                          (set! min-len s-len)
                                          (set! min-list s-list)
                                          ))
                                    ))
                              ))
                          ))
                    ))
                ))
            ))

        (list min-row min-col min-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax eliminate-dd-macro
  (syntax-rules ()
    ((eliminate-dd-macro sudoku-array ii jj dd)
     (begin
       (let ((possible-list (array-ref sudoku-array ii jj)))
         (begin
           (if (list? possible-list)
               (begin
                 (let ((next-possible-list
                        (delete dd possible-list)))
                   (begin
                     (if (> (length next-possible-list) 0)
                         (begin
                           (array-set!
                            sudoku-array next-possible-list ii jj)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (eliminate-other-cells-possibilities
         sudoku-array max-rows max-cols sub-block-size
         dd-row dd-col dd)
  (begin
    ;;; row first
    (do ((ii 0 (1+ ii)))
        ((>= ii max-rows))
      (begin
        (if (not (= ii dd-row))
            (begin
              (eliminate-dd-macro
               sudoku-array ii dd-col dd)
              ))
        ))
    ;;; column
    (do ((jj 0 (1+ jj)))
        ((>= jj max-cols))
      (begin
        (if (not (= jj dd-col))
            (begin
              (eliminate-dd-macro
               sudoku-array dd-row jj dd)
              ))
        ))
    ;;; sub-blocks
    (let ((sub-block-srow
           (euclidean/ dd-row sub-block-size))
          (sub-block-scol
           (euclidean/ dd-col sub-block-size)))
      (let ((sr-start
             (* sub-block-size sub-block-srow))
            (sc-start
             (* sub-block-size sub-block-scol)))
        (begin
          (do ((ii-rr 0 (1+ ii-rr)))
              ((>= ii-rr sub-block-size))
            (begin
              (let ((ii-row (+ ii-rr sr-start)))
                (begin
                  (if (not (= ii-row dd-row))
                      (begin
                        (do ((jj-cc 0 (1+ jj-cc)))
                            ((>= jj-cc sub-block-size))
                          (begin
                            (let ((jj-col (+ jj-cc sc-start)))
                              (begin
                                (if (not (= jj-col dd-col))
                                    (begin
                                      (eliminate-dd-macro
                                       sudoku-array ii-row jj-col dd)
                                      ))
                                ))
                            ))
                        ))
                  ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax check-row-first-macro
  (syntax-rules ()
    ((check-row-first-macro
      sudoku-array max-rows max-cols
      sub-block-size dd dd-row dd-col
      check-valid-flag)
     (begin
       (do ((ii 0 (1+ ii)))
           ((or (>= ii max-rows)
                (equal? check-valid-flag #f)))
         (begin
           (let ((row-elem (array-ref sudoku-array ii dd-col)))
             (begin
               (if (and
                    (not (= ii dd-row))
                    (number? row-elem)
                    (= row-elem dd))
                   (begin
                     (set! check-valid-flag #f)
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax check-col-first-macro
  (syntax-rules ()
    ((check-col-first-macro
      sudoku-array max-rows max-cols
      sub-block-size dd dd-row dd-col
      check-valid-flag)
     (begin
       (do ((jj 0 (1+ jj)))
           ((or (>= jj max-cols)
                (equal? check-valid-flag #f)))
         (begin
           (let ((col-elem (array-ref sudoku-array dd-row jj)))
             (begin
               (if (and
                    (not (= jj dd-col))
                    (number? col-elem)
                    (= col-elem dd))
                   (begin
                     (set! check-valid-flag #f)
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax check-sub-blocks-first-macro
  (syntax-rules ()
    ((check-sub-blocks-first-macro
      sudoku-array max-rows max-cols
      sub-block-size dd dd-row dd-col
      check-valid-flag)
     (begin
       (let ((sub-block-srow
              (euclidean/ dd-row sub-block-size))
             (sub-block-scol
              (euclidean/ dd-col sub-block-size)))
         (let ((sr-start
                (* sub-block-size sub-block-srow))
               (sc-start
                (* sub-block-size sub-block-scol)))
           (begin
             (do ((ii-rr 0 (1+ ii-rr)))
                 ((or (>= ii-rr sub-block-size)
                      (equal? check-valid-flag #f)))
               (begin
                 (let ((ii-row (+ ii-rr sr-start)))
                   (begin
                     (if (not (= ii-row dd-row))
                         (begin
                           (do ((jj-cc 0 (1+ jj-cc)))
                               ((or (>= jj-cc sub-block-size)
                                    (equal? check-valid-flag #f)))
                             (begin
                               (let ((jj-col (+ jj-cc sc-start)))
                                 (begin
                                   (if (not (= jj-col dd-col))
                                       (begin
                                         (let ((elem
                                                (array-ref
                                                 sudoku-array
                                                 ii-row jj-col)))
                                           (begin
                                             (if (and
                                                  (number? elem)
                                                  (= elem dd))
                                                 (begin
                                                   (set! check-valid-flag #f)
                                                   ))
                                             ))
                                         ))
                                   ))
                               ))
                           ))
                     ))
                 ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (check-ok-to-add-here?
         sudoku-array max-rows max-cols sub-block-size
         dd-row dd-col dd)
  (begin
    (let ((check-valid-flag #t))
      (begin
        ;;; row first
        (check-row-first-macro
         sudoku-array max-rows max-cols
         sub-block-size dd dd-row dd-col
         check-valid-flag)

        ;;; column
        (check-col-first-macro
         sudoku-array max-rows max-cols
         sub-block-size dd dd-row dd-col
         check-valid-flag)

        ;;; sub-blocks
        (check-sub-blocks-first-macro
         sudoku-array max-rows max-cols
         sub-block-size dd dd-row dd-col
         check-valid-flag)

        check-valid-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (first-pass-reduction
         sudoku-array max-rows max-cols sub-block-size)
  (begin
    (do ((ii-row 0 (1+ ii-row)))
        ((>= ii-row max-rows))
      (begin
        (do ((jj-col 0 (1+ jj-col)))
            ((>= jj-col max-cols))
          (begin
            (let ((this-elem
                   (array-ref sudoku-array ii-row jj-col)))
              (begin
                (if (not (list? this-elem))
                    (begin
                      (eliminate-other-cells-possibilities
                       sudoku-array max-rows max-cols sub-block-size
                       ii-row jj-col this-elem)
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-multi-element-macro
  (syntax-rules ()
    ((inner-multi-element-macro
      sudoku-array original-array max-rows max-cols
      sub-square-size this-row this-col
      possible-list start-row start-col
      depth
      continue-loop-flag valid-flag)
     (begin
       (let ((plen (length possible-list))
             (pok-flag #f))
         (begin
           ;;; try all possibilities, stop if found one that works
           (do ((ii-loop 0 (1+ ii-loop)))
               ((or (>= ii-loop plen)
                    (equal? pok-flag #t)))
             (begin
               (let ((dd (list-ref possible-list ii-loop)))
                 (begin
                   (if (check-ok-to-add-here?
                        sudoku-array max-rows max-cols sub-square-size
                        this-row this-col dd)
                       (begin
                         (array-copy! sudoku-array original-array)
                         (array-set! sudoku-array dd this-row this-col)
                         (eliminate-other-cells-possibilities
                          sudoku-array max-rows max-cols sub-square-size
                          this-row this-col dd)

                         (let ((vflag
                                (solve-sudoku!
                                 sudoku-array max-rows max-cols
                                 sub-square-size (1+ depth))))
                           (begin
                             (if (equal? vflag #t)
                                 (begin
                                   (set! continue-loop-flag #f)
                                   (set! pok-flag #t)
                                   (set! valid-flag #t))
                                 (begin
                                   (array-copy! original-array sudoku-array)
                                   (set! start-row this-row)
                                   (set! start-col this-col)
                                   ))
                             ))
                         ))
                   ))
               ))
           (if (equal? pok-flag #f)
               (begin
                 (set! continue-loop-flag #f)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (solve-sudoku!
         sudoku-array max-rows max-cols
         sub-square-size depth)
  (begin
    (let ((start-row -1)
          (start-col -1)
          (continue-loop-flag #t)
          (valid-flag #f)
          (max-seen 3)
          (seen-htable (make-hash-table))
          (original-array
           (make-array 0 max-rows max-cols)))
      (begin
        (while (equal? continue-loop-flag #t)
          (begin
            (let ((rlist
                   (find-min-possibilities
                    sudoku-array max-rows max-cols
                    start-row start-col)))
              (let ((possible-len (length (list-ref rlist 2)))
                    (hcount (hash-ref seen-htable rlist 0)))
                (begin
                  (if (<= possible-len 2)
                      (begin
                        (set! hcount (1+ hcount))
                        (hash-set! seen-htable rlist hcount)

                        (if (>= hcount max-seen)
                            (begin
                              (set! continue-loop-flag #f)
                              (set! valid-flag #f)
                              ))
                        ))

                  (let ((this-row (list-ref rlist 0))
                        (this-col (list-ref rlist 1))
                        (possible-list (list-ref rlist 2)))
                    (begin
                      (if (and (>= this-row 0)
                               (>= this-col 0))
                          (begin
                            (inner-multi-element-macro
                             sudoku-array original-array max-rows max-cols
                             sub-square-size this-row this-col
                             possible-list start-row start-col
                             depth
                             continue-loop-flag valid-flag))
                          (begin
                            (set! continue-loop-flag #f)
                            ))
                      ))
                  )))

            (if (not (equal? valid-flag #t))
                (begin
                  (let ((v-flag
                         (is-valid-sudoku-array?
                          sudoku-array max-rows max-cols sub-square-size)))
                    (begin
                      (if (equal? v-flag #t)
                          (begin
                            (set! continue-loop-flag #f)
                            (set! valid-flag #t)
                            ))
                      ))
                  ))
            ))

        valid-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (replace-zeros-with-all! aa-array max-rows max-cols)
  (begin
    (let ((all-possibilities (list 1 2 3 4 5 6 7 8 9)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-rows))
          (begin
            (do ((jj 0 (1+ jj)))
                ((>= jj max-cols))
              (begin
                (let ((elem (array-ref aa-array ii jj)))
                  (begin
                    (if (<= elem 0)
                        (begin
                          (array-set!
                           aa-array all-possibilities ii jj)
                          ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (are-rows-valid? sudoku-array max-rows max-cols)
  (begin
    (let ((numbers-list (list))
          (valid-flag #t))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii max-rows)
                 (equal? valid-flag #f)))
          (begin
            (do ((jj 0 (1+ jj)))
                ((or (>= jj max-cols)
                     (equal? valid-flag #f)))
              (begin
                (let ((this-elem
                       (array-ref sudoku-array ii jj)))
                  (begin
                    (if (and
                         (not (list? this-elem))
                         (> this-elem 0)
                         (< this-elem 10)
                         (equal?
                          (member this-elem numbers-list)
                          #f))
                        (begin
                          (set!
                           numbers-list
                           (cons this-elem numbers-list)))
                        (begin
                          (set! valid-flag #f)
                          ))
                    ))
                ))

            (if (equal? valid-flag #t)
                (begin
                  (set! numbers-list (list))
                  ))
            ))

        valid-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (are-cols-valid? sudoku-array max-rows max-cols)
  (begin
    (let ((numbers-list (list))
          (valid-flag #t))
      (begin
        (do ((jj 0 (1+ jj)))
            ((or (>= jj max-cols)
                 (equal? valid-flag #f)))
          (begin
            (do ((ii 0 (1+ ii)))
                ((or (>= ii max-rows)
                     (equal? valid-flag #f)))
              (begin
                (let ((this-elem
                       (array-ref sudoku-array ii jj)))
                  (begin
                    (if (and
                         (not (list? this-elem))
                         (> this-elem 0)
                         (< this-elem 10)
                         (equal?
                          (member this-elem numbers-list)
                          #f))
                        (begin
                          (set!
                           numbers-list
                           (cons this-elem numbers-list)))
                        (begin
                          (set! valid-flag #f)
                          ))
                    ))
                ))

            (if (equal? valid-flag #t)
                (begin
                  (set! numbers-list (list))
                  ))
            ))

        valid-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (are-sub-squares-valid?
         sudoku-array sub-square-size max-rows max-cols)
  (begin
    (let ((numbers-list (list))
          (valid-flag #t)
          (max-block-rows
           (euclidean-quotient max-rows sub-square-size))
          (max-block-cols
           (euclidean-quotient max-cols sub-square-size)))
      (begin
        ;;; loop over sub-blocks
        (do ((ii-block 0 (1+ ii-block)))
            ((or (>= ii-block max-block-rows)
                 (equal? valid-flag #f)))
          (begin
            (let ((init-row
                   (* ii-block sub-square-size))
                  (end-row
                   (* (+ ii-block 1) sub-square-size)))
              (begin
                (do ((jj-block 0 (1+ jj-block)))
                    ((or (>= jj-block max-block-cols)
                         (equal? valid-flag #f)))
                  (begin
                    (let ((init-col
                           (* jj-block sub-square-size))
                          (end-col
                           (* (+ jj-block 1) sub-square-size)))
                      (begin
                        ;;; loop over elements within a sub-block
                        (do ((ii init-row (1+ ii)))
                            ((or (>= ii end-row)
                                 (equal? valid-flag #f)))
                          (begin
                            (do ((jj init-col (1+ jj)))
                                ((or (>= jj end-col)
                                     (equal? valid-flag #f)))
                              (begin
                                (let ((this-elem
                                       (array-ref sudoku-array ii jj)))
                                  (begin
                                    (if (and
                                         (not (list? this-elem))
                                         (> this-elem 0)
                                         (< this-elem 10)
                                         (equal?
                                          (member this-elem numbers-list)
                                          #f))
                                        (begin
                                          (set!
                                           numbers-list
                                           (cons this-elem numbers-list)))
                                        (begin
                                          (set! valid-flag #f)
                                          ))
                                    ))
                                ))
                            ))
                        (if (equal? valid-flag #t)
                            (begin
                              (set! numbers-list (list))
                              ))
                        ))
                    ))
                ))
            ))

        valid-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (is-valid-sudoku-array?
         sudoku-array max-rows max-cols sub-square-size)
  (begin
    (cond
     ((equal?
       (are-rows-valid? sudoku-array max-rows max-cols)
       #f)
      (begin
        #f
        ))
     ((equal?
       (are-cols-valid? sudoku-array max-rows max-cols)
       #f)
      (begin
        #f
        ))
     ((equal?
       (are-sub-squares-valid?
        sudoku-array sub-square-size max-rows max-cols)
       #f)
      (begin
        #f
        ))
     (else
      (begin
        #t
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (three-digit-number sudoku-array)
  (begin
    (let ((result-num 0))
      (begin
        (do ((jj 0 (1+ jj)))
            ((>= jj 3))
          (begin
            (let ((this-elem (array-ref sudoku-array 0 jj)))
              (begin
                (set! result-num (+ (* result-num 10) this-elem))
                ))
            ))

        result-num
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-sudoku-array
         sudoku-array sub-square-size
         max-rows max-cols)
  (begin
    (let ((hlen
           (1+ (* 2 (+ max-cols sub-square-size)))))
      (let ((hline (make-string hlen #\-)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii max-rows))
            (begin
              (let ((pstring ""))
                (begin
                  (do ((jj 0 (1+ jj)))
                      ((>= jj max-cols))
                    (begin
                      (let ((elem-string
                             (format
                              #f "~a"
                              (array-ref sudoku-array ii jj))))
                        (begin
                          (if (zero? (modulo jj sub-square-size))
                              (begin
                                (set!
                                 pstring
                                 (string-append pstring " | " elem-string)))
                              (begin
                                (set!
                                 pstring
                                 (string-append pstring " " elem-string))
                                ))
                          ))
                      ))

                  (if (zero? (modulo ii sub-square-size))
                      (begin
                        (display (format #f " ~a~%" hline))
                        ))

                  (display (format #f "~a |~%" pstring))
                  ))
              ))
          (display (format #f " ~a~%" hline))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list-list (list))
          (current-list-list (list))
          (counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited "\r\n")
                          (ice-9-rdelim:read-delimited "\r\n")))
                        ((eof-object? line))
                      (begin
                        (if (and (not (eof-object? line))
                                 (> (string-length line) 0))
                            (begin
                              (cond
                               ((string-prefix-ci? "grid" line)
                                (begin
                                  (set! counter (1+ counter))
                                  (if (and (list? current-list-list)
                                           (> (length current-list-list) 0))
                                      (begin
                                        (set!
                                         results-list-list
                                         (cons
                                          (reverse current-list-list)
                                          results-list-list))
                                        (set!
                                         current-list-list (list))
                                        ))
                                  ))
                               (else
                                (begin
                                  (let ((this-number-list
                                         (map
                                          string->number
                                          (map
                                           string
                                           (string->list
                                            (string-trim-both line)))
                                          )))
                                    (begin
                                      (set!
                                       current-list-list
                                       (cons
                                        this-number-list
                                        current-list-list))
                                      ))
                                  )))
                              ))
                        ))
                    )))

              (if (and (list? current-list-list)
                       (> (length current-list-list) 0))
                  (begin
                    (set!
                     results-list-list
                     (cons
                      (reverse current-list-list)
                      results-list-list))
                    (set! current-list-list (list))
                    ))

              (display
               (format
                #f "read in ~a grids from ~a~%"
                counter fname))
              (newline)
              (force-output)

              (reverse results-list-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop filename sub-square-size debug-flag)
  (begin
    (let ((scounter 0)
          (three-digit-sum 0)
          (depth 0)
          (sudoku-list-list (read-in-file filename)))
      (begin
        (for-each
         (lambda (aa-list-list)
           (begin
             (set! scounter (1+ scounter))
             (let ((aa-array (list->array 2 aa-list-list)))
               (let ((array-dims (array-dimensions aa-array)))
                 (let ((max-rows (list-ref array-dims 0))
                       (max-cols (list-ref array-dims 1)))
                   (begin
                     (if (equal? debug-flag #t)
                         (begin
                           (newline)
                           (display
                            (format #f "(~a) initial matrix~%"
                                    scounter))
                           (display-sudoku-array
                            aa-array sub-square-size max-rows max-cols)
                           (force-output)
                           ))

                     (replace-zeros-with-all! aa-array max-rows max-cols)

                     ;;; get rid of possibilities using the given elements
                     (first-pass-reduction
                      aa-array max-rows max-cols
                      sub-square-size)

                     (solve-sudoku!
                      aa-array max-rows max-cols
                      sub-square-size depth)

                     (if (is-valid-sudoku-array?
                          aa-array max-rows max-cols sub-square-size)
                         (begin
                           (let ((tcode (three-digit-number aa-array)))
                             (begin
                               (set! three-digit-sum (+ three-digit-sum tcode))

                               (if (equal? debug-flag #t)
                                   (begin
                                     (display
                                      (format
                                       #f "(~a) completed matrix~%"
                                       scounter))
                                     (display-sudoku-array
                                      aa-array sub-square-size
                                      max-rows max-cols)
                                     (force-output)
                                     ))

                               (display
                                (ice-9-format:format
                                 #f "  (~:d) three digit code = ~:d : "
                                 scounter tcode))
                               (display
                                (ice-9-format:format
                                 #f "sum so far = ~:d~%"
                                 three-digit-sum))
                               (force-output)
                               )))
                         (begin
                           (display
                            (ice-9-format:format
                             #f " (~:d) invalid sudoku array found!~%"
                             scounter))
                           (display-sudoku-array
                            aa-array sub-square-size
                            max-rows max-cols)

                           (let ((aa-array (list->array 2 aa-list-list)))
                             (let ((array-dims (array-dimensions aa-array)))
                               (let ((max-rows (list-ref array-dims 0))
                                     (max-cols (list-ref array-dims 1)))
                                 (begin
                                   (display
                                    (format #f "original matrix~%"))
                                   (display-sudoku-array
                                    aa-array sub-square-size
                                    max-rows max-cols)

                                   (display
                                    (format
                                     #f "stopping program...~%"))
                                   (force-output)
                                   (quit)
                                   ))
                               ))
                           ))
                     ))
                 ))
             )) sudoku-list-list)

        (newline)
        (display
         (ice-9-format:format
          #f "The sum of all 3-digit numbers of all ~:d "
          scounter))
        (display
         (ice-9-format:format
          #f "puzzles is ~:d~%"
          three-digit-sum))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
