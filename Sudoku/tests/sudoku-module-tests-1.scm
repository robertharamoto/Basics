;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for sudoku-module.scm                     ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 20, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((sudoku-module)
              :renamer (symbol-prefix-proc 'sudoku-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (list-array-check
         shouldbe-list-list result-array
         max-rows max-cols
         sub-name test-label-index
         results-hash-table)
  (begin
    (do ((ii 0 (1+ ii)))
        ((>= ii max-rows))
      (begin
        (do ((jj 0 (1+ jj)))
            ((>= jj max-cols))
          (begin
            (let ((s-num
                   (list-ref
                    (list-ref shouldbe-list-list ii) jj))
                  (r-num (array-ref result-array ii jj)))
              (let ((err-1
                     (format
                      #f "~a : (~a) : error : row/col=~a/~a : "
                      sub-name test-label-index ii jj))
                    (err-2
                     (format
                      #f "shouldbe=~a, result=~a~%"
                      s-num r-num)))
                (begin
                  (unittest2:assert?
                   (equal? s-num r-num)
                   sub-name
                   (string-append err-1 err-2)
                   results-hash-table)
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (test-equals-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         results-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : error (~a) : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe=~a, result=~a"
            shouldbe result)))
      (begin
        (unittest2:assert?
         (equal? shouldbe result)
         sub-name
         (string-append
          err-1 err-2)
         results-hash-table)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-min-possibilities-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-find-min-possibilities-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (list
             (list (list 1 2 3 4 5 6) (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6) (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            0 0
            (list 4 1 (list 1 2 3)))
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            0 0
            (list 6 7 (list 1 2)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (this-row (list-ref this-list 1))
                  (this-col (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((max-rows (car (array-dimensions aa-array)))
                      (max-cols (cadr (array-dimensions aa-array))))
                  (let ((result
                         (sudoku-module:find-min-possibilities
                          aa-array max-rows max-cols this-row this-col)))
                    (begin
                      (test-equals-check
                       shouldbe result
                       "first-row" (car aa-list-list)
                       sub-name test-label-index
                       results-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-eliminate-other-cells-possibilities-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-eliminate-other-cells-possibilities-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            1 1 3
            (list
             (list (list 1 2 4 5 6)
                   (list 1 2 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 4 5 6)
                   3 (list 1 2 4 5 6)
                   5 (list 1 2 4 5 6)
                   (list 1 2 4 5 6 7) 1)
             (list (list 1 2 4 5 6 7 8 9)
                   (list 1 2 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))))
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            7 8 9
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8))
             (list 8 (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   2 (list 1 2 3 4 5 6 7 8)
                   3 (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8))))
           ))
         (sub-block-size 3)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (dd-row (list-ref this-list 1))
                  (dd-col (list-ref this-list 2))
                  (dd (list-ref this-list 3))
                  (shouldbe-list-list (list-ref this-list 4)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((max-rows (car (array-dimensions aa-array)))
                      (max-cols (cadr (array-dimensions aa-array))))
                  (begin
                    (sudoku-module:eliminate-other-cells-possibilities
                     aa-array max-rows max-cols sub-block-size
                     dd-row dd-col dd)

                    (list-array-check
                     shouldbe-list-list aa-array
                     max-rows max-cols
                     sub-name test-label-index
                     results-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-check-ok-to-add-here-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-check-ok-to-add-here-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   7 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            0 0 1 #t)
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            0 0 3 #f)
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            0 0 6 #f)
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            8 8 5 #f)
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            8 8 1 #f)
           (list
            (list
             (list (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   2 (list 1 2 3 4 5 6)
                   6 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 9 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6)
                   3 (list 1 2 3 4 5 6)
                   5 (list 1 2 3 4 5 6)
                   (list 1 2 3 4 5 6 7) 1)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   1 8 (list 1 2 3 4 5 6 7 8 9)
                   6 4 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   8 1 (list 1 2 3 4 5 6 7 8 9)
                   2 9 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 7 (list 1 2 3)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 8)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   6 7 (list 1 2 3 4 5 6 7 8 9)
                   8 2 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 6 (list 1 2 3 4 5 6 7 8 9)
                   9 5 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list 8 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   2 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9)
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   5 (list 1 2 3 4 5 6 7 8 9)
                   1 (list 1 2 3 4 5 6 7 8 9)
                   3 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)))
            5 7 8 #f)
           ))
         (sub-block-size 3)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (dd-row (list-ref this-list 1))
                  (dd-col (list-ref this-list 2))
                  (dd (list-ref this-list 3))
                  (shouldbe (list-ref this-list 4)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((max-rows (car (array-dimensions aa-array)))
                      (max-cols (cadr (array-dimensions aa-array))))
                  (let ((result
                         (sudoku-module:check-ok-to-add-here?
                          aa-array max-rows max-cols sub-block-size
                          dd-row dd-col dd)))
                    (let ((err-2
                           (format
                            #f "row/col=~a/~a, dd="
                            dd-row dd-col))
                          (shouldbe-string
                           (if shouldbe "true" "false"))
                          (result-string
                           (if result "true" "false")))
                      (begin
                        (test-equals-check
                         shouldbe-string
                         result-string
                         err-2 dd
                         sub-name test-label-index
                         results-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-first-pass-reduction-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-first-pass-reduction-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list
            (list
             (list 1 (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9))
             (list (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9) 9))
            (list
             (list 1 (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8 9)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8))
             (list (list 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8)
                   (list 1 2 3 4 5 6 7 8) 9)))
           ))
         (sub-block-size 3)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list-list)))
                (let ((max-rows (car (array-dimensions aa-array)))
                      (max-cols (cadr (array-dimensions aa-array))))
                  (begin
                    (sudoku-module:first-pass-reduction
                     aa-array max-rows max-cols sub-block-size)

                    (list-array-check
                     shouldbe-list-list aa-array
                     max-rows max-cols
                     sub-name test-label-index
                     results-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-solve-sudoku-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-solve-sudoku-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 3 0 2 0 6 0 0)
                       (list 9 0 0 3 0 5 0 0 1)
                       (list 0 0 1 8 0 6 4 0 0)
                       (list 0 0 8 1 0 2 9 0 0)
                       (list 7 0 0 0 0 0 0 0 8)
                       (list 0 0 6 7 0 8 2 0 0)
                       (list 0 0 2 6 0 9 5 0 0)
                       (list 8 0 0 2 0 3 0 0 9)
                       (list 0 0 5 0 1 0 3 0 0))
                 (list (list 4 8 3 9 2 1 6 5 7)
                       (list 9 6 7 3 4 5 8 2 1)
                       (list 2 5 1 8 7 6 4 9 3)
                       (list 5 4 8 1 3 2 9 7 6)
                       (list 7 2 9 5 6 4 1 3 8)
                       (list 1 3 6 7 9 8 2 4 5)
                       (list 3 7 2 6 8 9 5 1 4)
                       (list 8 1 4 2 5 3 7 6 9)
                       (list 6 9 5 4 1 7 3 8 2)))
           (list (list (list 2 0 0 0 8 0 3 0 0)
                       (list 0 6 0 0 7 0 0 8 4)
                       (list 0 3 0 5 0 0 2 0 9)
                       (list 0 0 0 1 0 5 4 0 8)
                       (list 0 0 0 0 0 0 0 0 0)
                       (list 4 0 2 7 0 6 0 0 0)
                       (list 3 0 1 0 0 7 0 4 0)
                       (list 7 2 0 0 4 0 0 6 0)
                       (list 0 0 4 0 1 0 0 0 3))
                 (list (list 2 4 5 9 8 1 3 7 6)
                       (list 1 6 9 2 7 3 5 8 4)
                       (list 8 3 7 5 6 4 2 1 9)
                       (list 9 7 6 1 2 5 4 3 8)
                       (list 5 1 3 4 9 8 6 2 7)
                       (list 4 8 2 7 3 6 9 5 1)
                       (list 3 9 1 6 5 7 8 4 2)
                       (list 7 2 8 3 4 9 1 6 5)
                       (list 6 5 4 8 1 2 7 9 3)))
           (list (list (list 0 0 0 0 0 0 9 0 7)
                       (list 0 0 0 4 2 0 1 8 0)
                       (list 0 0 0 7 0 5 0 2 6)
                       (list 1 0 0 9 0 4 0 0 0)
                       (list 0 5 0 0 0 0 0 4 0)
                       (list 0 0 0 5 0 7 0 0 9)
                       (list 9 2 0 1 0 8 0 0 0)
                       (list 0 3 4 0 5 9 0 0 0)
                       (list 5 0 7 0 0 0 0 0 0))
                 (list (list 4 6 2 8 3 1 9 5 7)
                       (list 7 9 5 4 2 6 1 8 3)
                       (list 3 8 1 7 9 5 4 2 6)
                       (list 1 7 3 9 8 4 2 6 5)
                       (list 6 5 9 3 1 2 7 4 8)
                       (list 2 4 8 5 6 7 3 1 9)
                       (list 9 2 6 1 7 8 5 3 4)
                       (list 8 3 4 2 5 9 6 7 1)
                       (list 5 1 7 6 4 3 8 9 2)))
           (list (list (list 0 0 0 0 0 0 0 8 0)
                       (list 8 0 0 7 0 1 0 4 0)
                       (list 0 4 0 0 2 0 0 3 0)
                       (list 3 7 4 0 0 0 9 0 0)
                       (list 0 0 0 0 3 0 0 0 0)
                       (list 0 0 5 0 0 0 3 2 1)
                       (list 0 1 0 0 6 0 0 5 0)
                       (list 0 5 0 8 0 2 0 0 6)
                       (list 0 8 0 0 0 0 0 0 0))
                 (list (list 7 6 1 5 4 3 2 8 9)
                       (list 8 3 2 7 9 1 6 4 5)
                       (list 5 4 9 6 2 8 1 3 7)
                       (list 3 7 4 2 1 5 9 6 8)
                       (list 1 2 8 9 3 6 5 7 4)
                       (list 6 9 5 4 8 7 3 2 1)
                       (list 4 1 7 3 6 9 8 5 2)
                       (list 9 5 3 8 7 2 4 1 6)
                       (list 2 8 6 1 5 4 7 9 3)))
           ))
         (sub-square-size 3)
         (depth 0)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list)))
                (let ((max-rows (car (array-dimensions aa-array)))
                      (max-cols (cadr (array-dimensions aa-array))))
                  (begin
                    (sudoku-module:replace-zeros-with-all!
                     aa-array max-rows max-cols)

                    (sudoku-module:first-pass-reduction
                     aa-array max-rows max-cols sub-square-size)

                    (sudoku-module:solve-sudoku!
                     aa-array max-rows max-cols
                     sub-square-size depth)

                    (list-array-check
                     shouldbe aa-array
                     max-rows max-cols
                     sub-name test-label-index
                     results-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-rows-valid-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-are-rows-valid-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 3 0 2 0 6 0 0)
                       (list 9 0 0 3 0 5 0 0 1)
                       (list 0 0 1 8 0 6 4 0 0)
                       (list 0 0 8 1 0 2 9 0 0)
                       (list 7 0 0 0 0 0 0 0 8)
                       (list 0 0 6 7 0 8 2 0 0)
                       (list 0 0 2 6 0 9 5 0 0)
                       (list 8 0 0 2 0 3 0 0 9)
                       (list 0 0 5 0 1 0 3 0 0))
                 #f)
           (list (list (list 4 8 3 9 2 1 6 5 7)
                       (list 9 6 7 3 4 5 8 2 1)
                       (list 2 5 1 8 7 6 4 9 3)
                       (list 5 4 8 1 3 2 9 7 6)
                       (list 7 2 9 5 6 4 1 3 8)
                       (list 1 3 6 7 9 8 2 4 5)
                       (list 3 7 2 6 8 9 5 1 4)
                       (list 8 1 4 2 5 3 7 6 9)
                       (list 6 9 5 4 1 7 3 8 2))
                 #t)
           ))
         (max-rows 9)
         (max-cols 9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list)))
                (let ((result
                       (sudoku-module:are-rows-valid?
                        aa-array max-rows max-cols)))
                  (let ((shouldbe-string
                         (if shouldbe "true" "false"))
                        (result-string
                         (if result "true" "false")))
                    (begin
                      (test-equals-check
                       shouldbe-string result-string
                       "aa-array" aa-array
                       sub-name test-label-index
                       results-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-cols-valid-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-are-cols-valid-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 3 0 2 0 6 0 0)
                       (list 9 0 0 3 0 5 0 0 1)
                       (list 0 0 1 8 0 6 4 0 0)
                       (list 0 0 8 1 0 2 9 0 0)
                       (list 7 0 0 0 0 0 0 0 8)
                       (list 0 0 6 7 0 8 2 0 0)
                       (list 0 0 2 6 0 9 5 0 0)
                       (list 8 0 0 2 0 3 0 0 9)
                       (list 0 0 5 0 1 0 3 0 0))
                 #f)
           (list (list (list 4 8 3 9 2 1 6 5 7)
                       (list 9 6 7 3 4 5 8 2 1)
                       (list 2 5 1 8 7 6 4 9 3)
                       (list 5 4 8 1 3 2 9 7 6)
                       (list 7 2 9 5 6 4 1 3 8)
                       (list 1 3 6 7 9 8 2 4 5)
                       (list 3 7 2 6 8 9 5 1 4)
                       (list 8 1 4 2 5 3 7 6 9)
                       (list 6 9 5 4 1 7 3 8 2))
                 #t)
           ))
         (max-rows 9)
         (max-cols 9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list)))
                (let ((result
                       (sudoku-module:are-cols-valid?
                        aa-array max-rows max-cols)))
                  (let ((shouldbe-string
                         (if shouldbe "true" "false"))
                        (result-string
                         (if result "true" "false")))
                    (begin
                      (test-equals-check
                       shouldbe-string result-string
                       "aa-array" aa-array
                       sub-name test-label-index
                       results-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-sub-squares-valid-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-are-sub-squares-valid-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 3 0 2 0 6 0 0)
                       (list 9 0 0 3 0 5 0 0 1)
                       (list 0 0 1 8 0 6 4 0 0)
                       (list 0 0 8 1 0 2 9 0 0)
                       (list 7 0 0 0 0 0 0 0 8)
                       (list 0 0 6 7 0 8 2 0 0)
                       (list 0 0 2 6 0 9 5 0 0)
                       (list 8 0 0 2 0 3 0 0 9)
                       (list 0 0 5 0 1 0 3 0 0))
                 #f)
           (list (list (list 4 8 3 9 2 1 6 5 7)
                       (list 9 6 7 3 4 5 8 2 1)
                       (list 2 5 1 8 7 6 4 9 3)
                       (list 5 4 8 1 3 2 9 7 6)
                       (list 7 2 9 5 6 4 1 3 8)
                       (list 1 3 6 7 9 8 2 4 5)
                       (list 3 7 2 6 8 9 5 1 4)
                       (list 8 1 4 2 5 3 7 6 9)
                       (list 6 9 5 4 1 7 3 8 2))
                 #t)
           ))
         (sub-square-size 3)
         (max-rows 9)
         (max-cols 9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list)))
                (let ((result
                       (sudoku-module:are-sub-squares-valid?
                        aa-array sub-square-size max-rows max-cols)))
                  (let ((shouldbe-string
                         (if shouldbe "true" "false"))
                        (result-string
                         (if result "true" "false")))
                    (begin
                      (test-equals-check
                       shouldbe-string result-string
                       "aa-array" aa-array
                       sub-name test-label-index
                       results-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-valid-sudoku-array-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-is-valid-sudoku-array-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 0 0 3 0 2 0 6 0 0)
                       (list 9 0 0 3 0 5 0 0 1)
                       (list 0 0 1 8 0 6 4 0 0)
                       (list 0 0 8 1 0 2 9 0 0)
                       (list 7 0 0 0 0 0 0 0 8)
                       (list 0 0 6 7 0 8 2 0 0)
                       (list 0 0 2 6 0 9 5 0 0)
                       (list 8 0 0 2 0 3 0 0 9)
                       (list 0 0 5 0 1 0 3 0 0))
                 #f)
           (list (list (list 4 8 3 9 2 1 6 5 7)
                       (list 9 6 7 3 4 5 8 2 1)
                       (list 2 5 1 8 7 6 4 9 3)
                       (list 5 4 8 1 3 2 9 7 6)
                       (list 7 2 9 5 6 4 1 3 8)
                       (list 1 3 6 7 9 8 2 4 5)
                       (list 3 7 2 6 8 9 5 1 4)
                       (list 8 1 4 2 5 3 7 6 9)
                       (list 6 9 5 4 1 7 3 8 2))
                 #t)
           ))
         (sub-square-size 3)
         (max-rows 9)
         (max-cols 9)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list)))
                (let ((result
                       (sudoku-module:is-valid-sudoku-array?
                        aa-array max-rows max-cols sub-square-size)))
                  (let ((shouldbe-string
                         (if shouldbe "true" "false"))
                        (result-string
                         (if result "true" "false")))
                    (begin
                      (test-equals-check
                       shouldbe-string result-string
                       "aa-array" aa-array
                       sub-name test-label-index
                       results-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-three-digit-number-1 results-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-three-digit-number-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list (list 4 8 3 9 2 1 6 5 7)
                       (list 9 6 7 3 4 5 8 2 1)
                       (list 2 5 1 8 7 6 4 9 3)
                       (list 5 4 8 1 3 2 9 7 6)
                       (list 7 2 9 5 6 4 1 3 8)
                       (list 1 3 6 7 9 8 2 4 5)
                       (list 3 7 2 6 8 9 5 1 4)
                       (list 8 1 4 2 5 3 7 6 9)
                       (list 6 9 5 4 1 7 3 8 2))
                 483)
           (list (list (list 5 8 3 9 2 1 6 5 7)
                       (list 9 6 7 3 4 5 8 2 1)
                       (list 2 5 1 8 7 6 4 9 3)
                       (list 5 4 8 1 3 2 9 7 6)
                       (list 7 2 9 5 6 4 1 3 8)
                       (list 1 3 6 7 9 8 2 4 5)
                       (list 3 7 2 6 8 9 5 1 4)
                       (list 8 1 4 2 5 3 7 6 9)
                       (list 6 9 5 4 1 7 3 8 2))
                 583)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((aa-array (list->array 2 aa-list)))
                (let ((result
                       (sudoku-module:three-digit-number aa-array)))
                  (begin
                    (test-equals-check
                     shouldbe result
                     "aa-array" aa-array
                     sub-name test-label-index
                     results-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
