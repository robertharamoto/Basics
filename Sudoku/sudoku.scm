#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  sudoku main file                                     ###
;;;###                                                       ###
;;;###  last updated August 5, 2024                          ###
;;;###                                                       ###
;;;###  updated May 20, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 16, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "." "sources" "tests"))

(set! %load-compiled-path
      (append %load-compiled-path (list "." "objects")))

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### getopt-long used for command-line option arguments processing
(use-modules ((ice-9 getopt-long)
              :renamer (symbol-prefix-proc 'ice-9-getopt:)))

;;;### timer-module for date/timing functions
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### sudoku-module for main-loop
(use-modules ((sudoku-module)
              :renamer (symbol-prefix-proc 'sudoku-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin support functions                              ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (get-filename args default-file)
  (begin
    (let ((sudoku-fname ""))
      (begin
        (if (and (not (equal? args #f))
                 (> (length args) 1))
            (begin
              (let ((found-flag #f)
                    (arg-fname ""))
                (begin
                  (for-each
                   (lambda (an-arg)
                     (begin
                       (if (file-exists? an-arg)
                           (begin
                             (let ((amatch
                                    (ice-9-regex:string-match
                                     ".scm" an-arg)))
                               (begin
                                 (if (equal? amatch #f)
                                     (begin
                                       (set! arg-fname an-arg)
                                       (set! found-flag #t)
                                       ))
                                 ))
                             ))
                       )) args)

                  (if (equal? found-flag #t)
                      (begin
                        (set! sudoku-fname arg-fname))
                      (begin
                        (if (file-exists? default-file)
                            (begin
                              (set! sudoku-fname default-file)
                              ))
                        ))
                  )))
            (begin
              (if (file-exists? default-file)
                  (begin
                    (set! sudoku-fname default-file)
                    ))
              ))

        sudoku-fname
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help ospec title-string)
    (begin
      (display (format #f "~a~%" title-string))
      (display (format #f "options available~%"))
      (for-each
       (lambda (llist)
         (begin
           (display
            (format
             #f "  --~a, -~a~%"
             (car llist)
             (cadr (cadr llist))))
           )) ospec)
      (force-output)
      ))
  (begin
    (let ((version-string "2024-08-05"))
      (let ((title-string
             (format #f "Sudoku Puzzle (version ~a)"
                     version-string))
            (option-spec
             (list (list 'version '(single-char #\v) '(value #f))
                   (list 'help '(single-char #\h) '(value #f))
                   (list 'file '(single-char #\f) '(value #t))
                   )))
        (let ((def-fname "s1.txt")
              (options
               (ice-9-getopt:getopt-long args option-spec)))
          (begin
            (let ((help-flag
                   (ice-9-getopt:option-ref options 'help #f)))
              (begin
                (if (equal? help-flag #t)
                    (begin
                      (local-display-help option-spec title-string)
                      (quit)
                      ))
                ))
            (let ((version-flag
                   (ice-9-getopt:option-ref options 'version #f)))
              (begin
                (if (equal? version-flag #t)
                    (begin
                      (local-display-version title-string)
                      (quit)
                      ))
                ))

            (let ((s-file
                   (ice-9-getopt:option-ref options 'file #f))
                  (sudoku-list-list (list)))
              (begin
                (if (or (equal? s-file #f)
                        (not (file-exists? s-file)))
                    (begin
                      (let ((result-file
                             (get-filename args def-fname)))
                        (begin
                          (set! s-file result-file)
                          )))
                    (begin
                      (set! s-file def-fname)
                      ))

                (display
                 (format #f "Sudoku is a puzzle with a simple "))
                (display
                 (format #f "consistency requirement.~%"))
                (display
                 (format #f "The requirement is that the numbers "))
                (display
                 (format #f "from 1 through 9~%"))
                (display
                 (format #f "must appear in every row, column, "))
                (display
                 (format #f "and 3x3 sub-square~%"))
                (display
                 (format #f "exactly once.~%"))
                (newline)
                (display
                 (format #f "To read more about the problem see:~%"))
                (display
                 (format #f "https://projecteuler.net/problem=96~%"))
                (display
                 (format #f "And Peter Norvig's write-up at:~%"))
                (display
                 (format #f "https://norvig.com/sudoku.html~%"))
                (newline)
                (display
                 (format #f "examining file ~a~%" s-file))

                (let ((sub-square-size 3)
                      (debug-flag #t))
                  (begin
                    (timer-module:time-code-macro
                     (begin
                       (sudoku-module:main-loop
                        s-file sub-square-size debug-flag)
                       (newline)
                       ))
                    (force-output)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
