#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  cowsay - make ascii cow say                          ###
;;;###                                                       ###
;;;###  last updated January 23, 2025                        ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;###
;;;###  last updated January 23, 2025
;;;###
;;;###  updated May 19, 2022
;;;###
;;;###  updated February 6, 2020
;;;###

;;;#############################################################
;;;#############################################################
;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;; rdelim for read-delimited function
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;; popen for open-input-pipe functions
(use-modules ((ice-9 popen)
              :renamer (symbol-prefix-proc 'ice-9-popen:)))

;;;#############################################################
;;;#############################################################
(define (get-ascii-list)
  (begin
    (let ((acc-list (list))
          (end-flag #f))
      (begin
        (while (equal? end-flag #f)
          (begin
            (let ((this-list
                   (ice-9-rdelim:read-delimited "\n")))
              (begin
                (if (eof-object? this-list)
                    (begin
                      (set! end-flag #t))
                    (begin
                      (set! acc-list (cons this-list acc-list))
                      ))
                ))
            ))
        (reverse acc-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (read-in-file fname)
  (begin
    (with-input-from-file
        fname
      get-ascii-list)
    ))

;;;#############################################################
;;;#############################################################
(define (max-cow-length cow-string-list)
  (begin
    (let ((max-len 0))
      (begin
        (for-each
         (lambda (astring)
           (begin
             (let ((alen (string-length astring)))
               (begin
                 (if (> alen max-len)
                     (begin
                       (set! max-len alen)
                       ))
                 ))
             )) cow-string-list)
        max-len
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (draw-single-line-quote sl-string)
  (begin
    (let ((slen (string-length sl-string))
          (init-pad (make-string 4 #\space)))
      (let ((nwidth (+ 2 slen)))
        (begin
          (display
           (format #f "~a  ~a ~%" init-pad (make-string nwidth #\_)))
          (display
           (format #f "~a < ~a >~%" init-pad sl-string))
          (display
           (format #f "~a  ~a ~%" init-pad (make-string nwidth #\-)))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; assumes (>= nlines 2)
(define (draw-n-line-quote
         nl-string slen nwidth nlines)
  (begin
    (let ((ii-index 0)
          (init-pad (make-string 4 #\space))
          (ii-last nlines))
      (begin
        (display
         (format #f "~a  ~a ~%" init-pad (make-string (1+ nwidth) #\_)))
        (do ((ii 0 (1+ ii)))
            ((> ii nlines))
          (begin
            (let ((aline
                   (substring nl-string ii-index
                              (min slen (+ ii-index nwidth)))))
              (let ((this-line-length (string-length aline)))
                (let ((this-line-pad ""))
                  (begin
                    (set! ii-index (+ ii-index nwidth))
                    (if (< this-line-length nwidth)
                        (begin
                          (set! this-line-pad
                                (make-string (- nwidth this-line-length) #\space))
                          ))
                    (cond
                     ((= ii 0)
                      (begin
                        (display
                         (format
                          #f "~a / ~a~a \\ ~%" init-pad aline this-line-pad))
                        ))
                     ((= ii ii-last)
                      (begin
                        (display
                         (format
                          #f "~a \\ ~a~a / ~%" init-pad aline this-line-pad))
                        ))
                     (else
                      (begin
                        (display
                         (format
                          #f "~a | ~a~a | ~%" init-pad aline this-line-pad))
                        )))
                    ))
                ))
            ))

        (display
         (format
          #f "~a  ~a ~%" init-pad (make-string (1+ nwidth) #\-)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (draw-quote qq-string nwrap nwidth)
  (begin
    (let ((qlen (string-length qq-string)))
      (let ((nlines (quotient qlen nwidth)))
        (begin
          (if (or (equal? nwrap #t) (< nlines 1))
              (begin
                (draw-single-line-quote qq-string))
              (begin
                (draw-n-line-quote qq-string qlen nwidth nlines)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (draw-ascii-list
         ascii-list init-offset
         eyes-string thoughts-string tongue-string)
  (begin
    (let ((offset-space
           (make-string init-offset #\space))
          (ncow-lines (length ascii-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii ncow-lines))
          (begin
            (let ((astring (list-ref ascii-list ii)))
              (begin
                (if (not
                     (equal?
                      (ice-9-regex:string-match "eyes" astring) #f))
                    (begin
                      (let ((bstring
                             (ice-9-regex:regexp-substitute
                              #f
                              (ice-9-regex:string-match
                               "eyes" astring)
                              'pre eyes-string 'post)))
                        (begin
                          (set! astring bstring)
                          ))
                      ))
                (if (not
                     (equal?
                      (ice-9-regex:string-match
                       "thoughts" astring) #f))
                    (begin
                      (let ((bstring
                             (ice-9-regex:regexp-substitute
                              #f
                              (ice-9-regex:string-match
                               "thoughts" astring)
                              'pre thoughts-string 'post)))
                        (begin
                          (set! astring bstring)
                          ))
                      ))
                (if (not
                     (equal?
                      (ice-9-regex:string-match
                       "tongue" astring) #f))
                    (begin
                      (let ((bstring
                             (ice-9-regex:regexp-substitute
                              #f
                              (ice-9-regex:string-match
                               "tongue" astring)
                              'pre tongue-string 'post)))
                        (begin
                          (set! astring bstring)
                          ))
                      ))
                (display
                 (format #f " ~a~a~%"
                         offset-space astring))
                ))
            ))
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-animal-say
         ascii-list cow-string nwrap nwidth
         eyes-string thoughts-string tongue-string)
  (begin
    (let ((slen (string-length cow-string)))
      (let ((cwidth (max-cow-length ascii-list))
            (min-width (min slen nwidth)))
        (begin
          (draw-quote cow-string nwrap nwidth)
          (let ((init-offset
                 (inexact->exact
                  (max 0.0
                       (+ 4.0
                          (truncate
                           (+ 0.50
                              (* 0.50
                                 (- min-width
                                    cwidth)))))))))
            (begin
              (draw-ascii-list
               ascii-list init-offset
               eyes-string thoughts-string tongue-string)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (get-flag-no-argument
         args long-flag short-flag default-value)
  (begin
    (if (list? args)
        (begin
          (let ((alen (length args))
                (flag-result default-value)
                (found-flag #f))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((or (>= ii alen)
                       (equal? found-flag #t)))
                (begin
                  (let ((this-item (list-ref args ii)))
                    (begin
                      (if (or (string=? this-item long-flag)
                              (string=? this-item short-flag))
                          (begin
                            (set! found-flag #t)
                            (set! flag-result #t)
                            ))
                      ))
                  ))

              flag-result
              )))
        (begin
          default-value
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-flag-with-argument
         args long-flag short-flag default-value)
  (begin
    (if (list? args)
        (begin
          (let ((alen (length args))
                (fvalue default-value)
                (found-flag #f))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((or (>= ii alen)
                       (equal? found-flag #t)))
                (begin
                  (let ((this-item (list-ref args ii)))
                    (begin
                      (if (or (string=? this-item long-flag)
                              (string=? this-item short-flag))
                          (begin
                            (set! found-flag #t)
                            (let ((farg (list-ref args (1+ ii))))
                              (begin
                                (if (and
                                     (number? default-value)
                                     (not
                                      (equal?
                                       (string->number farg) #f)))
                                    (begin
                                      (set! fvalue (string->number farg)))
                                    (begin
                                      (set! fvalue farg)
                                      ))
                                ))
                            ))
                      ))
                  ))
              fvalue
              )))
        (begin
          default-value
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-alt-flag args eye-flags-htable)
  (begin
    (if (list? args)
        (begin
          (let ((alen (length args))
                (flag-result #f)
                (found-flag #f))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((or (>= ii alen)
                       (equal? found-flag #t)))
                (begin
                  (let ((this-item (list-ref args ii)))
                    (begin
                      (if (not
                           (equal?
                            (hash-ref eye-flags-htable this-item #f)
                            #f))
                          (begin
                            (set! found-flag #t)
                            (set!
                             flag-result
                             (hash-ref eye-flags-htable this-item #f))
                            ))
                      ))
                  ))

              flag-result
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (get-quote
         args-list options-list-list options-htable eye-flags-htable)
  (define (local-iter alist opt-list acc-list)
    (begin
      (if (or (null? alist)
              (and (list? alist)
                   (<= (length alist) 0)))
          (begin
            acc-list)
          (begin
            (let ((item (car alist))
                  (tail-list (cdr alist)))
              (begin
                (if (and
                     (string? item)
                     (string-prefix? "-" item))
                    (begin
                      (if (not (equal? (member item opt-list) #f))
                          (begin
                            ;;; get rid of flag argument
                            (if (equal?
                                 (hash-ref options-htable item #f)
                                 #t)
                                (begin
                                  (set! tail-list (cdr tail-list))
                                  )))
                          (begin
                            (if (equal?
                                 (hash-ref eye-flags-htable item #f)
                                 #f)
                                (begin
                                  ;;; if not an eye-flag option
                                  ;;; assume its part of quote
                                  (set! acc-list (cons item acc-list))
                                  ))
                            )))
                    (begin
                      (set! acc-list (cons item acc-list))
                      ))

                (let ((next-acc-list
                       (local-iter tail-list opt-list acc-list)))
                  (begin
                    (set! acc-list next-acc-list)
                    ))
                ))

            acc-list
            ))
      ))
  (begin
    (let ((opt-list
           (map
            (lambda (alist)
              (begin
                (car alist)
                )) options-list-list)))
      (let ((result-list
             (local-iter args-list opt-list (list))))
        (let ((result-string
               (string-join
                (reverse result-list) " ")))
          (begin
            result-string
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (process-list-message args)
  (begin
    (let ((iport
           (ice-9-popen:open-input-pipe "ls cows/*.cow")))
      (let ((line (read-line iport)))
        (begin
          (while (not (eof-object? line))
            (begin
              (let ((fname
                     (ice-9-regex:regexp-substitute
                      #f
                      (ice-9-regex:string-match
                       "cows/" line)
                      'pre 'post)))
                (begin
                  (display (format #f "~a~%" fname))
                  ))

              (set! line (read-line iport))
              ))

          (ice-9-popen:close-pipe iport)
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (display-help-message args)
  (begin
    (display (format #f "usage: ~a message~%" (car args)))
    (display (format #f "  --help, -h        display this message~%"))
    (display (format #f "  --list, -l        display available cows~%"))
    (display (format #f "  --nowrap, -n      no wrapping of text quote~%"))
    (display (format #f "  --width, -W       width of text quote~%"))
    (display (format #f "  --file, -f        display cow file~%"))
    (display (format #f "  --eyes, -e        eye string e.g. ^^~%"))
    (display (format #f "  --tongue, -T      tongue string e.g. U~%"))
    (display (format #f "  --thought, -t     thoughts bubble e.g. \\ ~%"))
    (display (format #f "  eyes options~%"))
    (display (format #f "  -b                borg cow, eyes ==~%"))
    (display (format #f "  -d                dead cow, eyes XX~%"))
    (display (format #f "  -g                greedy cow, eyes $$~%"))
    (display (format #f "  -p                paranoid cow, eyes @@~%"))
    (display (format #f "  -s                stoned cow, eyes **~%"))
    (display (format #f "  -t                tired cow, eyes --~%"))
    (display (format #f "  -w                wired cow, eyes OO~%"))
    (display (format #f "  -y                youthful cow, eyes ..~%"))
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (if (and
         (list? args)
         (> (length args) 1))
        (begin
          (let ((args-rest (cdr args))
                (options-htable (make-hash-table 20))
                (options-list-list
                 (list
                  (list "--help" #f) (list "-h" #f)
                  (list "--list" #f) (list "-l" #f)
                  (list "--nowrap" #f) (list "-n" #f)
                  (list "--width" #t) (list "-W" #t)
                  (list "--file" #t) (list "-f" #t)
                  (list "--eyes" #t) (list "-e" #t)
                  (list "--tongue" #t) (list "-T" #t)
                  (list "--thought" #t) (list "-u" #t)))
                (eye-flags-htable (make-hash-table 20))
                (eye-list-list
                 (list (list "-b" "==") (list "-d" "XX")
                       (list "-g" "$$") (list "-p" "@@")
                       (list "-s" "**") (list "-t" "--")
                       (list "-w" "00") (list "-y" "..")))
                (help-flag (get-flag-no-argument args "--help" "-h" #f))
                (list-flag (get-flag-no-argument args "--list" "-l" #f)))
            (begin
              (if (equal? help-flag #t)
                  (begin
                    (display-help-message args)
                    (quit)
                    ))

              (if (equal? list-flag #t)
                  (begin
                    (process-list-message args)
                    (quit)
                    ))

              ;;; setup options hash table
              (for-each
               (lambda (alist)
                 (begin
                   (let ((aflag (list-ref alist 0))
                         (astring (list-ref alist 1)))
                     (begin
                       (hash-set! options-htable aflag astring)
                       ))
                   )) options-list-list)

              ;;; setup eye-flags hash table
              (for-each
               (lambda (alist)
                 (begin
                   (let ((aflag (list-ref alist 0))
                         (astring (list-ref alist 1)))
                     (begin
                       (hash-set! eye-flags-htable aflag astring)
                       ))
                   )) eye-list-list)

              (if (and (list? args-rest)
                       (>= (length args-rest) 1))
                  (begin
                    (let ((arg-string
                           (get-quote
                            args-rest
                            options-list-list
                            options-htable
                            eye-flags-htable))
                          (nwrap
                           (get-flag-no-argument
                            args-rest "--nowrap" "-n" #f))
                          (nwidth
                           (get-flag-with-argument
                            args-rest "--width" "-W" 40))
                          (animal-string
                           (get-flag-with-argument
                            args-rest "--file" "-f" "default.cow"))
                          (eyes-string
                           (get-flag-with-argument
                            args-rest "--eyes" "-e" "OO"))
                          (thoughts-string
                           (get-flag-with-argument
                            args-rest "--thought" "-u" "\\"))
                          (tongue-string
                           (get-flag-with-argument
                            args-rest "--tongue" "-T" ""))
                          (eye-alt-flag
                           (get-alt-flag args eye-flags-htable)))
                      (begin
                        (if (not (equal? eye-alt-flag #f))
                            (begin
                              (set! eyes-string eye-alt-flag)
                              ))

                        (let ((ascii-list
                               (read-in-file
                                (format #f "cows/~a" animal-string))))
                          (begin
                            (make-animal-say
                             ascii-list arg-string nwrap nwidth
                             eyes-string thoughts-string tongue-string)
                            ))
                        )))
                  (begin
                    (display-help-message args)
                    (quit)
                    ))
              )))
        (begin
          (display-help-message args)
          (quit)
          ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
