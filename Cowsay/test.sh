#! /bin/bash

for iflag in "-b" "-d" "-g" "-p" "-s" "-t" "-w" "-y"
do
    ./cowsay.scm ${iflag} -W 20 $(date "+%A %A %A %B %B %B %d, %Y  %T")
done

./cowsay.scm --eyes "##" -W 20 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm -e "ee" -W 10 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm --thought "o" -W 20 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm -u "o" -W 10 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm -T "U" -W 10 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm -T "u" -W 10 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm -n -W 10 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm -f tux.cow -W 10 $(date "+%A %A %A %B %B %B %d, %Y  %T")

./cowsay.scm -f neko.cow $(date "+%A %A %A %B %B %B %d, %Y  %T")

