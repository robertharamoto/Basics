#! /bin/bash

filenames[0]="default.cow"
filenames[1]="neko.cow"
filenames[2]="tux.cow"
filenames[3]="penguin.cow"

DFORMAT="+%A %B %d, %Y  %l:%M:%S %P"
SLEEPSECS=120

while [ 1 ]; do
    for ii in 0 1 2 3; do
        fname=${filenames[ii]}
        ./cowsay.scm --file ${fname} $(date "$DFORMAT")
        sleep ${SLEEPSECS}
    done
done
