#! /bin/bash

E_NOARGS=85

if [ $# -eq 0 ]
then
    echo "Usage: `basename $0` filename" >&2
    ### Error message to stderr.
    exit $E_NOARGS
    # Returns 85 as exit status of script (error code).
fi

echo "ready to run ${1}"
FNAME="${1}"
shift
GFLAGS='GUILE_AUTO_COMPILE=0 GUILE_WARN_DEPRECATED="detailed"'
env ${GFLAGS} guile -e main "${FNAME}" "${@}"


