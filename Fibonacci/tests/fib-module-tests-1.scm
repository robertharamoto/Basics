;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for fib-module.scm                        ###
;;;###                                                       ###
;;;###  last updated August 7, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 17, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((fib-module)
              :renamer (symbol-prefix-proc 'fib-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (fib-tests-assert
         sub-name test-label-index
         anumber
         shouldbe result
         result-hash-table)
  (begin
    (let ((err-msg-1
           (format
            #f "~a : (~a) : error : number = ~a, "
            sub-name test-label-index anumber))
          (err-msg-2
           (format
            #f "shouldbe ~a, result = ~a"
            shouldbe result)))
      (let ((error-message
             (string-append
              err-msg-1 err-msg-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-message
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-slow-recursive-fib-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-slow-recursive-fib-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1)
           (list 3 2) (list 4 3) (list 5 5)
           (list 6 8) (list 7 13) (list 8 21)
           (list 9 34) (list 10 55) (list 11 89)
           (list 12 144) (list 13 233) (list 14 377)
           (list 15 610) (list 16 987) (list 17 1597)
           (list 18 2584) (list 19 4181) (list 20 6765)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (fib-module:slow-recursive-fib num)))
                (begin
                  (fib-tests-assert
                   sub-name test-label-index
                   num shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-fast-recursive-fib-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-fast-recursive-fib-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1) (list 3 2)
           (list 4 3) (list 5 5) (list 6 8) (list 7 13)
           (list 8 21) (list 9 34) (list 10 55) (list 11 89)
           (list 12 144) (list 13 233) (list 14 377) (list 15 610)
           (list 16 987) (list 17 1597) (list 18 2584) (list 19 4181)
           (list 20 6765)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (fib-module:fast-recursive-fib num)))
                (begin
                  (fib-tests-assert
                   sub-name test-label-index
                   num shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-loop-fib-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-loop-fib-1"
                  (utils-module:get-basename
                   (current-filename))))
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1)
           (list 3 2) (list 4 3) (list 5 5)
           (list 6 8) (list 7 13) (list 8 21)
           (list 9 34) (list 10 55) (list 11 89)
           (list 12 144) (list 13 233) (list 14 377)
           (list 15 610) (list 16 987) (list 17 1597)
           (list 18 2584) (list 19 4181) (list 20 6765)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (fib-module:loop-fib num)))
                (begin
                  (fib-tests-assert
                   sub-name test-label-index
                   num shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-memoized-fib-1 result-hash-table)
 (begin
   (let ((sub-name
          (format #f "~a:test-memoized-fib-1"
                  (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 0) (list 1 1) (list 2 1)
           (list 3 2) (list 4 3) (list 5 5)
           (list 6 8) (list 7 13) (list 8 21)
           (list 9 34) (list 10 55) (list 11 89)
           (list 12 144) (list 13 233) (list 14 377)
           (list 15 610) (list 16 987) (list 17 1597)
           (list 18 2584) (list 19 4181) (list 20 6765)
           ))
         (fib-htable (make-hash-table 100))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (fib-module:memoized-fib num fib-htable)))
                (begin
                  (fib-tests-assert
                   sub-name test-label-index
                   num shouldbe result
                   result-hash-table)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
