;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  fibonacci number calculations                        ###
;;;###                                                       ###
;;;###  last updated July 26, 2024                           ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 15, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start fib module

(define-module (fib-module)
  #:export (slow-recursive-fib
            fast-recursive-fib
            loop-fib
            memoized-fib

            slow-recursive-loop
            fast-recursive-loop
            do-loop-loop
            memoized-loop
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### format - used to commafy numbers
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;#############################################################
;;;#############################################################
;;;###  slow-recursive-fib - most inefficient way to compute
;;;###  fibonacci numbers, must compute fib(n-2) twice,
;;;###  fib(n-3) three times, fib(n-4) four times, ...
;;;###  but it does conform closest to the definition of the
;;;###  fibonacci function
(define (slow-recursive-fib n)
  (begin
    (cond
     ((<= n 0)
      (begin
        0
        ))
     ((or (= n 1)
          (= n 2))
      (begin
        1
        ))
     (else
      (begin
        (+ (slow-recursive-fib (- n 1))
           (slow-recursive-fib (- n 2)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (fast-recursive-fib n)
  (define (local-iter count nn bb aa)
    (begin
      (cond
       ((>= count nn)
        (begin
          bb
          ))
       (else
        (begin
          (local-iter
           (1+ count) nn aa (+ aa bb))
          )))
      ))
  (begin
    (local-iter 0 n 0 1)
    ))

;;;#############################################################
;;;#############################################################
(define (loop-fib n)
  (begin
    (let ((aa 1)
          (bb 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii n))
          (begin
            (let ((old-aa aa))
              (begin
                (set! aa (+ old-aa bb))
                (set! bb old-aa)
                ))
            ))
        bb
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (memoized-fib n memo-htable)
  (begin
    (cond
     ((<= n 0)
      (begin
        0
        ))
     ((or (= n 1)
          (= n 2))
      (begin
        1
        ))
     (else
      (begin
        (let ((aa (hash-ref memo-htable n #f)))
          (begin
            (if (equal? aa #f)
                (begin
                  (let ((aa
                         (memoized-fib (- n 1) memo-htable))
                        (bb
                         (memoized-fib (- n 2) memo-htable)))
                    (begin
                      (hash-set! memo-htable n (+ aa bb))
                      )))
                (begin
                  aa
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (intermediate-loop func end-nn debug-flag)
  (begin
    (let ((ll-counter 5))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii end-nn))
          (begin
            (let ((result (func ii)))
              (begin
                (if (equal? debug-flag #t)
                    (begin
                      (display
                       (ice-9-format:format
                        #f "(f~:d ; ~:d)" ii result))
                      (if (zero? (modulo (1+ ii) ll-counter))
                          (begin
                            (newline))
                          (begin
                            (display " ")
                            ))
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (slow-recursive-loop end-nn debug-flag)
  (begin
    (intermediate-loop
     slow-recursive-fib end-nn debug-flag)
    ))

;;;#############################################################
;;;#############################################################
(define (fast-recursive-loop end-nn debug-flag)
  (begin
    (intermediate-loop
     fast-recursive-fib end-nn debug-flag)
    ))

;;;#############################################################
;;;#############################################################
(define (do-loop-loop end-nn debug-flag)
  (begin
    (intermediate-loop
     loop-fib end-nn debug-flag)
    ))

;;;#############################################################
;;;#############################################################
(define (memoized-loop end-nn debug-flag)
  (begin
    (let ((fib-htable (make-hash-table 100)))
      (begin
        (intermediate-loop
         (lambda (anum)
           (begin
             (memoized-fib anum fib-htable)
             ))
         end-nn debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
