#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  generate the makefile                                ###
;;;###                                                       ###
;;;###  last updated September 25, 2024                      ###
;;;###                                                       ###
;;;###  updated April 29, 2022                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### regex used for regex
(use-modules ((ice-9 regex)
              :renamer (symbol-prefix-proc 'ice-9-regex:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### popen - pipes functions
(use-modules ((ice-9 popen)
              :renamer (symbol-prefix-proc 'pipes-module:)))

;;;### rdelim - read-line
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'rdelim-module:)))

;;;### getopt-long used for command-line option arguments processing
(use-modules ((ice-9 getopt-long)
              :renamer (symbol-prefix-proc 'ice-9-getopt:)))

;;;### ice-9 ftw for file tree walk functions
(use-modules ((ice-9 ftw)
              :renamer (symbol-prefix-proc 'ice9-ftw:)))

;;;#############################################################
;;;#############################################################
;;;### used to get data from system
(define (get-one-line-from-shell command-string)
  (let ((port (pipes-module:open-input-pipe command-string)))
    (let ((result-string (rdelim-module:read-line port)))
      (begin
        (pipes-module:close-pipe port)
        (if (eof-object? result-string)
            (begin
              #f)
            (begin
              result-string
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;### load list of *.scm files
(define (list-all-scheme-files init-path file-string sub-string)
  (let ((counter 0)
        (file-list (list)))
    (begin
      (ice9-ftw:nftw
       init-path
       (lambda (filename statinfo flag base level)
         (begin
           (if (equal? flag 'regular)
               (begin
                 (if (and
                      (not (equal?
                            (ice-9-regex:string-match
                             file-string filename) #f))
                      (not (equal?
                            (ice-9-regex:string-match
                             sub-string filename) #f)))
                     (begin
                       (set! file-list
                             (append
                              file-list (list (basename filename))))
                       ))
                 ))
           #t
           )))

      file-list
      )))

;;;#############################################################
;;;#############################################################
(define-syntax display-single-line-comment-string-macro
  (syntax-rules ()
    ((display-single-line-comment-string-macro
      var-comment var-length)
     (begin
       (let ((var-clen
              (string-length var-comment))
             (local-comment var-comment)
             (max-allowed-len (- var-length 10)))
         (begin
           (if (not (equal? var-clen #f))
               (begin
                 (if (> var-clen max-allowed-len)
                     (begin
                       (set! local-comment
                             (substring
                              var-comment 0 max-allowed-len))
                       (set! var-clen
                             (string-length local-comment))
                       ))

                 (let ((var-space-len
                        (- var-length var-clen 10)))
                   (begin
                     (if (> var-space-len 0)
                         (begin
                           (let ((spacer-string
                                  (make-string
                                   var-space-len #\space)))
                             (begin
                               (display
                                (format
                                 #f "###  ~a~a  ###~%"
                                 local-comment
                                 spacer-string))
                               )))
                         (begin
                           (display
                            (format
                             #f "###  ~a  ###~%"
                             local-comment))
                           ))
                     )))
               (begin
                 (let ((spacer-string
                        (make-string
                         (- var-length 10) #\space)))
                   (begin
                     (display
                      (format
                       #f "###  ~a  ###~%"
                       spacer-string))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-boxed-two-line-comment-string-macro
  (syntax-rules ()
    ((display-boxed-two-line-comment-string-macro
      separator-comment-string
      comment-line-1-string comment-line-2-string
      comment-length)
     (begin
       (let ((blank-string " "))
         (begin
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))

           (display-single-line-comment-string-macro
            blank-string comment-length)

           (display-single-line-comment-string-macro
            comment-line-1-string comment-length)

           (display-single-line-comment-string-macro
            comment-line-2-string comment-length)

           (display-single-line-comment-string-macro
            blank-string comment-length)
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-boxed-one-line-comment-string-macro
  (syntax-rules ()
    ((display-boxed-one-line-comment-string-macro
      separator-comment-string
      comment-line-string comment-length)
     (begin
       (let ((blank-string " "))
         (begin
           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))

           (display-single-line-comment-string-macro
            blank-string comment-length)

           (display-single-line-comment-string-macro
            comment-line-string comment-length)

           (display-single-line-comment-string-macro
            blank-string comment-length)

           (display
            (format #f "~a~%" separator-comment-string))
           (display
            (format #f "~a~%" separator-comment-string))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-definitions-macro
  (syntax-rules ()
    ((display-definitions-macro
      sources-dir tests-dir objects-dir)
     (begin
       (display
        (format #f "SOURCESDIR=~a~%" sources-dir))
       (display
        (format #f "TESTSDIR=~a~%" tests-dir))
       (display
        (format #f "OBJECTSDIR=~a~%" objects-dir))

       (newline)
       (display
        (format #f "### flags~%"))
       (display
        (format
         #f "WARNINGS=--warn=unsupported-warning --warn=unused-variable"))
       (display
        (format
         #f " --warn=unused-toplevel --warn=unbound-variable"))
       (display
        (format
         #f " --warn=arity-mismatch --warn=duplicate-case-datum"))
       (display
        (format
         #f " --warn=bad-case-datum --warn=format~%"))
       (display
        (format
         #f "TESTWARNINGS=--warn=unsupported-warning --warn=unused-variable"))
       (display
        (format
         #f " --warn=unbound-variable"))
       (display
        (format
         #f "  --warn=arity-mismatch --warn=duplicate-case-datum"))
       (display
        (format
         #f " --warn=bad-case-datum --warn=format~%"))
       (display
        (format
         #f "LOADFLAGS=-L $(SOURCESDIR) -L $(TESTSDIR) -L $(OBJECTSDIR)~%"))
       (display
        (format
         #f "ENVVARS=GUILE_AUTO_COMPILE=0 GUILE_WARN_DEPRECATED=\"detailed\"~%"))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-compile-source-files-macro
  (syntax-rules ()
    ((display-compile-source-files-macro
      sources-list sources-deps-list)
     (begin
       (for-each
        (lambda (fname)
          (let ((obj-name
                 (ice-9-regex:regexp-substitute/global
                  #f ".scm$" fname
                  'pre ".go" 'post)))
            (begin
              (set!
               sources-deps-list
               (append sources-deps-list (list obj-name)))
              (display
               (format
                #f "$(OBJECTSDIR)/~a: $(SOURCESDIR)/~a~%"
                obj-name fname))
              (display
               (format
                #f "\t$(ENVVARS) $(GUILD) compile $(WARNINGS)"))
              (display
               (format
                #f " $(LOADFLAGS) -o $(OBJECTSDIR)/~a $(SOURCESDIR)/~a~%"
                obj-name fname))
              (newline)
              )))
        (sort sources-list string<?))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-compile-test-files-macro
  (syntax-rules ()
    ((display-compile-test-files-macro
      tests-list tests-deps-list)
     (begin
       (for-each
        (lambda (fname)
          (let ((obj-name
                 (ice-9-regex:regexp-substitute/global
                  #f ".scm$" fname
                  'pre ".go" 'post)))
            (begin
              (if (not
                   (string-ci=? fname "new-test.scm"))
                  (begin
                    (set!
                     tests-deps-list
                     (append tests-deps-list (list obj-name)))
                    (display
                     (format
                      #f "$(OBJECTSDIR)/~a: $(TESTSDIR)/~a~%"
                      obj-name fname))
                    (display
                     (format
                      #f "\t$(ENVVARS) $(GUILD) compile $(TESTWARNINGS)"))
                    (display
                     (format
                      #f " $(LOADFLAGS) -o $(OBJECTSDIR)/~a $(TESTSDIR)/~a~%"
                      obj-name fname))
                    (newline)
                    ))
              )))
        (sort tests-list string<?))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-make-dependencies-macro
  (syntax-rules ()
    ((display-make-dependencies-macro
      sources-deps-list tests-deps-list)
     (begin
       (display
        (format #f "all-deps: $(OBJECTSDIR) "))
       (for-each
        (lambda(oname)
          (display
           (format #f "$(OBJECTSDIR)/~a " oname)))
        (append sources-deps-list tests-deps-list))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-make-tests-macro
  (syntax-rules ()
    ((display-make-tests-macro)
     (begin
       (display
        (format #f "run-tests: all-deps~%"))
       (display
        (format #f "\t-date > out-test.txt~%"))
       (display
        (format #f "\t-./run-tests.scm >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t@echo  >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t@echo -n   sources loc = >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t@wc -l sources/*.scm "))
       (display
        (format #f "| tail -n 1 >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t@echo -n   tests loc = >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t@wc -l tests/*.scm "))
       (display
        (format #f "| tail -n 1 >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t@echo -n total loc =  >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t@wc -l tests/*.scm sources/*.scm "))
       (display
        (format #f "| tail -n 1 >> out-test.txt 2>&1~%"))
       (display
        (format #f "\t-cat out-test.txt~%"))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-make-clean-macro
  (syntax-rules ()
    ((display-make-clean-macro)
     (begin
       (display
        (format #f ".PHONY : clean~%"))
       (display
        (format #f "clean:~%"))
       (display
        (format #f "\t-rm -fv $(OBJECTSDIR)/*.go~%"))
       (display
        (format #f "\t-rm -fv $(SOURCESDIR)/*.scm~~ ~%"))
       (display
        (format #f "\t-rm -fv $(TESTSDIR)/*.scm~~~%"))
       (display
        (format #f "\t-rm -fv *.scm~~ *.sh~~ Makefile~~~%"))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;### write out makefile
(define (display-makefile
         sources-dir tests-dir objects-dir
         version-string)
  (begin
    (let ((sources-list
           (list-all-scheme-files sources-dir sources-dir ".scm$"))
          (tests-list
           (list-all-scheme-files tests-dir tests-dir ".scm$"))
          (sources-deps-list (list))
          (tests-deps-list (list))
          (comment-length 64))
      (let ((separator-comment-string
             (make-string comment-length #\#))
            (blank-string " ")
            (make-title-string
             (format #f "make file (version ~a)"
                     version-string))
            (make-date-string
             (srfi-19:date->string
              (srfi-19:current-date) "~A, ~B ~d, ~Y")))
        (begin
          (display-boxed-two-line-comment-string-macro
           separator-comment-string
           make-title-string
           make-date-string
           comment-length)

          (let ((guild
                 (get-one-line-from-shell "which guild")))
            (begin
              (display
               (format #f "GUILD=~a~%" guild))
              ))

          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "define directories"
           comment-length)

          (display-definitions-macro
           sources-dir tests-dir objects-dir)

          (newline)
          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "main definitions"
           comment-length)

          (display
           (format #f "all: run-tests~%"))

          (newline)
          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "individual source files"
           comment-length)

          (display-compile-source-files-macro
           sources-list sources-deps-list)

          (newline)
          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "individual test files"
           comment-length)

          (display-compile-test-files-macro
           tests-list tests-deps-list)

          (newline)
          (newline)
          (force-output)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "create objects directory if needed"
           comment-length)

          (display
           (format #f "$(OBJECTSDIR):~%"))
          (display
           (format #f "\t-mkdir $(OBJECTSDIR)~%"))

          (newline)
          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "compiler dependencies"
           comment-length)

          (display-make-dependencies-macro
           sources-deps-list tests-deps-list)

          (newline)
          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "run tests"
           comment-length)

          (display-make-tests-macro)

          (newline)
          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "clean up"
           comment-length)

          (display-make-clean-macro)

          (newline)
          (newline)

          (display-boxed-one-line-comment-string-macro
           separator-comment-string
           "end of make file"
           comment-length)

          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-variable-macro
  (syntax-rules ()
    ((update-variable-macro
      var-name var-symbol options)
     (begin
       (let ((var-flag
              (ice-9-getopt:option-ref
               options var-symbol #f)))
         (begin
           (if (not (equal? var-flag #f))
               (begin
                 (let ((var2-flag
                        (ice-9-regex:regexp-substitute/global
                         #f "," var-flag 'pre "" 'post)))
                   (begin
                     (if (number?
                          (string->number var2-flag))
                         (begin
                           (set!
                            var-name
                            (string->number var2-flag)))
                         (begin
                           (set! var-name var-flag)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-flag-macro
  (syntax-rules ()
    ((update-flag-macro
      var-name var-symbol options)
     (begin
       (if (string? var-name)
           (begin
             (if (string-ci=? var-name "true")
                 (begin
                   (set! var-name #t))
                 (begin
                   (set! var-name #f)
                   )))
           (begin
             (set! var-name #f)
             ))

       (let ((var-flag
              (ice-9-getopt:option-ref options var-symbol #f)))
         (begin
           (if (equal? var-flag #t)
               (begin
                 (set! var-name #t)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin main code                                      ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(define (main args)
  (define (local-display-version title-string)
    (begin
      (display
       (format #f "~a~%" title-string))
      (force-output)
      ))
  (define (local-display-help ospec title-string)
    (begin
      (display
       (format #f "~a~%" title-string))
      (display
       (format #f "options available~%"))
      (for-each
       (lambda(llist)
         (begin
           (display
            (format
             #f "  --~a, -~a~%"
             (car llist) (cadr (cadr llist))))
           )) ospec)
      (force-output)
      ))
  (begin
    (let ((version-string "2024-09-25")
          (makefile-name "Makefile"))
      (let ((sources-dir "sources")
            (tests-dir "tests")
            (objects-dir "objects")
            (title-string
             (format
              #f "Generate Makefile (version ~a)"
              version-string))
            (option-spec
             (list (list 'version '(single-char #\v) '(value #f))
                   (list 'help '(single-char #\h) '(value #f))
                   (list 'sources '(single-char #\s) '(value #t))
                   (list 'tests '(single-char #\t) '(value #t))
                   (list 'objects '(single-char #\o) '(value #t))
                   )))
        (let ((options
               (ice-9-getopt:getopt-long args option-spec)))
          (begin
            ;;;### command line help option
            (let ((help-flag
                   (ice-9-getopt:option-ref options 'help #f)))
              (begin
                (if (equal? help-flag #t)
                    (begin
                      (local-display-help option-spec title-string)
                      (quit)
                      ))
                ))
            ;;;### command line version option
            (let ((version-flag
                   (ice-9-getopt:option-ref options 'version #f)))
              (begin
                (if (equal? version-flag #t)
                    (begin
                      (local-display-version title-string)
                      (quit)
                      ))
                ))

            (update-variable-macro
             sources-dir 'sources options)

            (update-variable-macro
             tests-dir 'tests options)

            (update-variable-macro
             objects-dir 'objects options)

            (display
             (format
              #f "sources = ~a, tests = ~a, objects = ~a~%"
              sources-dir tests-dir objects-dir))
            (force-output)

            ;;;### once we have the input parameters, display makefile
            (with-output-to-file
                makefile-name
              (lambda ()
                (begin
                  (display-makefile
                   sources-dir tests-dir objects-dir
                   version-string)
                  )))

            (let ((dstring
                   (string-downcase
                    (srfi-19:date->string
                     (srfi-19:current-date)
                     "~A, ~B ~d, ~Y  ~I:~M:~S ~p"))))
              (begin
                (display (format #f "output to ~s  :  ~a~%"
                                 makefile-name dstring))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
